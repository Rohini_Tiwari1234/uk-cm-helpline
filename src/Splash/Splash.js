import React from 'react';
import {  Linking, ActivityIndicator, AsyncStorage,Dimensions, StatusBar,Alert, Image, StyleSheet, View,BackHandler} from 'react-native';
import Apis from '../apis/Api';
import { Spinner} from '@shoutem/ui';
import DeviceInfo from 'react-native-device-info';

const ScreenHeight = Dimensions.get("window").height;
const ScreenWidth = Dimensions.get("window").width;
export default class Splash extends React.Component {

  constructor(props) {
      super(props);
      this.state = {
        VersionDetail:[]
      }
    }
  
    componentWillMount(){
        
        var version = DeviceInfo.getVersion();
        console.log('login',version)
        Apis.GetMobile_app_version().then((res) => {
           
          console.log('res GetMobile_app_version');
         console.log(res,version);
            this.setState({VersionDetail:res.VersionDetail})

            if(version > res.VersionDetail.Android_build_version || version==res.VersionDetail.Android_build_version){
              this._bootstrapAsync();
            }
            else{
              Alert.alert(
                'Alert',
                'A new version of app is available. Please update.',
                [
                  { text: 'Later', onPress: () =>  this._bootstrapAsync() },
                  {
                    text: 'OK', onPress: () =>{
                      Linking.canOpenURL("https://play.google.com/store/apps/details?id=com.cmhelpline_officer&hl=en").then(supported => {
                      if (supported) {
                        Linking.openURL("https://play.google.com/store/apps/details?id=com.cmhelpline_officer&hl=en");
                      } else {
                        BackHandler.exitApp();
                      }
                    });
                    } 
                   },
                              //{text: 'OK', onPress: () => console.log('OK Pressed')},
                            ],
                            { cancelable: false }
                          )
            
            }
         
           },(error) => {
      
          
      });

      // this._bootstrapAsync();
         
      }


    // Fetch the token from storage then navigate to our appropriate place
    _bootstrapAsync = async () => {
     
       AsyncStorage.getItem("loggedIn").then((userToken)=>{
        console.log('loggedIn',userToken);
        if(userToken=="1")
        {
          this.props.navigation.navigate('Dashboard');
        
          
        }
        else if(userToken=="2")
        {
          this.props.navigation.navigate('CitizenDashboard');
        
          
        }
        else{
          this.props.navigation.navigate('Auth');
        }
        
       })
    
      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
      
    };

  
    // Render any loading content that you like here
    render() {
      return (
        <View style={{flex:1,backgroundColor:'#05ADEE'}}>

<StatusBar
          barStyle="dark-content"
          backgroundColor="#05ADEE"
        />
          {/* <View style={{ padding: 10, marginTop: '-15%'}}>
          {/* <Image source={require('../image/image/snag.gif')} style={{width:ScreenWidth,height:ScreenHeight}}/> 
          </View> */}
           <Spinner style={{color:'#fff'}}/>
        
        </View>
      );
    }
  }
