import React from 'react';
import {createStackNavigator , createSwitchNavigator,createAppContainer} from 'react-navigation';
import Login from './pages/Login/Login';
import Splash from './Splash/Splash';
import OfficerApp from './router/OfficerNavigationRoute';
import CitizenApp from './router/CitizenNavigationRoute';
import CitizenCorner from './pages/Login/CitizenCorner';
import CitizenLogin from './pages/Login/CitizenLogin';

console.disableYellowBox = true;

const AppNavigator = createStackNavigator (
    {
        CitizenCorner: {
            screen: CitizenCorner,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        },
        
        Login: {
            screen: Login,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        },

        CitizenLogin: {
            screen: CitizenLogin,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        }
       
});

const MainSwitch = createSwitchNavigator(
    {
       Splash:Splash,
        OApp: OfficerApp,
        CApp: CitizenApp,
       Auth: AppNavigator,
    },
    {
        initialRouteName: 'Splash',
    }
);

// export default class App extends React.Component{
   

//     constructor(Props){
//         super(Props);
//         this.state ={
//           appToken:"",
          
//         }
//       }

//       render(){
//         return <MainSwitch />
//     }
// }

export default createAppContainer(MainSwitch);
   


