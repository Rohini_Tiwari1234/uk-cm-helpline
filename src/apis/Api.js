const URL = 'http://cmhelpline.uk.gov.in:81/api'; // live


// const URL = 'http://cmhelpline.mp.gov.in:84/api'; // local

var cmAPI = {
    AddRegister_CitizenAppmobileuser(deviceudid) {
        // console.log("deviceudid",deviceudid);
        var url = URL + '/app_reg.asmx/registered_app?deviceudid=' + deviceudid;
        console.log(url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log(response); return response.json() });
    },
    getOfficerLogin(deviceudid, userid, Password) {


        var url = URL + '/api-login.asmx/login?deviceudid=' + deviceudid + '&&username=' + userid + '&&password=' + Password;
        console.log(url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log(response); return response.json() });
    },
    dashboardCount(officerid) {
        var url = URL + '/compcount.asmx/getcount?officerid=' + officerid;
        console.log(url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { return response.json() });
    },
    ComplaintproblemStatus(Officerid, status) {
        console.log('Officerid->', Officerid);
        console.log('status->', status);
        var url = URL + '/compcount.asmx/GetComplaintByStatusRemarkAndOfficerid?Officerid=' + Officerid + '&&StatusRemark=' + status;
        console.log("hitting: ", url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log("problem status response"); console.log(response); return response.json() });
    },

    GetComplaintByCompidAndOfficerid(Officerid, Compid) {
        console.log('Officerid->', Officerid);
        console.log('Compid->', Compid);
        var url = URL + '/compcount.asmx/GetComplaintByCompidAndOfficerid?Officerid=' + Officerid + '&&Compid=' + Compid;
        console.log("hitting: ", url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log("problem status response"); console.log(response); return response.json() });
    },





    UpdateComplaint(Compid, officerid, Remark, progacttype) {
        console.log('Officerid->', officerid);
        console.log('status->', Remark);
        var url = URL + '/api-comment.asmx/Comments?Compid=' + Compid + '&&officerid=' + officerid + '&&Remark=' + Remark + '&&progacttype=' + progacttype + '&&auth=&&compclose=';
        console.log("hitting: ", url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log("problem status response"); console.log(response); return response.json() });

    },

    GetCountAtrributeOrDistrictwise(officerid, attribid, distid, statusremark) {

        var url = URL + '/compcount.asmx/GetCountAtrributeOrDistrictwise?officerid=' + officerid + '&&attribid=' + attribid + '&&distid=' + distid + '&&statusremark=' + statusremark;
        console.log("hitting: ", url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log("problem status response"); console.log(response); return response.json() });

    },
    //attribid=0
    GetCountAtrributeAndDistrictwise(officerid, attribid, statusremark) {
        console.log("hitting: parameter ", officerid, attribid, statusremark);
        var url = URL + '/compcount.asmx/GetCountAtrributeAndDistrictwise?officerid=' + officerid + '&&attributeid=' + attribid + '&&statusremark=' + statusremark;
        console.log("hitting: ", url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log("problem status response"); console.log(response); return response.json() });

    },


    compdetailAttributeAndDistrictwise(userid, attributeid, Districtid, firstpage, nextpage, statusremark) {
        console.log("hitting: parameter ", userid, attributeid, Districtid, firstpage, nextpage, statusremark);
        var url = URL + '/api-pending.asmx/compdetailAttributeAndDistrictwise?userid=' + userid + '&&attributeid=' + attributeid + '&&Districtid=' + Districtid + '&&firstpage=' + firstpage + '&&nextpage=' + nextpage + '&&statusremark=' + statusremark;
        console.log("hitting: ", url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log("problem status response"); console.log(response); return response.json() });

    },

    // compdetailAttributewiseTL(userid, attributeid, type, firstpage, nextpage) {
    //     console.log("hitting: parameter ", userid, attributeid, type, firstpage, nextpage);
    //     var url = URL + '/api-pending.asmx/compdetailAttributewiseTL?userid=' + userid + '&&attributeid=' + attributeid + '&&type=' + type + '&&firstpage=' + firstpage + '&&nextpage=' + nextpage;
    //     console.log("hitting: ", url);
    //     return fetch(url, {
    //         method: 'GET',
    //     }).then((response) => { console.log("problem status response"); console.log(response); return response.json() });

    // },

    compdetailAttributeAndDistrictwiseTL(userid, attributeid, type, firstpage, nextpage,Districtid) {
        console.log("hitting: parameter ", userid, attributeid, type, firstpage, nextpage,Districtid);
        var url = URL + '/api-pending.asmx/compdetailAttributeAndDistrictwiseTL?userid=' + userid + '&&attributeid=' + attributeid +'&&Districtid=' + Districtid +  '&&firstpage=' + firstpage + '&&nextpage=' + nextpage+'&&type=' + type;
        console.log("hitting: ", url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log("problem status response"); console.log(response); return response.json() });

    },

      //not working nad not in use
      GetCountAtrributeOrDistrictwiseTL(type, Officerid,attributeid,districtid){
        console.log("hitting: parameter ",type, Officerid,attributeid,districtid);
        var url = URL + '/compcount.asmx/GetCountAtrributeOrDistrictwiseTL?type='+type+'&&Officerid='+Officerid+'&&attributeid='+attributeid+'&&districtid='+districtid;
     console.log("hitting: ",url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => {console.log("problem status response");console.log(response);return response.json()});

      },

    GetCountAtrributeAndDistrictwiseTL(Officerid, type) {
        console.log("hitting: parameter ", Officerid, type);
        var url = URL + '/compcount.asmx/GetCountAtrributeAndDistrictwiseTL?Officerid=' + Officerid + '&&type=' + type;
        console.log("hitting: ", url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log("problem status response"); console.log(response); return response.json() });

    },

  GetCountAtrributeAndDistrictwiseTL(Officerid, type) {
        console.log("hitting: parameter ", Officerid, type);
        var url = URL + '/compcount.asmx/GetCountAtrributeAndDistrictwiseTL?Officerid=' + Officerid + '&&type=' + type;
        console.log("hitting: ", url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log("problem status response"); console.log(response); return response.json() });

    },

    details(compid) {
        console.log("hitting: parameter ", compid);
        var url = URL + '/api-details.asmx/details?tid=' + compid;
        console.log("hitting: ", url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log("problem status response"); console.log(response); return response.json() });

    },

    CommentsforApprovedDisapprovedComplaint(Compid, auth, UId, prognotes, progacttype, compclose) {
        console.log("hitting: parameter ", Compid);
        var url = URL + '/api-comment.asmx/CommentsforApprovedDisapprovedComplaint?Compid=' + Compid + '&&auth=' + auth+'&&UId=' + UId + '&&prognotes=' + prognotes+'&&progacttype=' + progacttype + '&&compclose=' + compclose;
        console.log("hitting: ", url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log("problem status response"); console.log(response); return response.json() });

    },

    UpdateProfile(officerid, name, newPwd, designation, phone_no, DistHeadquarter) {
        console.log("hitting: parameter ", officerid, name, newPwd, designation, phone_no, DistHeadquarter);
        var url = URL + '/profileupdate.asmx/UpdateProfile?officerid=' + officerid + '&&name=' + name+'&&newPwd=' + newPwd + '&&designation=' + designation+'&&phone_no=' + phone_no + '&&DistHeadquarter=' + DistHeadquarter;
        console.log("hitting: ", url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log("problem status response"); console.log(response); return response.json() });

    },
    GetcompremarkFor93(Compid) {
        console.log("hitting: parameter ", Compid);
        var url = URL + '/api-detail1.asmx/GetcompremarkFor93?Compid=' + Compid;
        console.log("hitting: ", url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log("problem status response"); console.log(response); return response.json() });

    },

    OWAServicesSaveComplaint(Dept, Attribute, Dist, Block, Thana, Circle, Range, Div, Zone, Teh, Ward, VK, SubDiv, GP, G, Inst, Remark, CompID, officerid) {
        console.log("hitting: parameter ", Dept, Attribute, Dist, Block, Thana, Circle, Range, Div, Zone, Teh, Ward, VK, SubDiv, GP, G, Inst, Remark, CompID, officerid);
        var url = URL + '/api-OWAServices.asmx/SaveComplaint?Dept='+Dept+'&&Attribute=' + Attribute+'&&Dist=' + Dist+'&&Block=' + Block+'&&Thana=' + Thana+'&&Circle='+Circle+'&&Range=' + Range+'&&Div=' + Div+'&&Zone=' + Zone+'&&Teh=' + Teh+'&&Ward='+Ward+'&&VK=' + VK+'&&SubDiv=' + SubDiv+'&&GP=' + GP+'&&G=' + G+'&&Inst=' + Inst+'&&Remark=' + Remark+'&&CompID=' + CompID+'&&officerid=' + officerid;
        console.log("hitting: ", url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log("problem status response"); console.log(response); return response.json() });

    },

    GetMobile_app_version() {
       
        var url = URL + '/api-login.asmx/GetMobile_app_version';
        console.log("hitting: ", url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log("problem status response"); console.log(response); return response.json() });

    },

    getcountForAdminOfficer(SourceId,Officerid) {
       
        var url = URL + '/compcount.asmx/getcountForAdminOfficer?SourceId='+SourceId+'&&Officerid='+Officerid;
        console.log("hitting: ", url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => { console.log("problem status response"); console.log(response); return response.json() });

    }
    
}

module.exports = cmAPI;