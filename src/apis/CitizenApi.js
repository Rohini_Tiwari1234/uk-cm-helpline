 //const URL = 'http://cmhelpline.mp.gov.in:90/api';
 
// const URL = 'https://cmhelpline.uk.gov.in:81/api';  //live
const URL = 'http://cmhelpline.uk.gov.in:81/api';  //live


var cmAPI = {

    generateOTP(mobileno){
        console.log(mobileno);
        var url = URL + '/api-login.asmx/send_otp?mobileno='+mobileno;
        console.log("url" +url);
        return fetch(url, {
           method: 'GET',
        }).then((response) => response.json());
    },

    getData(mobileno,otp){
        var url = URL + '/api-login.asmx/Verify_Otp_and_Getdetails?mobileno='+mobileno+'&&otp='+otp;
        return fetch(url, {
          method: 'GET',
        }).then((response) => response.json());
    },

    fetchDistrict(){
        var url = URL + '/api-OWAservices.asmx/fillDist';
        return fetch( url , {
               method: 'GET',
               headers: {
              
                },}).then((res) => res.json());
    },

    fetchDistrictWiseBlock(data){
        console.log('block district idcin api',data)
        var url = URL + '/api-OWAservices.asmx/FillDistBlk?vID2='+data;
        return fetch( url , {
               method: 'GET',
               headers: {
               
                },}).then((res) => res.json());
    },
  
    GetBlocksByDistByDep(districtId, subdepartmentId){
        console.log('block district and dept wise idcin api',districtId,subdepartmentId)
        var url = URL + '/api-OWAservices.asmx/GetBlocksByDistByDep?districtId='+districtId+'&&subdepartmentId='+subdepartmentId;
        return fetch( url , {
               method: 'GET',
               headers: {
               
                },}).then((res) => res.json());
    },

    fetchDistwiseGramPanchayat(data){
        var url = URL + '/api-OWAservices.asmx/Filldistrictwisegrampanchayat?distid='+data;
        return fetch( url , {
               method: 'GET',
               headers: {
               
                },}).then((res) => res.json());
    },

    fetchDepartmentName(){
        var url = URL + '/app_reg.asmx/GetDepartment?Deptid='+0;
        return fetch( url , {
               method: 'GET',
               headers: {
               
                },}).then((res) => res.json());
    },
   
    fetchAttributeName(deptid,attrid){
        var url = URL + '/app_reg.asmx/GetAttribute?Deptid='+deptid+'&&Attrib='+attrid;
        return fetch( url , {
               method: 'GET',
               headers: {
               
                },}).then((res) => res.json());
    },

    fetchCircleName(distid,deptid){
        var url = URL + '/api-OWAservices.asmx/FillCircle?vID1='+distid+'&&vID2='+deptid;
        return fetch( url , {
               method: 'GET',
               headers: {
               
                },}).then((res) => res.json());
    },

    fetchThanaName(distid){
        var url = URL + '/api-OWAservices.asmx/FillThana?DistCode='+distid;
        return fetch( url , {
               method: 'GET',
               headers: {
               
                },}).then((res) => res.json());
    },

    fetchVitranKendraName(distid){
        var url = URL + '/api-OWAservices.asmx/FillVk?vDist1='+distid;
        return fetch( url , {
               method: 'GET',
               headers: {
               
                },}).then((res) => res.json());
    },

    fetchTehsilName(distid){
        var url = URL + '/api-OWAservices.asmx/FillDistTeh?vID1='+distid;
        return fetch( url , {
               method: 'GET',
               headers: {
               
                },}).then((res) => res.json());
    },

    fetchSubdivisionBythanaId(distid){
        var url = URL + '/api-OWAservices.asmx/FillDistTeh?vID1='+distid;
        return fetch( url , {
               method: 'GET',
               headers: {
               
                },}).then((res) => res.json());
    },

    fetchWard(distid){
        let blockid=0;
        var url = URL + '/api-OWAservices.asmx/FillDistWard?DistrictCode='+distid+'&&GroupCode='+blockid;
        return fetch( url , {
               method: 'GET',
               headers: {
               
                },}).then((res) => res.json());
    },

    fetchAnubhag(distid,deptid){
        var url = URL + '/api-OWAservices.asmx/FillSubDiv?DistrictCode='+distid+'&&SubDepCode='+deptid;
        return fetch( url , {
               method: 'GET',
               headers: {
               
                },}).then((res) => res.json());
    },
    // RegisterComplaint(data){
    //     var url = URL + '/api-OWAservices.asmx/RegisterNewComplaint';
    //    return   fetch( url , {
    //            method: 'Post',
    //            headers: {
    //             Accept: 'application/json',
    //             //'Content-Type': 'application/json',
    //           },
    //           body: JSON.stringify(data),}).then((res) => {return res.json();});
    //   },

    RegisterComplaint(vHnPh,Name,SName,vadharno,ANo,Email,vDist,vBlk,vGP,Add,UrbanRural,Dep,Atri,cdistcode,cgpcode,cblockcode,
        vRM,vWd,vG,vZ,vT,vR,vSD,vD,vC,vV,vHn,vI,Comp_GPCode,vTeh){
            
      //  var url = URL + '/api-OWAservices.asmx/RegisterNewComplaint?ANo='+ANo+'&&Add='+Add+'&&Atri='+Atri+'&&Comp_GPCode='+Comp_GPCode+'&&Dep='+Dep+'&&Email='+Email+'&&Name='+Name+'&&SName='+SName+'&&UrbanRural='+UrbanRural+'&&cblockcode='+cblockcode+'&&cdistcode='+cdistcode+'&&cgpcode='+cgpcode+'&&vBlk='+vBlk+'&&vC='+vC+'&&vD='+vD+'&&vDist='+vDist+'&&vG='+vG+'&&vGP='+vGP+'&&vHn='+vHn+'&&vHnPh='+vHnPh+'&&vI='+vI+'&&vR='+vR+'&&vReMark='+vRM+'&&vSD='+vSD+'&&vT='+vT+'&&vV='+vV+'&&vWd='+vWd+'&&vZ='+vZ+'&&vadharno='+vadharno+'&&vTeh='+vTeh;
      
      var url = URL + '/api-OWAservices.asmx/RegisterNewComplaint?Name='+Name+'&&SName='+SName+'&&ANo='+ANo+'&&Email='+Email+'&&Add='+Add+'&&Atri='+Atri+'&&Dep='+Dep+'&&vDist='+vDist+'&&vTeh='+vTeh+'&&vBlk='+vBlk+'&&vWd='+vWd+'&&vGP='+vGP+'&&vG='+vG+'&&vZ='+vZ+'&&vT='+vT+'&&vR='+vR+'&&vSD='+vSD+'&&vD='+vD+'&&vC='+vC+'&&vV='+vV+'&&vReMark='+vRM+'&&vHn='+vHn+'&&vHnPh='+vHnPh+'&&vI='+vI+'&&cdistcode='+cdistcode+'&&cblockcode='+cblockcode+'&&cgpcode='+cgpcode+'&&vadharno='+vadharno+'&&UrbanRural='+UrbanRural+'&&Comp_GPCode='+Comp_GPCode;
      console.log(url);
        return fetch( url , {
               method: 'GET',
               headers: {
               
              },
            }).then((res) => {return res.json();console.log(res.json())});
      },

      getUserDetailsByPhoneNo(phoneno){
        var url = URL + '/api-OWAservices.asmx/GetCallerDetailsByPhoneNo?phoneNo='+phoneno;
        return fetch( url , {
                method: 'GET',
                headers: {
                
               },
            }).then((res) => {return res.json();});
      },
    
    dashboardCount(mobileno){
        var url = URL + '/Citizen-api.asmx/GetCitizenComplaintCount?mobileno='+mobileno;
        console.log(url);
        return fetch(url, {
           method: 'GET',
        }).then((response) =>{return response.json()});
    },

    problemStatus(CallerId,status){
       
        var url = URL + '/Citizen-api.asmx/GetCitizenComplaintByStatusRemark?CallerId='+CallerId+'&&StatusRemark='+status;
        console.log("hitting: ",url);
        return fetch(url, {
            method: 'GET',
        }).then((response) => {console.log(response);return response.json()});
    },
   
    CitizenRemark(Compid,Remarks){
         
        var url = URL + '/Citizen-api.asmx/AddComplaintSummary_InCitizenAppByCitizen?Compid='+Compid+'&&Remarks='+Remarks;
        console.log("hitting: ",url);
            return fetch(url, {
                 method: 'GET',
            }).then((response) => {console.log(response);return response.json()});
    },

    GetCompDetailsByCompid(Compid){
        var url = URL + '/api-details.asmx/details?tid='+Compid;
        console.log("hitting: ",url);
            return fetch(url, {
                method: 'GET',
            }).then((response) => {console.log(response);return response.json()});
    },

    GetCallerInforByCalling(Mobile_No,C_Name,C_Email,Comp_Type){
        var url = URL + '/Citizen-api.asmx/AddInfoCallers_in_Waiting?Mobile_No='+Mobile_No+'&&C_Name='+C_Name+'&&C_Email='+C_Email+'&&Comp_Type='+Comp_Type;
        return fetch(url, {
          method: 'GET',
        }).then((response) =>{return response.json()});
    },

    ForSatisfyUnsatisfyPC(compid,mobileno,buttontype){
        var url = URL + '/Citizen-api.asmx/ForSatisfyUnsatisfyPC_CompFromcitizenApp?compid='+compid+'&&mobileno='+mobileno+'&&buttontype='+buttontype;
        return fetch(url, {
            method: 'GET',
        }).then((response) =>{console.log(response);return response.json()});
    },

    AddRegister_CitizenAppmobileuser(deviceudid,mobileno){
        console.log("mobileno",mobileno);
        var url = URL + '/Citizen-api.asmx/AddRegister_CitizenAppmobileuser?deviceudid='+deviceudid+'&&mobileno='+mobileno;
        console.log(url);
        return fetch(url, {
            method: 'GET',
        }).then((response) =>{console.log(response);return response.json()});
    }
}

module.exports = cmAPI;