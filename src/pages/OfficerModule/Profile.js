import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Dimensions,
  BackHandler,
  AsyncStorage,
  Keyboard,
  Modal,
  ScrollView, Alert, StatusBar
} from 'react-native';
import { ImageBackground, Spinner, Lightbox, NavigationBar, Text, View, Tile, Title, TextInput, Row, Col, Card, Button, Icon, Divider, Caption, Subtitle, Image, ListView, TouchableOpacity } from '@shoutem/ui';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Apis from '../../apis/Api';


export default class Profile extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      userDetail: [],
      username: '',
      name: '',
      password: '',
      designation: '',
      district: '',
      loading: false,
      mobile_no: '',
      DistHeadquarter: 1
    };
  }

  setSpinner = (st) => {
    this.setState({ loading: st });
  }

  componentDidMount() {


    AsyncStorage.getItem("userDetail").then((value) => {

      console.log('this.state.userCountDetail');
      console.log(JSON.parse(value));
      this.setState({
        userDetail: JSON.parse(value),
        mobileno: JSON.parse(value).officerno,
        name: JSON.parse(value).name,
        username:JSON.parse(value).loginuserid,
        password:JSON.parse(value).loginpassword,
        designation: JSON.parse(value).Designation,
        DistHeadquarter: JSON.parse(value).DistHeadquarter
        // district:JSON.parse(value).
      });



    });


   
  }


  UpdateProfile = () => {
    var officerid = this.state.userDetail.id, name = this.state.name, newPwd = this.state.password, designation = this.state.designation, phone_no = this.state.mobileno, DistHeadquarter = this.state.DistHeadquarter;
   console.log('officerid, name, newPwd, designation, phone_no, DistHeadquarter',officerid, name, newPwd, designation, phone_no, DistHeadquarter)
    Apis.UpdateProfile(officerid, name, newPwd, designation, phone_no, DistHeadquarter).then((res) => {
      this.setSpinner(false);
      console.log('res comp list by attri and dist');
      console.log(res);


      if (res.status == 'success') {
        this.setState({Remark:''})
        Alert.alert(
          'Alert',
          ' प्रोफाइल को अपडेट कर दिया गया है। कृपया पुनः लॉग इन करें !',
          [
            // { text: 'Cancel', onPress: () => this.setSpinner(false) },
            { text: 'OK', onPress: () => { this.props.navigation.navigate('Logout') } },
            //{text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          { cancelable: false }
        )

       
      }


    }, (error) => {

      console.log("error");
      console.log(error);
      this.setSpinner(false);
      // this.setSpinner(false);
      //alert('Please check internet connection.');
    });
  }

  ConfirmationToLogout = () => {

    Alert.alert(
      'Alert',
      'क्या आप लॉगआउट करना चाहते हैं ?',
      [
        { text: 'Cancel', onPress: () => this.setSpinner(false) },
        { text: 'OK', onPress: () => this.props.navigation.navigate('Logout') },

      ],
      { cancelable: false }
    )

  }


  render() {
    return (


      <View style={{ backgroundColor: '#05ADEE', flex: 1 }}>

<StatusBar
          barStyle="dark-content"
          backgroundColor="#117bc7"
        />

        <NavigationBar style={{ container: { borderBottomColor: '#117bc7', zIndex: 0, backgroundColor: '#117bc7', color: '#fff', height: 40 } }}

          leftComponent=
          {<View styleName="horizontal" >
            <Button styleName="clear" style={{ backgroundColor: '#117bc7' }}>
              <Icon name="left-arrow" style={{ fontSize: 35, color: '#fff' }}
                onPress={() => this.props.navigation.goBack()} />
            </Button>

          </View>}
          centerComponent={
            <View styleName="horizontal" >

              <Title style={{ alignSelf: 'center', backgroundColor: '#117bc7', fontSize: 20, color: '#fff' }} >प्रोफ़ाइल</Title>

            </View>
          }
          rightComponent={
            <View styleName="horizontal" >
              <Button styleName="clear" style={{ backgroundColor: '#117bc7' }}>
                <MaterialCommunityIcons name="logout" style={{ fontSize: 25, color: '#fff',marginRight:10 }}
                  onPress={() => this.ConfirmationToLogout()} />
              </Button>

            </View>
          }



        />
        <View
          style={{ backgroundColor: '#05ADEE', marginTop: '12%' }}>
        {/* {this.state.userDetail.ProfileImage=="" ? */}
          <Image style={{ alignSelf: 'center', height: 100, width: 100, borderRadius: 100 }}


            source={require('../../assets/Image/default.png')}
          />
          <Text style={{alignSelf:'center',color:'#fff'}}>{this.state.username}</Text>
        </View>
        <View
          style={{ backgroundColor: '#05ADEE', height: '70%', marginTop:10}}>



          {/* </View> */}

          <Card style={{ alignSelf: 'center', width: '90%', borderRadius: 5, height: '80%' }} >

            {/* <ScrollView style={{ width: 300 }}> */}

              {/* <Text style={{ left: 25, marginTop: 15 }} >यूजर नेम </Text>
              <TextInput style={{ height: 40, paddingBottom: 8, paddingTop: 8, alignSelf: 'center', marginLeft: 10, color: 'black', borderRadius: 5, width: '90%' }}
                value={this.state.username}
                placeholder={'यूजर नेम '}
                editable={false} 
                selectTextOnFocus={false}
                placeholderTextColor='#fff'
                ref={input => { this.textInput = input }}
               
              />
              <Divider styleName="line" style={{ borderBottomWidth: 1, borderBottomColor: '#E6E6E6', width: '90%', marginLeft: '3%', bottom: 5 }} /> */}

              {/* <Text style={{ left: 25 }}>पासवर्ड</Text> */}
              {/* <TextInput style={{ height: 40, paddingBottom: 2, paddingTop: 2, alignSelf: 'center', marginLeft: 10, color: 'black', borderRadius: 5, width: '90%' }}
                placeholder={'1254 '}
                placeholderTextColor='#fff'
                secureTextEntry={true}
                editable={false} 
                selectTextOnFocus={false}
                value={this.state.password}
                ref={input => { this.textInput = input }}
                // onChangeText={(text) => this.setState({ password: text })}
              /> */}

{/* <Text style={{ left: 25,paddingTop:4,paddingBottom:4 }}>{this.state.password}</Text> */}
              {/* <Divider styleName="line" style={{ borderBottomWidth: 1, borderBottomColor: '#E6E6E6', width: '90%', marginLeft: '3%', bottom: 5 }} /> */}
              <Text style={{ left: 25 }}>मोबाइल नंबर</Text>
              {/* <TextInput style={{ height: 40, paddingBottom: 2, paddingTop: 2, alignSelf: 'center', marginLeft: 10, color: 'black', borderRadius: 5, width: '90%' }}
                placeholder={'0000000000'}
                value={this.state.mobileno}
                placeholderTextColor='#fff'
                ref={input => { this.textInput = input }}
                // onChangeText={(text) => this.setState({ mobileno: text })}
              /> */}

<Text style={{ left: 25,paddingTop:4,paddingBottom:7 }}>{this.state.mobileno}</Text>
              <Divider styleName="line" style={{ borderBottomWidth: 1, borderBottomColor: '#E6E6E6', width: '90%', marginLeft: '3%', bottom: 5 }} />
              <Text style={{ left: 25 }}> नाम</Text>
              {/* <TextInput style={{ height: 40, paddingBottom: 2, paddingTop: 2, alignSelf: 'center', marginLeft: 10, color: 'black', borderRadius: 5, width: '90%' }}
                placeholder={'नाम'}
                value={this.state.name}
                placeholderTextColor='#fff'
                ref={input => { this.textInput = input }}
                // onChangeText={(text) => this.setState({ name: text })}
              /> */}

<Text style={{ left: 25,paddingTop:4,paddingBottom:7 }}>{this.state.name}</Text>

              <Divider styleName="line" style={{ borderBottomWidth: 1, borderBottomColor: '#E6E6E6', width: '90%', marginLeft: '3%', bottom: 5 }} />
              <Text style={{ left: 25 }}> पद </Text>
              {/* <TextInput style={{ height: 40, paddingBottom: 2, paddingTop: 2, alignSelf: 'center', marginLeft: 10, color: 'black', borderRadius: 5, width: '90%' }}
                placeholder={'पद'}
                value={this.state.designation}
                placeholderTextColor='#fff'
                ref={input => { this.textInput = input }}
                // onChangeText={(text) => this.setState({ designation: text })}
              /> */}


<Text style={{ left: 25,paddingTop:4,paddingBottom:7 }}>{this.state.designation}</Text>
              <Divider styleName="line" style={{ borderBottomWidth: 1, borderBottomColor: '#E6E6E6', width: '90%', marginLeft: '3%', bottom: 5 }} />

              <Text style={{ left: 25 }} > स्थान</Text>
              {/* <TextInput style={{ height: 40, paddingBottom: 2, paddingTop: 2, alignSelf: 'center', marginLeft: 10, color: 'black', borderRadius: 5, width: '90%' }}
                placeholder={'स्थान'}
                value={this.state.DistHeadquarter}
                placeholderTextColor='#fff'
                ref={input => { this.textInput = input }}
                // onChangeText={(text) => this.setState({ DistHeadquarter: text })}
              /> */}
<Text style={{ left: 25,paddingTop:4,paddingBottom:7 }}>{this.state.DistHeadquarter}</Text>
<Divider styleName="line" style={{ borderBottomWidth: 1, borderBottomColor: '#E6E6E6', width: '90%', marginLeft: '3%', bottom: 5 }} /> 
              {/* <Button style={{
                backgroundColor: '#05B2F4', borderRadius: 5, borderWidth: 2, fontFamily: 'CeraPRO-Bold', borderColor: '#fff', bottom: 5,
                alignSelf: 'center', width: '95%', height: 50
              }}  >
                <Text style={{ color: '#fff', fontSize: 20 }}>सेव करे</Text>
              </Button> */}


              {/* </View> */}
            {/* </ScrollView> */}
          </Card>

        </View>






      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  borderview: {
    borderBottomColor: '#05ADEE',
    borderBottomWidth: 115,
    backgroundColor: '#05ADEE'

  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});