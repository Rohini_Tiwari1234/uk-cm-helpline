import React, { Component, Animated } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Dimensions,
    BackHandler,
    AsyncStorage,
    DatePickerAndroid,
    TimePickerAndroid,
    Keyboard, Alert, NetInfo,
    Modal, FlatList, RefreshControl, ScrollView
} from 'react-native';
import { NavigationBar, Touchable, Spinner, DropDownMenu, Title, Tile, Divider, Overlay, Screen, Subtitle, Caption, Button, ImageBackground, Text, Icon, Image, TextInput, View, Row, TouchableOpacity, Card } from '@shoutem/ui';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Apis from '../../apis/Api';
import Communications from 'react-native-communications';

const getItemLayout = (data, index) => (
    { length: data.length, offset: data.length * index, index }
);


export default class DistrictWiseList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            masterDistrictWiseCount: [],
            DistrictWiseCount: [],
            loading: false,
            status: '',
            netstatus: '',
            spinner: false,
            Officerid: '',
            ComplaintCount: 0,
            refreshing: false,
            StatusText: '',
            AttributeId:0,
            filter:0
        };
        this.renderRow = this.renderRow.bind(this);
    }

    setSpinner = (st) => {
        this.setState({ loading: st });
    }
    componentDidMount() {
        this.setSpinner(true);

        var status = this.props.navigation.getParam('Status');
        var OfficerId = this.props.navigation.getParam('OfficerId');
        var StatusText = this.props.navigation.getParam('StatusText');
        var AttributeId = this.props.navigation.getParam('AttributeId');
        var filter = this.props.navigation.getParam('filter');
        this.setState({ StatusText: StatusText,AttributeId:AttributeId })
        var distid = 0;
        console.log('compid,filter');
        console.log(status, OfficerId,filter);

        this.setState({
            Officerid: OfficerId, status: status,filter:filter
        })
        if(filter==0){
        Apis.GetCountAtrributeOrDistrictwise(OfficerId, AttributeId,distid, status).then((res) => {
            //   this.setSpinner(true);
            console.log('res');
            console.log(res);
            this.setSpinner(false);


            if (res.status == 'success') {
                this.setState({
                   masterDistrictWiseCount: res.messages, DistrictWiseCount: res.messages
                });
                this.getCompCount(res.messages);

            }


        }, (error) => {

            console.log("error");
            console.log(error);
            this.setSpinner(false);
            // this.setSpinner(false);
            //alert('Please check internet connection.');
        });
    }
    else{
        if(filter==1){
        Apis.GetCountAtrributeOrDistrictwiseTL(filter,OfficerId,AttributeId,distid).then((res) => {
            //   this.setSpinner(true);
            console.log('res GetCountAtrributeAndDistrictwiseTL');
            console.log(res);
            this.setSpinner(false);
            if (res.status == 'success') {
                this.setState({
                    masterDistrictWiseCount: res.messages, DistrictWiseCount: res.messages
                 });
                 this.getCompCount(res.messages);

            }

        }, (error) => {

            console.log("error");
            console.log(error);
            this.setSpinner(false);
            // this.setSpinner(false);
            //alert('Please check internet connection.');
        });
    }
    else if(filter==2){
        Apis.GetCountAtrributeOrDistrictwiseTL(filter,OfficerId,AttributeId,distid).then((res) => {
            //   this.setSpinner(true);
            console.log('res GetCountAtrributeOrDistrictwiseTL');
            console.log(res);
            this.setSpinner(false);
            if (res.status == 'success') {
                this.setState({
                    masterDistrictWiseCount: res.messages, DistrictWiseCount: res.messages
                 });
                 this.getCompCount(res.messages);

            }

        }, (error) => {

            console.log("error");
            console.log(error);
            this.setSpinner(false);
            // this.setSpinner(false);
            //alert('Please check internet connection.');
        });
    }
    else{
        Apis.GetCountAtrributeOrDistrictwiseTL(filter,OfficerId,AttributeId,distid).then((res) => {
            //   this.setSpinner(true);
            console.log('res GetCountAtrributeAndDistrictwiseTL');
            console.log(res);
            this.setSpinner(false);
            this.setState({
                masterDistrictWiseCount: res.messages, DistrictWiseCount: res.messages
             });
             this.getCompCount(res.messages);

        }, (error) => {

            console.log("error");
            console.log(error);
            this.setSpinner(false);
            // this.setSpinner(false);
            //alert('Please check internet connection.');
        });
    }
    }

    }

    getCompCount = (datalist) => {
        var totalcount = 0;
        var list = datalist;
        list.map((item, index) => {
            totalcount = item.ComplaintCount + totalcount
        })
        this.setState({ ComplaintCount: totalcount })
        console.log('total count', totalcount);
    }

    refreshControl() {
        var distid = 0;
        if(this.state.filter==0){
        Apis.GetCountAtrributeOrDistrictwise(this.state.OfficerId, this.state.AttributeId,distid, this.state.status).then((res) => {
            //   this.setSpinner(true);
            console.log('res');
            console.log(res);
            this.setSpinner(false);


            if (res.status == 'success') {
                this.setState({
                    masterDistrictWiseCount: res.messages, DistrictWiseCount: res.messages
                });
                this.getCompCount(res.messages);

            }


        }, (error) => {

            console.log("error");
            console.log(error);
            this.setSpinner(false);
            // this.setSpinner(false);
            //alert('Please check internet connection.');
        });
    }
    else{
    if(this.state.filter==1){
    Apis.GetCountAtrributeOrDistrictwiseTL(this.state.filter,this.state.OfficerId, this.state.AttributeId,distid).then((res) => {
        //   this.setSpinner(true);
        console.log('res GetCountAtrributeAndDistrictwiseTL');
        console.log(res);
        this.setSpinner(false);
        if (res.status == 'success') {
            this.setState({
                masterDistrictWiseCount: res.messages, DistrictWiseCount: res.messages
             });
             this.getCompCount(res.messages);

        }

    }, (error) => {

        console.log("error");
        console.log(error);
        this.setSpinner(false);
        // this.setSpinner(false);
        //alert('Please check internet connection.');
    });
}
else if(this.state.filter==2){
    Apis.GetCountAtrributeOrDistrictwiseTL(this.state.filter,this.state.OfficerId, this.state.AttributeId,distid).then((res) => {
        //   this.setSpinner(true);
        console.log('res GetCountAtrributeAndDistrictwiseTL');
        console.log(res);
        this.setSpinner(false);
        if (res.status == 'success') {
            this.setState({
                masterDistrictWiseCount: res.messages, DistrictWiseCount: res.messages
             });
             this.getCompCount(res.messages);

        }

    }, (error) => {

        console.log("error");
        console.log(error);
        this.setSpinner(false);
        // this.setSpinner(false);
        //alert('Please check internet connection.');
    });
}
else{
    Apis.GetCountAtrributeOrDistrictwiseTL(this.state.filter,this.state.OfficerId, this.state.AttributeId,distid).then((res) => {
        //   this.setSpinner(true);
        console.log('res GetCountAtrributeAndDistrictwiseTL');
        console.log(res);
        this.setSpinner(false);
        if (res.status == 'success') {
            this.setState({
                masterDistrictWiseCount: res.messages, DistrictWiseCount: res.messages
             });
             this.getCompCount(res.messages);

        }

    }, (error) => {

        console.log("error");
        console.log(error);
        this.setSpinner(false);
        // this.setSpinner(false);
        //alert('Please check internet connection.');
    });
}
    }
}
  

    _keyExtractor = (item, index) => index;

    renderRow(districtListData) {
        var districtList = districtListData.item;
       console.log('districtList',districtList)
        return (
            <Touchable  onPress={()=>this.props.navigation.navigate('CompList',{Status:this.state.status,OfficerId:this.state.Officerid,AttributeId:this.state.AttributeId,Districtid:districtList.Districtcode,StatusText:this.state.StatusText,filter:this.state.filter})} >

                <View style={styles.Card} >


                    <View styleName="horizontal space-between" style={{ backgroundColor: '#E6E6E6', marginTop: 5 }}>

                        {/* <Text style={styles.Subtitle2}>{restaurants.name}</Text> */}
                        <Text style={styles.Subtitle2}>जिला</Text>
                        <Subtitle style={{
                            fontSize: 18, color: '#696969', marginRight: 5
                        }}>शिकायत संख्या :  <Text style={{ color: 'red', marginRight: 5, fontWeight: '500' }}>{districtList.ComplaintCount}</Text></Subtitle>


                    </View>
                    <View  >
                        <View styleName="horizontal h-start" style={{ paddingTop: 0, paddingBottom: 0, paddingLeft: 0, paddingRight: 0 }}>
                            <Title style={styles.Subtitle}>{districtList.DistrictName}</Title>



                        </View>


                        <Divider styleName="line" style={{ marginTop: 10, borderBottomWidth: 2, borderBottomColor: '#E6E6E6', width: '95%', marginLeft: '3%', bottom: 5 }} />
                    </View>

                </View>
            </Touchable>

        );
    }

    loadPosts = () => {
        this.setState({ loading: false })
    }
  

   
    render() {
        // const snaglist = this.state.filteredSnaglist.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS));
       
        const DistrictWiseCountList = this.state.DistrictWiseCount;
        return (
            <View style={{ flex: 1 }}>

<NavigationBar style={{ container: { zIndex: 1,height:40, backgroundColor: '#117bc7', color: '#fff' } }}

leftComponent=
{<Touchable onPress={() => this.props.navigation.goBack()} >
    < Icon style={{ color: '#fff', fontSize: 30,marginLeft:10 }} name="left-arrow" />
    </Touchable>
}
centerComponent={
    <Title style={styles.navigationBarTitleColor} >{this.state.StatusText}  </Title>
}

rightComponent={(
    <Touchable onPress={() => this.props.navigation.navigate('Dashboard')} >
        < MaterialCommunityIcons style={{marginRight:10, color: '#fff', fontSize: 30 }} name="home" />
    </Touchable>
)}
/>
                   

                <ScrollView
                    refreshControl={
                        <RefreshControl
                            onRefresh={() => this.refreshControl()}
                            refreshing={this.state.refreshing}
                            enabled={true}

                        />
                    }
                >

                    <View style={{marginTop:'10%'}} >

                        {DistrictWiseCountList.length > 0 ?
                            <FlatList

                                data={DistrictWiseCountList}
                                renderItem={this.renderRow}
                                extraData={this.state.masterDistrictWiseCount}
                                keyExtractor={this._keyExtractor}
                                maxToRenderPerBatch={5}
                                getItemLayout={getItemLayout}
                                onEndReached={this.loadPosts.bind(this)}
                                // onEndReachedThreshold={1}
                                ListFooterComponent={this.state.loading ? <Spinner /> : null}
                                progressViewOffset={15}

                            />
                            :
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', paddingTop: '50%' }}   >
                                <Text>शिकायत मौजूद नहीं है !</Text>
                            </View>
                        }

                    </View>
                </ScrollView>
{this.state.loading ?
    <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)', alignItems: 'center', justifyContent: 'center', position: 'absolute', width: '100%', height: '100%' }}>
        <Spinner color="white" />
    </View> : null}

            </View>
        );
    }
}



const styles = {
    navigationBarTitleColor: {
        fontSize: 16,
        color: '#fff'
    },
    Card: {
        width: '95%',
        margin: 5,
        paddingLeft: 10,
        paddingRight: 10
    },
    Subtitle2: {
        fontSize: 17,
        color: '#696969',
        paddingLeft: 5

    },

    Subtitle: {
        paddingLeft: 10,
        fontSize: 15,
        // color: '#F7941F',
        paddingTop: 10
    }

}