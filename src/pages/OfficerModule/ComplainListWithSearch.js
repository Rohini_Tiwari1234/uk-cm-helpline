
import { StyleSheet, Modal, AsyncStorage,FlatList } from 'react-native';
import React, { Component } from 'react';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { NavigationBar,Touchable,TextInput, Title, Button, Divider,  Subtitle,  Text, Icon, View,TouchableOpacity } from '@shoutem/ui';
import Communications from 'react-native-communications';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Apis from '../../apis/Api';

export default class complainlistwithsearch extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      masterCompList: [],
      CompList:[],
      CompId:0,
      userDetail:[],
      Message:''
    }
  }

  componentDidMount() {
    
    AsyncStorage.getItem("userDetail").then((value) => {

        console.log('this.state.userCountDetail');
        console.log(JSON.parse(value).name);
        this.setState({
            userDetail: JSON.parse(value)
        });
      });
  }
 

SearchCompList=()=>{
  var OfficerId = this.state.userDetail.id;
  console.log('OfficerId',OfficerId);
if(this.state.CompId!=null && this.state.CompId!='' && this.state.CompId!=0){
  Apis.GetComplaintByCompidAndOfficerid(OfficerId, this.state.CompId).then((res) => {
      //   this.setSpinner(true);
      console.log('res search comp list ');
      console.log(res,res.messages);


      if (res.status == 'success') {
         if(res.messages.length!=0){
          this.setState({
              CompList: res.messages[0].Complaint_detail, masterCompList: res.messages[0].Complaint_detail
          });
        }
        else{
            this.setState({
                Message:"शिकायत मौजूद नहीं है !",CompList:null
            });
          }
      }
    


  }, (error) => {

      console.log("error");
      console.log(error);
     
      // this.setSpinner(false);
      //alert('Please check internet connection.');
  });
}
  else{
    this.setState({
        Message:"कृपया शिकायत क्रमांक दर्ज करें !",CompList:null
    });
}

}



    render(){
     
      console.log("complaint:", this.state.CompList)
        return (
            <View style={{flex:1}}>

<NavigationBar style={{ container: {borderBottomColor:'#117bc7', zIndex: 0, backgroundColor: '#117bc7', color: '#fff',height:50  } }}

leftComponent=
{<Touchable onPress={() => this.props.navigation.goBack()} >
    < Icon style={{color:'#fff',fontSize:30,marginLeft:10}} name="left-arrow" />
    </Touchable>
}
  centerComponent={
    <Title style={styles.navigationBarTitleColor} > शिकायतें खोंजे </Title>
  }

rightComponent={(
  <Touchable onPress={() => this.props.navigation.navigate('Dashboard')} >
  < MaterialCommunityIcons style={{ color: '#fff', fontSize: 30,marginRight:10 }} name="home" />
</Touchable>
)}
/>
<View styleName="horizontal"style={{marginTop:50,marginLeft:30}} >

<TextInput
       style={styles.TextInputStyleClass}
       placeholder="Search "
        keyboardType="numeric"
       ref={input => { this.textInput = input }}
       value={this.state.CompId}
       onChangeText={(text) => { this.setState({CompId:text});console.log('compid',text) }}
    >

    </TextInput>
    <Touchable onPress={()=>this.SearchCompList()} >
   <View style={{backgroundColor:'#117bc7',bottom:5,width:50,height:40,right:10}} >
    <Icon name="search" style={{color:'#fff',marginTop:10}}  />
   </View>
   </Touchable>
   

</View>

{this.state.CompList!=null && this.state.CompList!='' && this.state.CompList.length!=0 ?
              <View style={styles.Card}>
              <View styleName="horizontal space-between" style={{ backgroundColor: '#E6E6E6' }}>
                  <Text style={styles.Subtitle2}>क्रमांक :<Text >{this.state.CompList.compid}</Text></Text>
                  <Subtitle >दिनांक :<Text>{this.state.CompList.compdate}</Text></Subtitle>
    
              </View>
    
              <View styleName="horizontal h-start" >
    
    
                  <Title style={styles.Subtitle}>नाम :</Title>
                  <Title style={styles.Subtitle}>{this.state.CompList.callername} {this.state.CompList.callersurname}</Title>
              </View>
    
              <View styleName="horizontal h-start" >
                  {/* <Title style={styles.Subtitle}>{restaurants.address}</Title> */}
    
                  <Title style={styles.Subtitle}>फ़ोन नम्बर :</Title>
                  <View styleName="horizontal space-between" >
                      <Title style={styles.Subtitle}>{this.state.CompList.phoneno}</Title>
                      <Touchable onPress={() => Communications.phonecall(this.state.CompList.phoneno, true)} >
                      <Icon name="call" style={{ alignSelf: 'center', height: 30, width: 30, borderRadius: 100, marginLeft: 80, backgroundColor: 'green', color: '#fff' }} />
                      </Touchable>
                  </View>
              </View>
              <View styleName="horizontal h-start" >
                  <Title style={styles.Subtitle}>शिकायत का विवरण :</Title>
    
              </View>
              <Title style={{ color: '#B4B4B4', fontSize: 17, left: 5 }}>{this.state.CompList.compremarks}</Title>
    
    
              <Button style={{ left: 15, backgroundColor: '#05B2F4', borderRadius: 5, borderWidth: 2, fontFamily: 'CeraPRO-Bold', borderColor: '#fff', marginTop: 5, width: '95%', height: 40 }} onPress={() => this.props.navigation.navigate('CompDetail', { CompDetail: this.state.CompList,CompId: this.state.CompList.compid, Officerid:this.state.userDetail.id })}>
                  <View styleName="horizontal space-between">
                      <Text style={{ color: '#fff', marginLeft: '60%' }}>विवरण देखें </Text>
                      <FontAwesome name="arrow-right" style={{ fontSize: 20, color: '#fff' }} />
    
                  </View>
              </Button>
    
    
    
              <Divider styleName="line" style={{ marginTop: 3, borderBottomWidth: 2, borderBottomColor: '#E6E6E6', width: '95%', marginLeft: '3%' }} />
    
    
    
          </View>
          :
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', paddingTop: '30%' }}   >
          <Text>{this.state.Message}</Text>
      </View>}

               </View>
        );
    }
}



const styles = {

    
  Card: {
    width: '95%',
    margin: 5,
    paddingLeft: 10,
    paddingRight: 10
},


navigationBarTitleColor: {
    fontSize: 20,
    color: '#fff'
},
View: {
    width: '95%',

},
Subtitle2: {
    fontSize: 18,
    // fontFamily: 'CeraPro-Bold',
    //  color: '#B3B3B3',
    fontWeight: '500',
    //    paddingLeft:3,

    marginLeft: 5


},

Subtitle: {
    paddingLeft: 10,
    fontSize: 15,
    // color: '#F7941F',
    paddingTop: 5
},

    
TextInputStyleClass: {
borderWidth:2,

 borderColor:'#B3B3B3',
  height: 40,
  width: '80%',
  // height: 40,
  padding: 5,
  // fontSize: 15,
  // marginRight: 10,
  // width: '90%',
  margin: 5
  // borderRadius: 3
}



}