
import { StyleSheet, Modal, AsyncStorage, FlatList, RefreshControl, ScrollView } from 'react-native';
import React, { Component } from 'react';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { NavigationBar,Spinner, Touchable, Image, Title, Button, Divider, Subtitle, Text, Icon, View, TouchableOpacity } from '@shoutem/ui';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Apis from '../../apis/Api';
import Communications from 'react-native-communications';


const getItemLayout = (data, index) => (
    { length: data.length, offset: data.length * index, index }
);


export default class ComplainList extends Component {
    constructor(props) {
        super(props);
        this.renderRow = this.renderRow.bind(this);
        this.state = {
            masterCompList: [],
            CompList: [],
            compCount: 0,
            Officerid: '',
            status: '',
            StatusText: '',
            AttributeId: 0,
            Districtid: 0,
            refreshing: false,
            firstpage:1,
            nextpage: 10,
            loading:false,
            filter:0,
            fPage:0
        }
    }

    setSpinner = (st) => {
        this.setState({ loading: st });
    }

    componentDidMount() {

        this.setSpinner(true);
        var status = this.props.navigation.getParam('Status');
        var OfficerId = this.props.navigation.getParam('OfficerId');
        var StatusText = this.props.navigation.getParam('StatusText');
        var AttributeId = this.props.navigation.getParam('AttributeId');
        var Districtid = this.props.navigation.getParam('Districtid');
        var filter = this.props.navigation.getParam('filter');

        this.setState({ StatusText: StatusText, AttributeId: AttributeId, Districtid: Districtid })
        console.log('compid');
        console.log(status, OfficerId,filter);

        this.setState({
            Officerid: OfficerId, status: status,filter:filter
        })
        if(filter==0){
        Apis.compdetailAttributeAndDistrictwise(OfficerId, AttributeId, Districtid, this.state.firstpage, this.state.nextpage, status).then((res) => {
            //   this.setSpinner(true);
            console.log('res comp list by attri and dist');
            console.log(res);


            if (res.status == 'success') {
                this.setState({ compCount: res.messages.total_tickets, CompList: res.messages.user_tickets, masterCompList: res.messages.user_tickets }, () => {
                    this.setState({fPage:this.state.firstpage, firstpage: this.state.firstpage+1, nextpage: this.state.nextpage })
                })
                // this.setState({
                //     compCount: res.messages.total_tickets, CompList: res.messages.user_tickets, masterCompList: res.messages.user_tickets
                // });
                this.setSpinner(false);
            }
            else{
                this.setSpinner(false);
            }


        }, (error) => {

            console.log("error");
            console.log(error);
            this.setSpinner(false);
            // this.setSpinner(false);
            //alert('Please check internet connection.');
        });
    }
    else{
        Apis.compdetailAttributeAndDistrictwiseTL(OfficerId, AttributeId, filter,  this.state.firstpage, this.state.nextpage,Districtid).then((res) => {
            //   this.setSpinner(true);
            console.log('res comp list by compdetailAttributewiseTL');
            console.log(res);


            if (res.status == 'success') {
                this.setState({ compCount: res.messages.total_tickets, CompList: res.messages.user_tickets, masterCompList: res.messages.user_tickets }, () => {
                    this.setState({fPage:this.state.firstpage, firstpage: this.state.firstpage + 1, nextpage: this.state.nextpage  })
                })
                this.setSpinner(false);
            }


        }, (error) => {

            console.log("error");
            console.log(error);
            this.setSpinner(false);
            // this.setSpinner(false);
            //alert('Please check internet connection.');
        });
    }

    }

    refreshControl() {
        this.setState({ firstpage: 1, nextpage: 10 })
        if(this.state.filter==0){
        console.log('this.state.firstpage', this.state.firstpage,this.state.fPage)
        if (this.state.firstpage == 1) {
            Apis.compdetailAttributeAndDistrictwise(this.state.Officerid, this.state.AttributeId, this.state.Districtid, this.state.firstpage, this.state.nextpage, this.state.status).then((res) => {
                //   this.setSpinner(true);
                console.log('res comp list by attri and dist');
                console.log(res);

                if (res.status == 'success') {
                    this.setState({ compCount: res.messages.total_tickets, CompList: res.messages.user_tickets, masterCompList: res.messages.user_tickets }, () => {
                        this.setState({ fPage:this.state.firstpage,firstpage: this.state.firstpage + 1, nextpage: this.state.nextpage  })
                    })
                    this.setSpinner(false);
                }


            }, (error) => {

                console.log("error");
                console.log(error);
                this.setSpinner(false);
                // this.setSpinner(false);
                //alert('Please check internet connection.');
            });
        }
    }
    else{
        Apis.compdetailAttributeAndDistrictwiseTL(this.state.Officerid, this.state.AttributeId, this.state.filter, this.state.firstpage, this.state.nextpage,this.state.Districtid).then((res) => {
            //   this.setSpinner(true);
            console.log('res comp list by compdetailAttributewiseTL');
            console.log(res);


            if (res.status == 'success') {
                this.setState({ compCount: res.messages.total_tickets, CompList: res.messages.user_tickets, masterCompList: res.messages.user_tickets }, () => {
                    this.setState({ fPage:this.state.firstpage,firstpage: this.state.firstpage + 1, nextpage: this.state.nextpage })
                })
                this.setSpinner(false);
            }


        }, (error) => {

            console.log("error");
            console.log(error);
            this.setSpinner(false);
            // this.setSpinner(false);
            //alert('Please check internet connection.');
        });
    }
    }

    _keyExtractor = (item, index) => index;

    renderRow(complainList) {
        var complist = complainList.item;
        console.log("Snag list data", complist);

        return (

            <View style={styles.Card}>
                <View styleName="horizontal space-between" style={{ backgroundColor: '#E6E6E6' }}>
                    <Text style={styles.Subtitle2}>क्रमांक :<Text >{complist.tid}</Text></Text>
                    <Subtitle >दिनांक :<Text>{complist.compdate}</Text></Subtitle>

                </View>

                <View styleName="horizontal h-start" >


                    <Title style={styles.Subtitle}>नाम :</Title>
                    <Title style={styles.Subtitle}>{complist.callername}</Title>
                </View>

                <View styleName="horizontal h-start" >
                    {/* <Title style={styles.Subtitle}>{restaurants.address}</Title> */}

                    <Title style={styles.Subtitle}>फ़ोन नम्बर :</Title>
                    <View styleName="horizontal space-between" >
                        <Title style={styles.Subtitle}>{complist.phone}</Title>
                        <Touchable onPress={() => Communications.phonecall(complist.phone, true)} >
                        <Icon name="call" style={{ alignSelf: 'center', height: 30, width: 30, borderRadius: 100, marginLeft: 80, backgroundColor: 'green', color: '#fff' }} />
                        </Touchable>
                    </View>
                </View>
                <View styleName="horizontal h-start" >
                    <Title style={styles.Subtitle}>शिकायत का विवरण :</Title>

                </View>
                <Title style={{ color: '#B4B4B4', fontSize: 17, left: 5 }}>{complist.complaint}</Title>


                <Button style={{ left: 15, backgroundColor: '#117bc7', borderRadius: 5, borderWidth: 2, fontFamily: 'CeraPRO-Bold', borderColor: '#fff', marginTop: 5, width: '95%', height: 40 }} onPress={() => this.props.navigation.navigate('CompDetail', { CompDetail: complist,CompId: complist.tid,Officerid:this.state.Officerid })}>
                <View styleName="horizontal space-between">
                    <Text style={{ color: '#fff', marginLeft: '60%' }}>विवरण देखें </Text>
                    <FontAwesome name="arrow-right" style={{ fontSize: 20, color: '#fff' }} />

                </View>
            </Button>



                <Divider styleName="line" style={{ marginTop: 3, borderBottomWidth: 2, borderBottomColor: '#E6E6E6', width: '95%', marginLeft: '3%' }} />



            </View>





        );
    }

    loadPosts = () => {
        this.setState({ loading: false })
    }

    loadNextCompList = () => {
        console.log('this.state.firstpage',this.state.firstpage,this.state.firstpage+1,this.state.nextpage,this.state.fPage)
        this.setSpinner(true);
        if(this.state.filter==0){
        Apis.compdetailAttributeAndDistrictwise(this.state.Officerid, this.state.AttributeId, this.state.Districtid, this.state.firstpage, this.state.nextpage, this.state.status).then((res) => {
            //   this.setSpinner(true);
            console.log('res comp list by attri and dist');
            console.log(res);


            if (res.status == 'success') {
                this.setState({ compCount:  res.messages.total_tickets, CompList:  res.messages.user_tickets, masterCompList:  res.messages.user_tickets }, () => {
                    this.setState({fPage:this.state.firstpage-1 , firstpage: this.state.firstpage + 1, nextpage: this.state.nextpage })
                })
                // this.setState({
                //     compCount: res.messages.total_tickets, CompList: res.messages.user_tickets, masterCompList: res.messages.user_tickets
                // });
                this.setSpinner(false);
            }


        }, (error) => {

            console.log("error");
            console.log(error);
            this.setSpinner(false);
            // this.setSpinner(false);
            //alert('Please check internet connection.');
        });
    }
    else{
    Apis.compdetailAttributeAndDistrictwiseTL(this.state.Officerid, this.state.AttributeId, this.state.filter, this.state.firstpage, this.state.nextpage,this.state.Districtid).then((res) => {
        //   this.setSpinner(true);
        console.log('res comp list by compdetailAttributewiseTL');
        console.log(res);


        if (res.status == 'success') {
            this.setState({ compCount: res.messages.total_tickets, CompList: res.messages.user_tickets, masterCompList: res.messages.user_tickets }, () => {
                this.setState({ fPage:this.state.firstpage-1,firstpage: this.state.firstpage + 1, nextpage: this.state.nextpage  })
            })

            // this.setState({
            //     compCount: res.messages.total_tickets, CompList: res.messages.user_tickets, masterCompList: res.messages.user_tickets
            // });
            this.setSpinner(false);
        }


    }, (error) => {

        console.log("error");
        console.log(error);
        this.setSpinner(false);
        // this.setSpinner(false);
        //alert('Please check internet connection.');
    });
}

    }

    loadPreviousCompList = () => {
        console.log('this.state.firstpage',this.state.firstpage,this.state.firstpage+1,this.state.nextpage)
        this.setSpinner(true);
        console.log('fPage',this.state.fPage)
        if(this.state.filter==0){
      
        Apis.compdetailAttributeAndDistrictwise(this.state.Officerid, this.state.AttributeId, this.state.Districtid, this.state.fPage, this.state.nextpage, this.state.status).then((res) => {
            //   this.setSpinner(true);
            console.log('res comp list by attri and dist');
            console.log(res);


            if (res.status == 'success') {
                // this.setState({ compCount: [this.state.compCount, ...res.messages.total_tickets], CompList: [this.state.CompList, ...res.messages.user_tickets], masterCompList: [...this.state.masterCompList, ...res.messages.user_tickets] }, () => {
                //     this.setState({ firstpage: this.state.firstpage-1, nextpage: this.state.nextpage  })
                // })
                this.setState({ compCount:res.messages.total_tickets, CompList:res.messages.user_tickets, masterCompList:res.messages.user_tickets }, () => {
                    this.setState({fPage:this.state.firstpage, firstpage: this.state.firstpage-1, nextpage: this.state.nextpage  })
                })
                this.setSpinner(false);
            }


        }, (error) => {

            console.log("error");
            console.log(error);
            this.setSpinner(false);
            // this.setSpinner(false);
            //alert('Please check internet connection.');
        });
    }
    else{

        Apis.compdetailAttributeAndDistrictwiseTL(this.state.Officerid, this.state.AttributeId, this.state.filter, this.state.fPage, this.state.nextpage,this.state.Districtid).then((res) => {
            //   this.setSpinner(true);
            console.log('res comp list by compdetailAttributewiseTL');
            console.log(res);


            if (res.status == 'success') {
                this.setState({ compCount: res.messages.total_tickets, CompList: res.messages.user_tickets, masterCompList: res.messages.user_tickets }, () => {
                    this.setState({fPage:this.state.firstpage, firstpage: this.state.firstpage - 1, nextpage: this.state.nextpage  })
                })
                this.setSpinner(false);
            }


        }, (error) => {

            console.log("error");
            console.log(error);
            this.setSpinner(false);
            // this.setSpinner(false);
            //alert('Please check internet connection.');
        });
    }
}

    render() {
        const complainList = this.state.CompList;
        console.log("complaint:", complainList)
        return (
            <View style={{ flex: 1 }}>

<NavigationBar style={{ container: { borderBottomColor: '#117bc7', zIndex: 0, backgroundColor: '#117bc7', color: '#fff', height: 50 } }}

leftComponent=
{<Touchable onPress={() => this.props.navigation.goBack()} >
    < Icon style={{ color: '#fff', fontSize: 30,marginLeft:10 }} name="left-arrow" />
</Touchable>
}
centerComponent={
    <Title style={styles.navigationBarTitleColor} >{this.state.StatusText}  </Title>
}

rightComponent={(
    <Touchable onPress={() => this.props.navigation.navigate('Dashboard')} >
        < MaterialCommunityIcons style={{marginRight:10, color: '#fff', fontSize: 30 }} name="home" />
    </Touchable>
)}
/>

                <ScrollView style={{ marginTop: '15%' }}
                    refreshControl={
                        <RefreshControl
                            onRefresh={() => this.refreshControl()}
                            refreshing={this.state.refreshing}
                            enabled={true}

                        />
                    }
                >

                    <View  >

                        {complainList.length > 0 ?
                            <FlatList

                                data={complainList}
                                renderItem={this.renderRow}
                                extraData={this.state.masterCompList}
                                keyExtractor={this._keyExtractor}
                                maxToRenderPerBatch={5}
                                getItemLayout={getItemLayout}
                                onEndReached={this.loadPosts.bind(this)}
                                // onEndReachedThreshold={1}
                                ListFooterComponent={this.state.loading ? <Spinner /> : null}
                                progressViewOffset={15}

                            />
                            :
                            <View style={{ flex: 1, alignSelf: 'center', justifyContent: 'center', paddingTop: '30%' }}   >
                                <Text>शिकायत मौजूद नहीं है !</Text>
                            </View>
                        }

                    </View>
                </ScrollView>
{this.state.loading ?
    <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)', alignItems: 'center', justifyContent: 'center', position: 'absolute', width: '100%', height: '100%' }}>
        <Spinner color="white" />
    </View> : null}
    <View styleName="horizontal" style={{ backgroundColor: '#117bc7',height:40 }}>
    <Button styleName="confirmation" style={{backgroundColor: 'transparent',marginLeft:5,borderWidth:0,borderColor:'transparent'}} onPress={() => this.loadPreviousCompList()}>
        <Icon style={{color:'#fff'}} name="left-arrow" />
        <Text style={{color:'#fff'}}>Previous</Text>
    </Button>

    <Button styleName="confirmation" style={{backgroundColor: 'transparent',marginRight:5,borderWidth:0,borderColor:'transparent'}} onPress={() => this.loadNextCompList()}>
        <Text style={{color:'#fff'}}>Next</Text>
        <Icon style={{color:'#fff'}} name="right-arrow" />
       
    </Button>
</View>


            </View>
        );
    }
}



const styles = {

    Card: {
        width: '95%',
        margin: 5,
        paddingLeft: 10,
        paddingRight: 10
    },


    navigationBarTitleColor: {
        fontSize: 20,
        color: '#fff'
    },
    View: {
        width: '95%',

    },
    Subtitle2: {
        fontSize: 18,
        // fontFamily: 'CeraPro-Bold',
        //  color: '#B3B3B3',
        fontWeight: '500',
        //    paddingLeft:3,

        marginLeft: 5


    },

    Subtitle: {
        paddingLeft: 10,
        fontSize: 15,
        // color: '#F7941F',
        paddingTop: 5
    },





}