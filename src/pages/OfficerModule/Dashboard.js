import React, { Component, Animated } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Dimensions,
    AsyncStorage,
    StatusBar,
    Alert
} from 'react-native';
import { Toast, Footer, FooterTab } from 'native-base';
import { Screen, ImageBackground, Spinner, Lightbox, NavigationBar, Text, View, Tile, Title, TextInput, Row, Col, Card, Button, Icon, Divider, Caption, Subtitle, Image, ListView, Touchable, TouchableOpacity } from '@shoutem/ui';
import Apis from '../../apis/Api';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

export default class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userDetail: [],
            userCountDetail: [],
            WIP: 'WIP',
            close: 'CLOSED',
            CWA: 93,
            PC: 'PC',
            Beyond_TL: 'Beyond_TL',
            TL_3Days: 'TL_3Days',
            TL_Today: 'TL_Today',
            ForceClose: 'ForceClose',
            Open: 'Open',
            NC: 'NC',
            Logincount: 'Logincount',
            OWA: 'OWA',
            WIPCount: 0,
            closeCount: 0,
            CWACount: 0,
            PCCount: 0,
            Beyond_TLCount: 0,
            TL_3DaysCount: 0,
            TL_TodayCount: 0,
            OpenCount: 0,
            NCCount: 0,
            ForceCloseCount: 0,
            LoginCount: 0,
            OWACount: 0,
            loading: false,
            mobile_no: '',
            status: '',
            Count: 0,
            spinner: false,
        };
    }
    setSpinner = (st) => {
        this.setState({ loading: st });
    }
    componentDidMount() {
        this.setSpinner(true);
        AsyncStorage.getItem("userDetail").then((value) => {

            console.log('this.state.userCountDetail');
            console.log(JSON.parse(value).name, JSON.parse(value).IsAdmin_Officer);
            this.setState({
                userDetail: JSON.parse(value)
            });

            if (JSON.parse(value).IsAdmin_Officer == "True") {
                var sourceId = 0;
                Apis.getcountForAdminOfficer(sourceId, JSON.parse(value).id).then((res) => {

                    console.log('getcountForAdminOfficer res');
                    console.log(res);
                    if (res.status != "error") {
                        this.setState({
                            userCountDetail: res.messages
                        });

                        var Open = res.messages.filter(function (value) {
                            return value.CMPTYPE == 'Open';
                        });

                        var WIP = res.messages.filter(function (value) {
                           
                            return value.CMPTYPE == "WIP";
                        });

                        var PC = res.messages.filter(function (value) {
                            return value.CMPTYPE == 'PC';
                        });
                        var Close = res.messages.filter(function (value) {
                            return value.CMPTYPE == 'Closed';
                        });
                        this.setSpinner(false);
                        var OWA = res.messages.filter(function (value) {
                            return value.CMPTYPE == 'OWA';
                        });

                        var NC = res.messages.filter(function (value) {
                            return value.CMPTYPE == 'NC';
                        });

                        var ForceClose = res.messages.filter(function (value) {
                            return value.CMPTYPE == 'ForceClose';
                        });

                        var Logincount = res.messages.filter(function (value) {
                            return value.CMPTYPE == 'Logincount';
                        });
                        this.setSpinner(false);
                        this.setState({
                            WIPCount: WIP[0].COUNT, PCCount: PC[0].COUNT, closeCount: Close[0].COUNT,
                            OWACount: OWA[0].COUNT, OpenCount: Open[0].COUNT, NCCount: NC[0].COUNT,
                            ForceCloseCount: ForceClose[0].COUNT, LoginCount: Logincount[0].COUNT
                        })
                    }
                    else {
                        this.setSpinner(false);
                    }

                }, (error) => {

                    this.setSpinner(false);

                });
            }
            else {
                Apis.dashboardCount(JSON.parse(value).id).then((res) => {
                   console.log('res');
                    console.log(res);
                    if (res.status == "success") {
                        this.setState({
                            userCountDetail: res.messages
                        });
                        this.setSpinner(false);
                        var WIP = res.messages.filter(function (value) {
                            return value.CMPTYPE == 'WIP';
                        });

                        var PC = res.messages.filter(function (value) {
                            return value.CMPTYPE == 'PC';
                        });
                        var Close = res.messages.filter(function (value) {
                            return value.CMPTYPE == 'CLOSE';
                        });
                        this.setSpinner(false);
                        var CWA = res.messages.filter(function (value) {
                            return value.CMPTYPE == 'CWA';
                        });

                        var TL_3Days = res.messages.filter(function (value) {
                            return value.CMPTYPE == 'TL_3Days';
                        });

                        var TL_Today = res.messages.filter(function (value) {
                            return value.CMPTYPE == 'TL_Today';
                        });

                        var Beyond_TL = res.messages.filter(function (value) {
                            return value.CMPTYPE == 'Beyond_TL';
                        });

                        this.setState({
                            WIPCount: WIP[0].COUNT, PCCount: PC[0].COUNT, closeCount: Close[0].COUNT,
                            CWACount: CWA[0].COUNT, TL_3DaysCount: TL_3Days[0].COUNT, TL_TodayCount: TL_Today[0].COUNT,
                            Beyond_TLCount: Beyond_TL[0].COUNT
                        })

                    }
                    else {
                        this.setSpinner(false);
                    }

                }, (error) => {
                    this.setSpinner(false);

                });
            }



        });
    }

    showToast = (message) => {
        Toast.show({
            text: message,
            position: 'bottom',
        })
    }

    ConfirmationToLogout = () => {

        Alert.alert(
            'Alert',
            'क्या आप लॉगआउट करना चाहते हैं ?',
            [
                { text: 'Cancel', onPress: () => this.setSpinner(false) },
                { text: 'OK', onPress: () => this.props.navigation.navigate('Logout') },

            ],
            { cancelable: false }
        )

    }

    render() {
        return (

            <Screen style={styles.Screen}>
                <StatusBar barStyle='light-content'
                    backgroundColor='#117bc7' />
                <NavigationBar style={{ container: { zIndex: 1, height: 40, backgroundColor: '#117bc7', color: '#fff', borderBottomWidth: 0, borderColor: 'none' } }}

                    centerComponent=
                    {(
                        <Title style={{ color: '#fff', fontSize: 16, marginTop: 5, width: 200, marginLeft: 20, fontWeight: 'bold' }} >उत्तराखंड सीएम हेल्पलाइन</Title>
                    )}
                    rightComponent={
                        <View styleName="horizontal" >
                            <Button styleName="clear" onPress={() => this.ConfirmationToLogout()}>
                                <MaterialCommunityIcons name="logout" style={{ fontSize: 25, color: '#fff', marginRight: 10 }}/>
                            </Button>
                        </View>
                    }

                />
                <View style={{ backgroundColor: '#117bc7', height: 75 }}></View>


                <Card style={styles.DemoCard}>
                    <View styleName="horizontal space-between">
                        <View styleName="vertical">
                            <Text style={{ color: '#268DC8' }}>{this.state.userDetail.name}</Text>
                            <Text >{this.state.userDetail.officerno}</Text>
                        </View>

                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('Profile') }}>
                            < Icon style={{ bottom: 5, color: '#117bc7' }} name="about" />
                        </TouchableOpacity>
                    </View>
                </Card>


                <View style={{ bottom: 15 }}>
                    <View styleName="horizontal" style={{ alignSelf: 'center' }}>

                        {this.state.WIPCount != 0 ?
                            <Touchable onPress={() => this.props.navigation.navigate('AttributeWiseList', { OfficerId: this.state.userDetail.id, Open: this.state.WIP, StatusText: 'लंबित शिकायतें', filter: 0 })}>
                                <Card style={styles.Card1}>
                                    <Text style={styles.RENTED} >लंबित शिकायतें</Text>
                                    <View styleName="horizontal" style={styles.ViewCard1} >
                                        <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                        <Text style={styles.RENTED2200}>
                                            {this.state.WIPCount}
                                        </Text>
                                    </View>
                                </Card>
                            </Touchable>
                            :
                            <Card style={styles.Card1}>
                                <Text style={styles.RENTED} >लंबित शिकायतें</Text>
                                <View styleName="horizontal" style={styles.ViewCard1} >
                                    <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                    <Text style={styles.RENTED2200}>
                                        {this.state.WIPCount}
                                    </Text>
                                </View>
                            </Card>}
                        {this.state.PCCount != 0 ?
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('AttributeWiseList', { OfficerId: this.state.userDetail.id, Open: this.state.PC, StatusText: 'आंशिक रूप से बंद', filter: 0 })} >
                                <Card style={styles.Card2}>
                                    <Text style={styles.RENTED} >आंशिक रूप से बंद</Text>
                                    <View styleName="horizontal" style={styles.ViewCard2} >
                                        <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                        <Text style={styles.RENTED2200}>
                                            {this.state.PCCount}
                                        </Text>
                                    </View>
                                </Card>
                            </TouchableOpacity>
                            :
                            <Card style={styles.Card2}>
                                <Text style={styles.RENTED} >आंशिक रूप से बंद</Text>
                                <View styleName="horizontal" style={styles.ViewCard2} >
                                    <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                    <Text style={styles.RENTED2200}>
                                        {this.state.PCCount}
                                    </Text>
                                </View>
                            </Card>}

                    </View>
                    <View styleName="horizontal" style={{ marginTop: 10, alignSelf: 'center' }}>

                        {this.state.closeCount != 0 ?
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('AttributeWiseList', { OfficerId: this.state.userDetail.id, Open: this.state.close, StatusText: 'बंद शिकायतें', filter: 0 })}>
                                <Card style={styles.Card3}>
                                    <Text style={styles.RENTED} >बंद शिकायतें</Text>
                                    <View styleName="horizontal" style={styles.ViewCard3} >
                                        <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                        <Text style={styles.RENTED2200}>
                                            {this.state.closeCount}
                                        </Text>
                                    </View>
                                </Card>
                            </TouchableOpacity>
                            :
                            <Card style={styles.Card3}>
                                <Text style={styles.RENTED} >बंद शिकायतें</Text>
                                <View styleName="horizontal" style={styles.ViewCard3} >
                                    <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                    <Text style={styles.RENTED2200}>
                                        {this.state.closeCount}
                                    </Text>
                                </View>
                            </Card>}
                        
                        {this.state.userDetail.IsAdmin_Officer != "True" ?
                            (this.state.CWACount != 0 ?
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('AttributeWiseList', { OfficerId: this.state.userDetail.id, Open: this.state.CWA, StatusText: 'मान्य अमान्य लंबित शिकायतें', filter: 0 })}>
                                    <Card style={styles.Card4}>
                                        <Text style={styles.RENTED} >मान्य अमान्य लंबित शिकायतें</Text>
                                        <View styleName="horizontal" style={styles.ViewCard4} >
                                            <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                            <Text style={styles.RENTED2200}>
                                                {this.state.CWACount}
                                            </Text>
                                        </View>
                                    </Card>
                                </TouchableOpacity>
                                :
                                <Card style={styles.Card4}>
                                    <Text style={styles.RENTED} >मान्य अमान्य लंबित शिकायतें</Text>
                                    <View styleName="horizontal" style={styles.ViewCard4} >
                                        <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                        <Text style={styles.RENTED2200}>
                                            {this.state.CWACount}
                                        </Text>
                                    </View>
                                </Card>)

                            :
                            (this.state.OWACount != 0 ?
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('AttributeWiseList', { OfficerId: this.state.userDetail.id, Open: this.state.OWA, StatusText: 'कार्य क्षेत्र से बाहर', filter: 0 })}>
                                    <Card style={styles.Card4}>
                                        <Text style={styles.RENTED} >कार्य क्षेत्र से बाहर</Text>
                                        <View styleName="horizontal" style={styles.ViewCard4} >
                                            <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                            <Text style={styles.RENTED2200}>
                                                {this.state.OWACount}
                                            </Text>
                                        </View>
                                    </Card>
                                </TouchableOpacity>
                                :
                                <Card style={styles.Card4}>
                                    <Text style={styles.RENTED} >कार्य क्षेत्र से बाहर</Text>
                                    <View styleName="horizontal" style={styles.ViewCard4} >
                                        <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                        <Text style={styles.RENTED2200}>
                                            {this.state.OWACount}
                                        </Text>
                                    </View>
                                </Card>)}
                        </View>

                    {this.state.userDetail.IsAdmin_Officer != "True" ?
                        <View styleName="horizontal" style={{ marginTop: 10, alignSelf: 'center' }}>

                            {this.state.TL_TodayCount != 0 ?
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('AttributeWiseList', { OfficerId: this.state.userDetail.id, Open: this.state.TL_Today, StatusText: 'अंतिम कार्यदिवस', filter: 1 })}>
                                    <Card style={styles.Card5}>
                                        <Text style={styles.RENTED} >अंतिम कार्यदिवस</Text>
                                        <View styleName="horizontal" style={styles.ViewCard5} >
                                            <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                            <Text style={styles.RENTED2200}>
                                                {this.state.TL_TodayCount}
                                            </Text>
                                        </View>
                                    </Card>
                                </TouchableOpacity>
                                :
                                <Card style={styles.Card5}>
                                    <Text style={styles.RENTED} >अंतिम कार्यदिवस</Text>
                                    <View styleName="horizontal" style={styles.ViewCard5} >
                                        <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                        <Text style={styles.RENTED2200}>
                                            {this.state.TL_TodayCount}
                                        </Text>
                                    </View>
                                </Card>}


                            {this.state.TL_3DaysCount != 0 ?
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('AttributeWiseList', { OfficerId: this.state.userDetail.id, Open: this.state.TL_3Days, StatusText: 'अंतिम 3 कार्यदिवस', filter: 2 })}>
                                    <Card style={styles.Card6}>
                                        <Text style={styles.RENTED} >अंतिम 3 कार्यदिवस</Text>
                                        <View styleName="horizontal" style={styles.ViewCard6} >
                                            <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                            <Text style={styles.RENTED2200}>
                                                {this.state.TL_3DaysCount}
                                            </Text>
                                        </View>
                                    </Card>
                                </TouchableOpacity>
                                : <Card style={styles.Card6}>
                                    <Text style={styles.RENTED} >अंतिम 3 कार्यदिवस</Text>
                                    <View styleName="horizontal" style={styles.ViewCard6} >
                                        <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                        <Text style={styles.RENTED2200}>
                                            {this.state.TL_3DaysCount}
                                        </Text>
                                    </View>
                                </Card>}

                            </View>
                        : null}

                    {this.state.userDetail.IsAdmin_Officer != "True" ?
                        <View styleName="horizontal" style={{ marginTop: 10, alignSelf: 'center' }}>
                            {this.state.Beyond_TLCount != 0 ?
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('AttributeWiseList', { OfficerId: this.state.userDetail.id, Open: this.state.Beyond_TL, StatusText: 'समय सीमा बाहर', filter: 3 })}>
                                    <Card style={styles.Card7}>
                                        <Text style={styles.RENTED} >समय सीमा बाहर</Text>
                                        <View styleName="horizontal" style={styles.ViewCard7} >
                                            <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                            <Text style={styles.RENTED2200}>
                                                {this.state.Beyond_TLCount}
                                            </Text>
                                        </View>
                                    </Card>
                                </TouchableOpacity>
                                :
                                <Card style={styles.Card7}>
                                    <Text style={styles.RENTED} >समय सीमा बाहर</Text>
                                    <View styleName="horizontal" style={styles.ViewCard7} >
                                        <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                        <Text style={styles.RENTED2200}>
                                            {this.state.Beyond_TLCount}
                                        </Text>
                                    </View>
                                </Card>}


                            {this.state.TL_3DaysCount != 0 ?

                                <Card style={styles.Card8}></Card>

                                : <Card style={styles.Card8}> </Card>}

                        </View>
                        : null}

                    {this.state.userDetail.IsAdmin_Officer == "True" ?
                        <View styleName="horizontal" style={{ marginTop: 10, alignSelf: 'center' }}>

                            {this.state.OpenCount != 0 ?
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('AttributeWiseList', { OfficerId: this.state.userDetail.id, Open: this.state.Open, StatusText: 'ओपन', filter: 0 })}>
                                    <Card style={styles.Card5}>
                                        <Text style={styles.RENTED} >ओपन</Text>
                                        <View styleName="horizontal" style={styles.ViewCard5} >
                                            <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                            <Text style={styles.RENTED2200}>
                                                {this.state.OpenCount}
                                            </Text>
                                        </View>
                                    </Card>
                                </TouchableOpacity>
                                :
                                <Card style={styles.Card5}>
                                    <Text style={styles.RENTED} >ओपन</Text>
                                    <View styleName="horizontal" style={styles.ViewCard5} >
                                        <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                        <Text style={styles.RENTED2200}>
                                            {this.state.OpenCount}
                                        </Text>
                                    </View>
                                </Card>}

                            {this.state.NCCount != 0 ?
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('AttributeWiseList', { OfficerId: this.state.userDetail.id, Open: this.state.NC, StatusText: 'नॉट कनेक्टेड क्लोज़', filter: 0 })}>

                                    <Card style={styles.Card6}>
                                        <Text style={styles.RENTED} >नॉट कनेक्टेड क्लोज़</Text>
                                        <View styleName="horizontal" style={styles.ViewCard6} >

                                            <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />

                                            <Text style={styles.RENTED2200}>
                                                {this.state.NCCount}
                                            </Text>
                                        </View>
                                    </Card>
                                </TouchableOpacity>
                                :
                                <Card style={styles.Card6}>
                                    <Text style={styles.RENTED} >नॉट कनेक्टेड क्लोज़</Text>
                                    <View styleName="horizontal" style={styles.ViewCard6} >

                                        <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />

                                        <Text style={styles.RENTED2200}>
                                            {this.state.NCCount}
                                        </Text>
                                    </View>
                                </Card>}
                        </View>
                        : null}

                    {this.state.userDetail.IsAdmin_Officer == "True" ?
                        <View styleName="horizontal" style={{ marginTop: 10, alignSelf: 'center' }}>

                            {this.state.ForceCloseCount != 0 ?
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('AttributeWiseList', { OfficerId: this.state.userDetail.id, Open: this.state.ForceClose, StatusText: 'फोर्स क्लोज़', filter: 0 })}>
                                    <Card style={styles.Card7}>
                                        <Text style={styles.RENTED} >फोर्स क्लोज़</Text>
                                        <View styleName="horizontal" style={styles.ViewCard7} >
                                            <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                            <Text style={styles.RENTED2200}>
                                                {this.state.ForceCloseCount}
                                            </Text>
                                        </View>
                                    </Card>
                                </TouchableOpacity>
                                :
                                <Card style={styles.Card7}>
                                    <Text style={styles.RENTED} >फोर्स क्लोज़</Text>
                                    <View styleName="horizontal" style={styles.ViewCard7} >
                                        <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />

                                        <Text style={styles.RENTED2200}>
                                            {this.state.ForceCloseCount}
                                        </Text>
                                    </View>
                                </Card>}

                            {this.state.LoginCount != 0 ?

                                <Card style={styles.Card2}>
                                    <Text style={styles.RENTED} >अधिकारी लॉगिन </Text>
                                    <View styleName="horizontal" style={styles.ViewCard2} >
                                        <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                        <Text style={styles.RENTED2200}>
                                            {this.state.LoginCount}
                                        </Text>
                                    </View>
                                </Card>

                                :
                                <Card style={styles.Card2}>
                                    <Text style={styles.RENTED} >अधिकारी लॉगिन </Text>
                                    <View styleName="horizontal" style={styles.ViewCard2} >
                                        <Icon name="news" style={{ color: '#fff', fontSize: 35, marginLeft: 30 }} />
                                        <Text style={styles.RENTED2200}>
                                            {this.state.LoginCount}
                                        </Text>
                                    </View>
                                </Card>}
                        </View>
                    : null}
                </View>
                <TouchableOpacity style={{ backgroundColor: '#117bc7', position: 'absolute', bottom: 0, height: 45, width: '100%' }} onPress={() => this.props.navigation.navigate('SearchComp', { OfficerId: this.state.userDetail.id })}>
                    <View style={{ backgroundColor: '#117bc7', position: 'absolute', bottom: 0, height: 45, width: '100%' }} >
                        <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 20, paddingTop: 5 }}>शिकायत खोजें</Text>
                    </View>
                </TouchableOpacity>
                {this.state.loading ?
                    <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)', alignItems: 'center', justifyContent: 'center', position: 'absolute', width: '100%', height: '100%' }}>
                        <Spinner style={{ color: 'blue' }} />
                    </View> : null}

            </Screen>

        );
    }
}

const styles = StyleSheet.create({
    
    RENTED: {
        fontSize: 13,
        marginTop: 3,
        color: '#fff',
        alignSelf: 'center'
    },

    RENTED2200: {
        color: '#fff',
        marginLeft: 5,
        fontSize: 25
    },
  
    Screen: {
        marginBottom: 0,
        backgroundColor: '#eaeaea',
        height: '100%'
    },
    navigationBar: {
        backgroundColor: '#FBAF41',
        flexDirection: 'row',
    },

    navigationBarIconColor: {
        fontSize: 25,
        color: '#fff',
        marginRight: 20
    },
    navigationBarIcon: {
        fontSize: 25,
        color: '#fff',
        marginLeft: 10
    },
  
    DemoCard: {
        paddingLeft: 30, 
        paddingRight: 30,
        width: '80%',
        height: 60,
        bottom: 30,
        alignSelf: 'center',
        borderRadius: 5
    },
   
    Card1: {
        marginLeft: 10,
        width: 160,
        height: 80,
        backgroundColor: '#EB4B3B',
    },
    
    ViewCard1: {
        marginLeft: 10,
        height: 80,
        backgroundColor: '#EB4B3B',
    },

    Card2: {
        marginLeft: 5,
        marginRight: 10,
        width: 160,
        height: 80,
        backgroundColor: '#F8BD47',
    },
    
    ViewCard2: {
        marginLeft: 5,
        marginRight: 10,
        height: 80,
        backgroundColor: '#F8BD47',
    },

    Card3: {
        marginLeft: 10,
        marginRight: 5,
        width: 160,
        height: 80,
        backgroundColor: '#39B196',
    },
    
    ViewCard3: {
        marginLeft: 10,
        marginRight: 5,
        height: 80,
        backgroundColor: '#39B196',
    },

    Card4: {
        marginRight: 10,
        width: 160,
        height: 80,
        backgroundColor: '#6BA1EB',
    },
    
    ViewCard4: {
        marginRight: 10,
        height: 80,
        backgroundColor: '#6BA1EB',
    },

    Card5: {
        marginLeft: 10,
        marginRight: 5,
        width: 160,
        height: 80,
        backgroundColor: '#8871F5',
    },
    
    ViewCard5: {
        marginLeft: 10,
        marginRight: 5,
        height: 80,
        backgroundColor: '#8871F5',
    },

    Card6: {
        marginRight: 10,
        width: 160,
        height: 80,
        backgroundColor: '#2C9DEE',
    },
    
    ViewCard6: {
        marginRight: 10,
        height: 80,
        backgroundColor: '#2C9DEE',
    },
    
    Card7: {
        marginLeft: 10,
        marginRight: 5,
        width: 158,
        height: 80,
        backgroundColor: '#3AB54A',

    },
    
    Card8: {
        marginRight: 10,
        width: 160,
        height: 80,
        backgroundColor: 'transparent'
    },
    
    ViewCard7: {
        marginLeft: 10,
        marginRight: 5,
        height: 80,
        backgroundColor: '#3AB54A',
    },

})
const screenDim = Dimensions.get('window');