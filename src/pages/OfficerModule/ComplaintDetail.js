/**
 * Deepak S Rathode
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { ImageBackground, Touchable, Spinner, Lightbox, NavigationBar,  View, Screen, Tile,Text, Title, TextInput, Row, Col, Card, Button, Icon, Divider, Caption, Subtitle, Image, ListView, TouchableOpacity, } from '@shoutem/ui';
import { ScrollView, Modal, TouchableHighlight, ActivityIndicator, StatusBar, AsyncStorage, Alert, } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Apis from '../../apis/Api';
import Communications from 'react-native-communications';
import { Toast } from 'native-base';
export default class ComplaintDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            CompDetail: [],
            CompDetailComment: [],
            CompDetailCommentCount: 0,
            userDetail: [],
            Remark: '',
            loading: false,
            compRemark: [],
            Officerid: '',
            CompId: 0,
            CompList: [],
            Default_Rating: 0,
            Max_Rating : 5

        };
        this.Star = 'https://reactnativecode.com/wp-content/uploads/2018/01/full_star.png';

        this.Star_With_Border = 'https://reactnativecode.com/wp-content/uploads/2018/01/border_star.png';


    }
    setSpinner = (st) => {
        this.setState({ loading: st });
    }

    showToast = (message) => {
        Toast.show({
            text: message,
            position: 'bottom',
        })
    }

    componentDidMount() {

        this.setSpinner(true);
        var compdetail = this.props.navigation.getParam('CompDetail');
        var CompId = this.props.navigation.getParam('CompId');
        var Officerid = this.props.navigation.getParam('Officerid');


        console.log('CompDetail');
        console.log(compdetail, CompId);
        AsyncStorage.getItem("userDetail").then((value) => {

            console.log('this.state.userCountDetail');
            console.log(JSON.parse(value));
            this.setState({
                userDetail: JSON.parse(value), Officerid: Officerid, CompId: CompId, CompList: compdetail
            });
            this.getCompDetail(CompId);
            console.log('this.state.Officerid==this.state.userDetail.id', compdetail.officerid, JSON.parse(value).id)

        });

    }

    getCompDetail = (CompId) => {
        Apis.details(CompId).then((res) => {
            this.setSpinner(false);
            console.log('res comp list by attri and dist');
            console.log(res);


            if (res.status == 'success') {
                this.setState({
                    CompDetail: res.messages.ticket_detail,
                    CompDetailComment: res.messages.comments.comments, CompDetailCommentCount: res.messages.comments.total_comment
                })

            }


        }, (error) => {

            console.log("error");
            console.log(error);
            this.setSpinner(false);

        });

        Apis.GetcompremarkFor93(CompId).then((res) => {
            this.setSpinner(false);
            console.log('res GetcompremarkFor93 list ');
            console.log(res);


            if (res.status == 'success') {
                this.setState({
                    compRemark: res.messages
                })

            }


        }, (error) => {

            console.log("error");
            console.log(error);
            this.setSpinner(false);

        });
    }

    UpdateComplaint = (StatusRemark) => {
        console.log('date to update', this.state.CompDetail.tid, this.state.userDetail.id, this.state.Remark, StatusRemark)
        if (this.state.Remark != '' && this.state.Remark != null) {
            Apis.UpdateComplaint(this.state.CompDetail.tid, this.state.userDetail.id, this.state.Remark, StatusRemark).then((res) => {
                if (res.status == 'success') {
                    Alert.alert(
                        'Alert',
                        'शिकायत क्रमांक : ' + this.state.CompDetail.tid + ' को अपडेट कर दिया गया है।',
                        [
                            // { text: 'Cancel', onPress: () => this.setSpinner(false) },
                            { text: 'OK', onPress: () => { this.setState({ Remark: '' }) } },
                            //{text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                    )

                    this.getCompDetail(this.state.CompDetail.tid)
                }
                else {
                    this.setSpinner(false);
                    this.showToast('"माफ़ कीजिये: शिकायत अपडेट करने में असमर्थ!');
                }

            }, (error) => {
                this.setSpinner(false);
                console.log(error);

            })
        }
        else {
            alert('कृपया निराकरण दर्ज करें !')
        }

    }


    ApproveDisapproveComplaint = (StatusRemark) => {

        var auth = '', compclose = '';
        console.log('date to update', this.state.CompDetail.tid, auth, this.state.userDetail.id, this.state.Remark, StatusRemark, compclose)
        if (this.state.Remark != '' && this.state.Remark != null) {
            Apis.CommentsforApprovedDisapprovedComplaint(this.state.CompDetail.tid, auth, this.state.userDetail.id, this.state.Remark, StatusRemark, compclose).then((res) => {
                if (res.status == 'success') {
                    Alert.alert(
                        'Alert',
                        'शिकायत क्रमांक : ' + this.state.CompDetail.tid + ' को अपडेट कर दिया गया है।',
                        [
                            // { text: 'Cancel', onPress: () => this.setSpinner(false) },
                            { text: 'OK', onPress: () => { this.setState({ Remark: '' }) } },
                            //{text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                    )
                    this.getCompDetail(this.state.CompDetail.tid)
                }
                else {
                    this.setSpinner(false);
                    this.showToast('"माफ़ कीजिये: शिकायत अपडेट करने में असमर्थ!');
                }

            }, (error) => {
                this.setSpinner(false);
                console.log(error);

            })
        }
        else {
            alert('कृपया निराकरण दर्ज करें !')
        }

    }

    OutOfDepartment = () => {

        this.props.navigation.navigate('OutofDept', { CompDetail: this.state.CompDetail })
    }

    
    UpdateRating( key )
    {
        this.setState({ Default_Rating: key });
    }


    render() {

        let React_Native_Rating_Bar = [];
 
        for( var i = 1; i <= this.state.Max_Rating; i++ )
        {
            console.log('this.state.CompDetail.rating',this.state.CompDetail.rating)
          React_Native_Rating_Bar.push(
 
            
                    <Image 
                      style = { styles.StarImage } 
                      source = { ( i <= this.state.CompDetail.rating ) ? { uri: this.Star } : { uri: this.Star_With_Border } } />
                
          
          );
        }


        return (



            <Screen style={{ flex: 1, backgroundColor: '#fff', }}>



                <NavigationBar style={{ container: { Index: 1, backgroundColor: '#117bc7', color: '#fff', borderBottomWidth: 0, borderColor: 'none' } }}
                    statusBarColor="#117bc7"
                    leftComponent={
                        <Touchable onPress={() => this.props.navigation.goBack()} >
                            <Icon style={{ color: '#fff', fontSize: 35, marginLeft: 10 }} name="left-arrow" />
                        </Touchable>}

                    centerComponent={<Text style={{ fontSize: 18, color: '#fff', width: 230, alignSelf: 'center', fontWeight: '500' }}>


                        शिकायत
पर की गयी कार्रवाई

</Text>}
                    rightComponent={<Touchable onPress={() => this.props.navigation.navigate('Dashboard')} >
                        < MaterialCommunityIcons style={{ color: '#fff', fontSize: 30, marginRight: 10 }} name="home" />
                    </Touchable>}
                />

 
                <ScrollView style={{  marginTop: 70,}}>
                 
                        {this.state.CompDetail.Officerid == this.state.userDetail.id ?
                         <View >
                         <View>
                           
                                {this.state.CompDetail.StatusRemark != "PC" && this.state.CompDetail.StatusRemark != "CLOSE" && this.state.CompDetail.StatusRemark != "ForceClose" ?
                                    <TextInput
                                    
                                        multiline={true}
                                        placeholderTextColor="#fff"
                                        placeholder="कार्रवाई"
                                        style={{
                                            alignSelf:'center',
                                            height: 100, backgroundColor: '#ccc',
                                            width: 335, 
                                        }}
                                        value={this.state.Remark}
                                        onChangeText={(text) => this.setState({ Remark: text })} >


                                    </TextInput>
                                    : null}

                                {this.state.CompDetail.StatusRemark == "WIP" && this.state.CompDetail.StatusRemarkId != 93 ?
                                    (<View>
                                        <Row style={{ height: 55,alignSelf:'center' }}  >
                                            <Button style={{ backgroundColor: '#3AB54A', width: 163,borderColor: '#3AB54A' }} styleName="secondary" onPress={() => this.UpdateComplaint(1)}>
                                                <Text>कार्य प्रगति पर है</Text>
                                            </Button>

                                            <Button style={{ backgroundColor: '#F7941D', marginLeft: 8, width: 163,borderColor:'#F7941D' }} styleName="secondary" onPress={() => this.UpdateComplaint(2)}>
                                                <Text>आंशिक रूप से बंद</Text>
                                            </Button>


                                        </Row>


                                        <Row style={{ height: 55,alignSelf:'center' }}>
                                            <Button style={{ backgroundColor: '#2B3991', width: 163,borderColor:'#2B3991' }} styleName="secondary" onPress={() => this.OutOfDepartment()} >
                                                <Text>कार्य क्षेत्र से बाहर</Text>
                                            </Button>
                                            {this.state.userDetail.officerLevel != "L1" ?
                                                <Button style={{ backgroundColor: '#BF1E2E', marginLeft: 8, width: 163,borderColor:'#BF1E2E' }} styleName="secondary" onPress={() => this.UpdateComplaint(3)}>
                                                    <Text>स्पेशल क्लोजर</Text>
                                                </Button>
                                                : null}
                                        </Row>
                                    </View>)
                                    : null}
                            </View>
                            </View>
                            : null}

                        {this.state.CompDetail.Officerid == this.state.userDetail.id ?
                            this.state.CompDetail.StatusRemarkId == 93 && this.state.CompDetail.StatusRemark == "WIP" ?
                                (<View>
                                    <Row style={{ height: 55 }}  >
                                        {this.state.userDetail.officerLevel != "L1" ?
                                            <Button style={{ backgroundColor: '#BF1E2E', marginLeft: 5, marginRight: 5, width: 163 }} styleName="secondary" onPress={() => this.UpdateComplaint(3)}>
                                                <Text>स्पेशल क्लोजर</Text>
                                            </Button>
                                            : null}
                                        <Button style={{ backgroundColor: '#3AB54A', width: 163 }} styleName="secondary" onPress={() => this.ApproveDisapproveComplaint(6)}>
                                            <Text>मान्य करें</Text>
                                        </Button>



                                    </Row>


                                    <Row style={{ height: 55 }}>
                                        <Button style={{ backgroundColor: '#BF1E2E', marginLeft: 5, width: 163 }} styleName="secondary" onPress={() => this.ApproveDisapproveComplaint(7)}>
                                            <Text>अमान्य करें </Text>
                                        </Button>

                                    </Row>
                                </View>)
                                : null
                            : null}

<View style={{marginBottom:'50%'}}>

                        <Row style={{ height: 45, backgroundColor: '#F0F0F0' }}>
                                
                            <Text style={{ fontWeight: 'bold', marginLeft: 5 }}>क्रमांक: <Text>{this.state.CompDetail.tid}   </Text>    </Text>
                        
                            <Text style={{ textAlign:'right',alignSelf:'stretch', fontWeight: 'bold' }}>दिनांक: <Text>{this.state.CompDetail.compdate}   </Text>    </Text>


                        </Row>
                        <Row style={{height: 30, backgroundColor: '#F0F0F0',marginTop:5,paddingTop:2,paddingBottom:2}}>         
                           
                            <Text style={{ textAlign:'right',alignSelf:'stretch', fontWeight: 'bold',right:25 }}>रेटिंग : {React_Native_Rating_Bar}</Text>

                        </Row>

                        <Row style={{ marginLeft: 5, height: 45 }}>
                            <View styleName="horizontal" >
                                <Text style={{ fontWeight: 'bold' }}>शिकायत की स्थिति:  </Text>
                                <Text style={{ color: '#3AB54A', paddingLeft: 40, fontSize: 18, fontWeight: 'bold' }}> o</Text>
                                <Text style={{ paddingLeft: 5 }}>{this.state.CompDetail.compstatus}  </Text>

                            </View>

                        </Row>


                        <Row style={{ marginLeft: 5, height: 45 }}>

                            <Text style={{ fontWeight: 'bold' }}>विभाग का नाम:  </Text>

                            <Text>{this.state.CompDetail.Department} </Text>


                        </Row>



                        <Row style={{ marginLeft: 5, height: 45 }}>

                            <Text style={{ fontWeight: 'bold' }}>शिकायत कर्ता का नाम:  </Text>

                            <Text>{this.state.CompDetail.callername} </Text>


                        </Row>


                        <Row style={{ marginLeft: 7, height: 45 }}>

                            <Text style={{ fontWeight: 'bold' }}>फोन न.:  </Text>

                            <Text style={{ paddingLeft: 25 }}>{this.state.CompDetail.phone} </Text>
                            <Touchable onPress={() => Communications.phonecall(this.state.CompDetail.phone, true)} >
                                <Icon name="call" style={{ alignSelf: 'center', height: 30, width: 30, borderRadius: 100, backgroundColor: 'green', color: '#fff' }} />
                            </Touchable>

                        </Row>
                        <Row style={{ marginLeft: 7 }}>

                            <Text style={{ fontWeight: 'bold' }}>पता:  </Text>

                            <Text style={{lineHeight:25}}>{this.state.CompDetail.calleraddress}</Text>


                        </Row>



                        <Row style={{ }}>

                            <Text style={{ fontWeight: 'bold' }}>जिला:  </Text>

                            <Text>{this.state.CompDetail.District}</Text>


                        </Row>


                        {/* <Row style={{ marginLeft: 7, height: 45 }}>

                            <Text style={{ fontWeight: 'bold' }}>रेटिंग :  </Text>
                          

<Text>{React_Native_Rating_Bar}</Text>
                        </Row> */}



                        <Row style={{ backgroundColor: '#F0F0F0', height: 45 }}>

                            <Text style={{ marginLeft: 10, fontWeight: 'bold', marginTop: 10 }}>शिकायत का प्रारूप:  </Text>
                        </Row>
                        <Text style={{ marginTop: 15, marginLeft: 20 }}>{this.state.CompDetail.Complainttype} </Text>







                        <Row style={{ backgroundColor: '#F0F0F0', height: 45, marginTop: 15 }}>
                            <Text style={{ marginLeft: 15, marginTop: 10, fontWeight: 'bold', backgroundColor: '#F0F0F0' }}>शिकायत का विवरण:  </Text>
                        </Row>

 
                        <Text style={{ marginTop: 20, lineHeight:25,marginLeft: 15}}>{this.state.CompDetail.complaint}</Text>





                        <Row style={{ backgroundColor: '#F0F0F0', height: 45, marginTop: 15 }}>



                            <Text style={{ marginLeft: 10, marginTop: 10, fontWeight: 'bold' }}>शिकायत पर की गई गतिविधि:  </Text>
                        </Row>

                        {this.state.CompDetailComment.map((item, index) => {
                            return (
                                // <Row key={index}>
                                    <View >
                             
                                    <Text style={{ textAlign: 'justify', alignSelf: 'stretch',marginTop:5,lineHeight:20,marginRight:10,marginLeft:10}}>{item.notes}
                                        </Text>
                                  
                                        <Text style={{ textAlign: 'right', alignSelf: 'stretch', fontSize: 12,marginRight:10,marginTop:10 }}>दिनांक:  {item.logdate}</Text>
                                      
                                        <Divider styleName="line" style={{ borderBottomWidth: 2, borderBottomColor: '#E6E6E6', width: '94%', marginLeft: '3%', marginRight: '3%' }} />
                                    </View>

                                // </Row>
                            )
                        })}




               </View>
                 
                </ScrollView>
               
                {this.state.loading ?
                    <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)', alignItems: 'center', justifyContent: 'center', position: 'absolute', width: '100%', height: '100%' }}>
                        <Spinner color="white" />
                    </View> : null}

            </Screen>
        );


    }

}
const styles = {



    borderview: {
        borderBottomColor: '#268DC8',

        borderBottomWidth: 70,
        paddingRight: '1%',
    },
    borderview2: {
        borderBottomColor: '#05ADEE',
        borderBottomWidth: 50,
        paddingRight: '1%',
    },
    borderview3: {
        borderBottomColor: '#fff',
        borderBottomWidth: 65,
        paddingLeft: '1%',
        marginLeft: '10%',
        width: '80%',
        borderRadius: 5
    },

    StarImage:
    {
        width: 15,
        height: 15
    },


    Card: {
        marginLeft: '6%',
        width: '85%',
        borderRadius: 10,
        marginTop: '5%',
        elevation: 1,
        marginRight: '10%',

    },
    Card1: {
        marginLeft: '5%',
        borderRadius: 10,
        elevation: 1,
        width: '43%',
        // height: '60%',
        height: 100,
        backgroundColor: '#DC4437',
        marginTop: 5

    },


    Card2: {
        marginLeft: '5%',
        borderRadius: 10,
        elevation: 1,
        width: '43%',
        // height: '60%',
        backgroundColor: '#86B83B',
        height: 100,
        marginTop: 5
    },


    Card3: {
        marginLeft: '5%',
        borderRadius: 10,
        elevation: 1,
        width: '43%',
        // height: '50%',
        backgroundColor: '#8871F5',
        height: 100
    },

    Card4: {
        marginLeft: '5%',
        borderRadius: 10,
        elevation: 1,
        width: '43%',
        // height: '50%',
        backgroundColor: '#2CC5EE',
        height: 100
    },

    Card5: {
        marginLeft: '5%',
        borderRadius: 10,
        elevation: 1,
        width: '43%',
        // height: '50%',
        backgroundColor: '#8871F5',
        height: 100
    },
    Card6: {
        marginLeft: '5%',
        borderRadius: 10,
        elevation: 1,
        width: '43%',
        // height: '50%',
        backgroundColor: '#2C9DEE',
        height: 100
    },



    Card0: {
        marginLeft: '5%',
        borderRadius: 10,
        marginTop: 15,
        elevation: 1,
        width: '90%',
        // height: '15%',
        backgroundColor: '#1D75BD',
        height: 100
    },

    Card7: {
        marginLeft: '5%',
        borderRadius: 10,
        marginTop: 10,
        elevation: 1,
        width: '90%',
        // height: '15%',
        backgroundColor: '#FF8500',
        height: 100
    },

    RENTED: {
        fontSize: 10,
        paddingLeft: '35%',
        color: '#fff',
        fontFamily: 'CeraPRO-Medium',
        marginTop: '5%'
    },
    RENTED2200: {
        paddingLeft: '40%',
        color: '#fff',
        fontFamily: 'CeraPRO-Medium',
        fontSize: 25
    },

    VACANT: {
        fontSize: 10,
        paddingLeft: '30%',
        color: '#fff',
        fontFamily: 'CeraPRO-Medium',
        marginTop: '5%'
    },
    VACANT1400: {
        paddingLeft: '40%',
        color: '#fff',
        fontFamily: 'CeraPRO-Medium',
        fontSize: 25
    },

    PUBLISHED: {
        fontSize: 10,
        paddingLeft: '40%',
        color: '#fff',
        fontFamily: 'CeraPRO-Medium',
        marginTop: '5%'
    },
    PUBLISHED3600: {
        paddingLeft: '40%',
        color: '#fff',
        fontFamily: 'CeraPRO-Medium',
        fontSize: 25,
        marginBottom: '15%'
    },

    UNPUBLISHED: {
        fontSize: 10,
        paddingLeft: '30%',
        color: '#fff',
        fontFamily: 'CeraPRO-Medium',
        marginTop: '5%'
    },
    UNPUBLISHED1400: {
        paddingLeft: '40%',
        color: '#fff',
        fontFamily: 'CeraPRO-Medium',
        fontSize: 25
    },






    LinearGradient: {
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: '10%',
        width: '60%'
    },
    Icon: {
        alignSelf: 'center',
        marginTop: '7%',
        fontSize: 55,
        color: '#fff'
    },


    Text: {
        alignSelf: 'center',
        paddingLeft: '5%',
        fontSize: 13,
        color: '#5D5D5D'
    },
    row: { flexDirection: 'row', marginTop: '3%', marginBottom: 10 }

};
