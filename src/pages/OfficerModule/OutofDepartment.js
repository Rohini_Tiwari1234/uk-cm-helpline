/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */




import { StyleSheet, Footer, Modal, ScrollView, Picker,AsyncStorage,Alert } from 'react-native';
import React, { Component, } from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Apis from '../../apis/Api';
import PApis from '../../apis/CitizenApi';

import { NavigationBar, TextInput, Divider, Icon, Title, Screen, GridRow, ListView, ImageBackground,Touchable, Tile, Subtitle, Caption, Button, Text, Image, View, TouchableOpacity, Card, DropDownMenu, selectedCar, Spinner, Row } from '@shoutem/ui';

export default class OutofDepartment extends Component {
    constructor(props) {
        super(props);
        this.state = {
    
          DistrictId: 0,
          BlockId: 0,
          GPId: 0,
          DepartmentId: 0,
          CompType: '',
          TehsilId: 0,
          ThanaId: 0,
          AnubhagId: 0,
          AttributeId: 0,
          WardId: 0,
          VitranKendraId: 0,
          CircleId: 0,
          Description: '',
          CompDetail: [],
          District: [],
          Block: [],
          GramPanchayat: [],
          Department: [],
          Attribute: [],
          Thana: [],
          VitranKendra: [],
          Anubhag: [],
          Ward: [],
          Circle: [],
          Tehsil: [],
          thanaActive: false,
          blockActive: false,
          wardActive: false,
          anubhagActive: false,
          vkActive: false,
          circleActive: false,
          tehsilActive: false,
          userAutoFillData: [],
          loading: false,
          mobile_no: '',
          Remark:'',
          userDetail:[]
        };
      }

    componentDidMount() {
        var compdetail = this.props.navigation.getParam('CompDetail');
        console.log('comp detail by owa',compdetail)
        this.setState({CompDetail:compdetail})
        AsyncStorage.getItem("userDetail").then((value) => {
            console.log('this.state.userCountDetail');
            console.log(JSON.parse(value));
            this.setState({
                userDetail: JSON.parse(value)
            });
        });
        value=0;
        this.getDistrictName();
        this.getDepartmentName();
        this.getAttributeName(value); 
    }

    setSpinner = (st) => {
        this.setState({ loading: st });
    }

    getDepartmentName() {

        PApis.fetchDepartmentName().then((res) => {
    
          this.setState({
            Department: res.messages
          });
    
        }, (error) => { console.log(error); });
      }
    
      getAttributeName(deptid) {
        this.setState({ DepartmentId: deptid, loading: true });
        this.resetDistrict();
    
        // this.setSpinner(true);
        let attrid = 0;
        PApis.fetchAttributeName(deptid, attrid).then((res) => {
    
          this.setState({
            Attribute: res.messages
          })
          this.setSpinner(false);
    
        }, (error) => { console.log(error); });
      }
    
      getThanaName(distid) {
        this.setSpinner(true);
    
        PApis.fetchThanaName(distid).then((res) => {
    
          this.setState({
            Thana: res.messages
          })
          this.setSpinner(false);
        }, (error) => { console.log(error); });
      }
    
      getWard(distid) {
        this.setSpinner(true);
        if (distid != null) {
            PApis.fetchWard(distid).then((res) => {
    
            this.setState({
              Ward: res.messages
            })
            this.setSpinner(false);
          }, (error) => { console.log(error); });
        }
        else {
            PApis.fetchWard(this.state.DistrictId).then((res) => {
    
            this.setState({
              Ward: res.messages
            })
            this.setSpinner(false);
          }, (error) => { console.log(error); });
        }
    
      }
    
    
      getVkName(distid) {
        this.setSpinner(true);
        PApis.fetchVitranKendraName(distid).then((res) => {
    
          this.setState({
            VitranKendra: res.messages
          })
          this.setSpinner(false);
        }, (error) => { console.log(error); });
      }
    
    
      getAnubhag(distid, deptid) {
          console.log('distid, deptid in anubahg picker',distid, deptid)
        this.setSpinner(true);
        PApis.fetchAnubhag(distid, deptid).then((res) => {
    
          this.setState({
            Anubhag: res.messages
          })
          this.setSpinner(false);
        }, (error) => { console.log(error); });
      }
    
      getTehsil(distid) {
        this.setSpinner(true);
        PApis.fetchTehsilName(distid).then((res) => {
    
          this.setState({
            Tehsil: res.messages
          })
          this.setSpinner(false);
        }, (error) => { console.log(error); });
      }
    
      getCircleName(distid, deptid) {
        this.setSpinner(true);
        PApis.fetchCircleName(distid, deptid).then((res) => {
    
          this.setState({
            Circle: res.messages
          })
          this.setSpinner(false);
        }, (error) => { console.log(error); });
    
      }

      getDistrictName = () => {
        console.log("getdistrict 2");
        this.setSpinner(true);
        PApis.fetchDistrict().then((res) => {
    
          this.setState({
            District: res.messages,
    
          });
          this.setSpinner(false);
          this.renderDistrictPicker();
        }, (error) => { console.log(error); });
    
      }

      getBlockName(distid,DepartmentId) {
        console.log('block by district and dept',distid,DepartmentId)
        this.setSpinner(true);
        PApis.GetBlocksByDistByDep(distid,DepartmentId).then((res) => {
          console.log('block by district and dept response',res)
          this.setState({
            Block: res.messages
          })
          this.setSpinner(false);
        }, (error) => { console.log(error); });
      }
  
      getGramPanchayatName(distid) {
        this.setSpinner(true);
        PApis.fetchDistwiseGramPanchayat(distid).then((res) => {
    
          this.setState({
            GramPanchayat: res.messages
          })
          this.setSpinner(false);
        }, (error) => { console.log(error); });
    
      }

      renderDistrictPicker() {

        let report = this.state.District;
        if (report.length > 0) {
    
          let projectEventTypes = [
            { label: 'जिला *', value: null }
          ];
          report.forEach((eventType) => projectEventTypes.push({ label: eventType.Name, value: eventType.Id }));
          return (
            <Picker style={{ borderBottomWidth: 0.3 }}
              selectedValue={this.state.DistrictId}
              mode="dropdown"
              onValueChange={(value) => { this.setState({ DistrictId: value }); this.functionForPickerSelection(value); }}
            >
              {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
            </Picker>
          );
        }
        return null;
    
      }

      renderBlockPicker() {

        let report = this.state.Block;
        if (report != null) {
    
          let projectEventTypes = [
            { label: 'ब्लॉक *', value: null }
    
          ];
          report.forEach((eventType) => projectEventTypes.push({ label: eventType.Block_Name_H, value: eventType.Block_Code }));
          return (
            <Picker
              enabled={this.state.blockActive}
              selectedValue={this.state.BlockId}
              mode="dropdown"
              onValueChange={(value) => { this.setState({ BlockId: value }); this.getWard(this.state.DistrictId) }}
            >
              {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
            </Picker>
          );
        }
        return null;
    
      }
    

      renderDepartmentPicker() {

        let report = this.state.Department;
    
        if (report.length > 0) {
    
          let projectEventTypes = [
            { label: 'विभाग *', value: null }
          ];
          report.forEach((eventType) => projectEventTypes.push({ label: eventType.Department_Name, value: eventType.subDepid }));
          return (
            <Picker
              selectedValue={this.state.DepartmentId}
              mode="dropdown"
              onValueChange={(value) => { this.getAttributeName(value); }}
    
            >
              {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
            </Picker>
          );
        }
        return null;
      }
      renderAttributePicker() {
    
        let report = this.state.Attribute;
        if (report.length > 0) {
    
          let projectEventTypes = [
            { label: 'शिकायत की श्रेणी *', value: null }
          ];
          report.forEach((eventType) => projectEventTypes.push({ label: eventType.attribname, value: eventType.attribid }));
          return (
            <Picker
              selectedValue={this.state.AttributeId}
              mode="dropdown"
              onValueChange={(value) => { this.setState({ AttributeId: value }); }}
            >
              {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
            </Picker>
          );
        }
        return null;
      }
    
      renderThanaPicker() {
    
        let report = this.state.Thana;
        if (report.length > 0) {
          let projectEventTypes = [
            { label: 'थाना * ', value: null }
    
          ];
          report.forEach((eventType) => projectEventTypes.push({ label: eventType.Thana, value: eventType.ThanaId }));
          return (
            <Picker
    
              selectedValue={this.state.ThanaId}
              mode="dropdown"
              onValueChange={(value) => { this.setState({ ThanaId: value }); }}
            >
              {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
            </Picker>
          );
        }
        return null;
      }
    
      renderWardPicker() {
    
        let report = this.state.Ward;
    
        if (report.length > 0) {
    
          let projectEventTypes = [
            { label: 'वार्ड *', value: null }
    
          ];
          report.forEach((eventType) => projectEventTypes.push({ label: eventType.wardname, value: eventType.wardid }));
          return (
            <Picker
    
              selectedValue={this.state.WardId}
              mode="dropdown"
              onValueChange={(value) => { this.setState({ WardId: value }); }}
            >
              {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
            </Picker>
          );
        }
               return null;
       
      }
    
      renderVitranKendraPicker() {
    
        let report = this.state.VitranKendra;
        if (report.length > 0) {
          let projectEventTypes = [
            { label: 'वितरण केंद्र *', value: null }
    
          ];
          report.forEach((eventType) => projectEventTypes.push({ label: eventType.Name, value: eventType.VKId }));
          return (
            <Picker
    
              selectedValue={this.state.VitranKendraId}
              mode="dropdown"
              onValueChange={(value) => { this.setState({ VitranKendraId: value }); }}
            >
              {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
            </Picker>
          );
        } return null;
      }
    
      renderAnubhagPicker() {
    console.log('this.state.Anubhag',this.state.Anubhag)
        let report = this.state.Anubhag;
        if (report.length > 0) {
          let projectEventTypes = [
            { label: 'अनुभाग *', value: null }
    
          ];
          report.forEach((eventType) => projectEventTypes.push({ label: eventType.SubDivName, value: eventType.SubDivId }));
          return (
            <Picker
    
    
              selectedValue={this.state.AnubhagId}
              mode="dropdown"
              onValueChange={(value) => { this.setState({ AnubhagId: value });console.log('AnubhagId',value) }}
            >
              {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
            </Picker>
          );
        }
        return null;
      }
      renderTehsilPicker() {
    
        let report = this.state.Tehsil;
        if (report.length > 0) {
          let projectEventTypes = [
            { label: 'तहसील *', value: null }
    
          ];
          report.forEach((eventType) => projectEventTypes.push({ label: eventType.Tehsil_Name_H, value: eventType.Tehsil_Code }));
          return (
            <Picker
    
              selectedValue={this.state.TehsilId}
              mode="dropdown"
              onValueChange={(value) => { this.setState({ TehsilId: value }); }}
            >
              {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
            </Picker>
          );
        }
        return null;
      }
      renderCirclePicker() {
    
        let report = this.state.Circle;
        if (report.length > 0) {
          let projectEventTypes = [
            { label: 'सर्किल *', value: null }
    
          ];
          report.forEach((eventType) => projectEventTypes.push({ label: eventType.CircleName, value: eventType.CircleId }));
          return (
            <Picker
    
              selectedValue={this.state.CircleId}
              mode="dropdown"
              onValueChange={(value) => { this.setState({ CircleId: value }); }}
            >
              {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
            </Picker>
          );
        }
    
        return null;
    
      }

      renderGramPanchayatPicker() {
        if(this.state.GramPanchayat!=undefined){
          console.log('this.state.GramPanchayat',this.state.GramPanchayat)
          let report = this.state.GramPanchayat;
          if (report.length > 0) {
      
            let projectEventTypes = [
              { label: 'ग्राम पंचायत *', value: null }
            ];
            report.forEach((eventType) => projectEventTypes.push({ label: eventType.GP_Name_H, value: eventType.GP_Code }));
            return (
              <Picker
                selectedValue={this.state.GPId}
                mode="dropdown"
                onValueChange={(value) => { this.setState({ GPId: value }) }}
              >
                {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
              </Picker>
            );
          }
        }
        return null;
    
      }
    
    
    
      // functionForPickerSelection = (distid) => {
    
      //   if (this.state.DepartmentId == 6 && distid != null) {
      //     if (distid != null) {
      //       this.setState({
      //         anubhagActive: true,
      //         vkActive: true,
      //         thanaActive: false,
      //         blockActive: false,
      //         wardActive: false,
      //         circleActive: false,
      //         tehsilActive: false
      //       })
      //       this.getVkName(distid);
      //       this.getAnubhag(distid, this.state.DepartmentId);
    
      //     }
      //     else {
      //       alert('"कृपया ज़िला दर्ज करें।');
      //     }
      //   }
      //   else if (this.state.DepartmentId == 12 && distid != null) {
      //     if (distid != null) {
      //       this.setState({
      //         anubhagActive: true,
      //         vkActive: false,
      //         thanaActive: true,
      //         blockActive: false,
      //         wardActive: false,
      //         circleActive: false,
      //         tehsilActive: false
      //       })
      //       this.getThanaName(distid);
      //       this.getAnubhag(distid, this.state.DepartmentId);
      //     }
      //     else {
      //       alert('"कृपया ज़िला दर्ज करें।');
      //     }
      //   }
      //   else if (this.state.DepartmentId == 138 && distid != null) {
      //     if (distid != null) {
      //       this.setState({
      //         anubhagActive: false,
      //         vkActive: false,
      //         thanaActive: false,
      //         blockActive: false,
      //         wardActive: false,
      //         circleActive: true,
      //         tehsilActive: false
      //       })
      //       this.getCircleName(distid, this.state.DepartmentId);
    
      //     }
      //     else {
      //       alert('"कृपया ज़िला दर्ज करें।');
      //     }
      //   }
      //   else if (this.state.DepartmentId == 34 || this.state.DepartmentId == 1002 || this.state.DepartmentId == 1006 || this.state.DepartmentId == 53 || this.state.DepartmentId == 151 || this.state.DepartmentId == 149
      //     || this.state.DepartmentId == 1007 || this.state.DepartmentId == 156 || this.state.DepartmentId == 150 || this.state.DepartmentId == 153 && distid != null) {
      //     if (distid != null) {
      //       this.setState({
      //         anubhagActive: false,
      //         vkActive: false,
      //         thanaActive: false,
      //         blockActive: false,
      //         wardActive: false,
      //         circleActive: false,
      //         tehsilActive: true
      //       })
      //       this.getTehsil(distid);
      //     }
      //     else {
      //       alert('"कृपया ज़िला दर्ज करें।');
      //     }
    
      //   }
      //   else if (this.state.DepartmentId == 1042 || this.state.DepartmentId == 1041 || this.state.DepartmentId == 1030 || this.state.DepartmentId == 182 || this.state.DepartmentId == 1016 || this.state.DepartmentId == 1043
      //     || this.state.DepartmentId == 181 || this.state.DepartmentId == 184 || this.state.DepartmentId == 180 || this.state.DepartmentId == 1040 || this.state.DepartmentId == 1014 || this.state.DepartmentId == 1075
      //     || this.state.DepartmentId == 1031 || this.state.DepartmentId == 185 || this.state.DepartmentId == 187 || this.state.DepartmentId == 183 || this.state.DepartmentId == 179 || this.state.DepartmentId == 168
      //     || this.state.DepartmentId == 178 || this.state.DepartmentId == 1015 || this.state.DepartmentId == 176 || this.state.DepartmentId == 186 || this.state.DepartmentId == 177 || this.state.DepartmentId == 169
      //     || this.state.DepartmentId == 173 || this.state.DepartmentId == 171 || this.state.DepartmentId == 174 || this.state.DepartmentId == 172 || this.state.DepartmentId == 170 || this.state.DepartmentId == 175
      //     && distid != null) {
      //     if (distid != null) {
      //       this.setState({
      //         anubhagActive: false,
      //         vkActive: false,
      //         thanaActive: false,
      //         blockActive: true,
      //         wardActive: true,
      //         circleActive: false,
      //         tehsilActive: false
      //       })
      //       this.getBlockName(distid);
      //       this.getWard(distid);
      //     }
      //     else {
    
      //       alert('"कृपया ज़िला दर्ज करें।');
      //     }
    
      //   }
      //   else {
      //     if (distid != null) {
      //       this.setState({
      //         anubhagActive: false,
      //         vkActive: false,
      //         thanaActive: false,
      //         blockActive: true,
      //         wardActive: false,
      //         circleActive: false,
      //         tehsilActive: false
      //       })
      //       this.getBlockName(distid);
    
      //     }
      //     else {
    
      //       alert('"कृपया ज़िला दर्ज करें।');
      //     }
      //   }
      // }


      functionForPickerSelection = (distid) => {

        if (this.state.DepartmentId == 95 && distid != null) {
          if (distid != null) {
            this.setState({
              anubhagActive: true,
              vkActive: true,
              thanaActive: false,
              blockActive: false,
              wardActive: false,
              circleActive: false,
              tehsilActive: false
            })
            this.getVkName(distid);
            this.getAnubhag(distid, this.state.DepartmentId);
    
          }
          else {
            this.showToast('"कृपया ज़िला दर्ज करें।');
          }
        }
        else if (this.state.DepartmentId == 24 && distid != null) {
          if (distid != null) {
            this.setState({
              anubhagActive: true,
              vkActive: false,
              thanaActive: true,
              blockActive: false,
              wardActive: false,
              circleActive: false,
              tehsilActive: false
            })
            this.getThanaName(distid);
            this.getAnubhag(distid, this.state.DepartmentId);
          }
          else {
            this.showToast('"कृपया ज़िला दर्ज करें।');
          }
        }
        // else if (this.state.DepartmentId == 138 && distid != null) {
        //   if (distid != null) {
        //     this.setState({
        //       anubhagActive: false,
        //       vkActive: false,
        //       thanaActive: false,
        //       blockActive: false,
        //       wardActive: false,
        //       circleActive: true,
        //       tehsilActive: false
        //     })
        //     this.getCircleName(distid, this.state.DepartmentId);
    
        //   }
        //   else {
        //     this.showToast('"कृपया ज़िला दर्ज करें।');
        //   }
        // }
        else if (this.state.DepartmentId == 2 || this.state.DepartmentId == 32 || this.state.DepartmentId == 62  && distid != null) {
          if (distid != null) {
            this.setState({
              anubhagActive: false,
              vkActive: false,
              thanaActive: false,
              blockActive: false,
              wardActive: false,
              circleActive: false,
              tehsilActive: true
            })
            this.getTehsil(distid);
          }
          else {
            this.showToast('"कृपया ज़िला दर्ज करें।');
          }
    
        }
        // else if (this.state.DepartmentId == 1042 || this.state.DepartmentId == 1041 || this.state.DepartmentId == 1030 || this.state.DepartmentId == 182 || this.state.DepartmentId == 1016 || this.state.DepartmentId == 1043
        //   || this.state.DepartmentId == 181 || this.state.DepartmentId == 184 || this.state.DepartmentId == 180 || this.state.DepartmentId == 1040 || this.state.DepartmentId == 1014 || this.state.DepartmentId == 1075
        //   || this.state.DepartmentId == 1031 || this.state.DepartmentId == 185 || this.state.DepartmentId == 187 || this.state.DepartmentId == 183 || this.state.DepartmentId == 179 || this.state.DepartmentId == 168
        //   || this.state.DepartmentId == 178 || this.state.DepartmentId == 1015 || this.state.DepartmentId == 176 || this.state.DepartmentId == 186 || this.state.DepartmentId == 177 || this.state.DepartmentId == 169
        //   || this.state.DepartmentId == 173 || this.state.DepartmentId == 171 || this.state.DepartmentId == 174 || this.state.DepartmentId == 172 || this.state.DepartmentId == 170 || this.state.DepartmentId == 175
        //   && distid != null) {
        //   if (distid != null) {
        //     this.setState({
        //       anubhagActive: false,
        //       vkActive: false,
        //       thanaActive: false,
        //       blockActive: true,
        //       wardActive: true,
        //       circleActive: false,
        //       tehsilActive: false
        //     })
        //     this.getBlockName(distid);
        //     this.getWard(distid);
        //   }
        //   else {
    
        //     this.showToast('"कृपया ज़िला दर्ज करें।');
        //   }
    
        // }
        else {
          if (distid != null) {
            this.setState({
              anubhagActive: false,
              vkActive: false,
              thanaActive: false,
              blockActive: true,
              wardActive: false,
              circleActive: false,
              tehsilActive: false
            })
            this.getBlockName(distid, this.state.DepartmentId);
    
          }
          else {
    
            this.showToast('"कृपया ज़िला दर्ज करें।');
          }
        }
      }


      resetDistrict = () => {
        if (this.state.DistrictId != null) {
    
          this.setState({
            DistrictId: null,
            thanaActive: false,
            blockActive: false,
            wardActive: false,
            anubhagActive: false,
            vkActive: false,
            circleActive: false,
            tehsilActive: false,
          });
        }
        else {
          this.getDistrictName();
          
        }
      }
     
    UpdateCompToOutOfDepartment=()=>{
       
       var Dept=this.state.DepartmentId, Attribute=this.state.AttributeId, Dist=this.state.DistrictId, Block=this.state.BlockId, Thana=this.state.ThanaId,
        Circle=this.state.CircleId, Range=0, Div=0, Zone=0, Teh=this.state.TehsilId, Ward=this.state.WardId, VK=this.state.VitranKendraId, SubDiv=this.state.AnubhagId,
         GP=0, G=0, Inst=0, Remark=this.state.Remark, CompID=this.state.CompDetail.tid, officerid=this.state.userDetail.id;
        
         console.log('parameter to  update',Dept, Attribute, Dist, Block, Thana, Circle, Range, Div, Zone, Teh, Ward, VK, SubDiv, GP, G, Inst, Remark, CompID, officerid)
        
         if(this.state.Remark!='' && this.state.Remark!=null){
            Apis.OWAServicesSaveComplaint(Dept, Attribute, Dist, Block, Thana, Circle, Range, Div, Zone, Teh, Ward, VK, SubDiv, GP, G, Inst, Remark, CompID, officerid).then((res) => {
        
             if (res.status == 'success') {
                 Alert.alert(
                   'Alert',
                   'शिकायत क्रमांक : ' + this.state.CompDetail.tid + ' को अपडेट कर दिया गया है।',
                   [
                     // { text: 'Cancel', onPress: () => this.setSpinner(false) },
                     { text: 'OK', onPress: () => {this.setState({Remark:''}) } },
                     //{text: 'OK', onPress: () => console.log('OK Pressed')},
                   ],
                   { cancelable: false }
                 )
                 this.props.navigation.navigate('Dashboard');
                 
                           
               }
               else {
                 this.setSpinner(false);
                 this.showToast('"माफ़ कीजिये: शिकायत अपडेट करने में असमर्थ!');
               }
 
             }, (error) => {
               this.setSpinner(false);
               console.log(error);
              
             })
            }
            else{
                alert('कृपया निराकरण दर्ज करें !')
            }

    }

render() {
   



    return (


        
        <Screen style={{ height: '100%', color: '#fff' }}>

<NavigationBar style={{ container: { Index: 1, backgroundColor: '#117bc7', color: '#fff', borderBottomWidth: 0, borderColor: 'none' } }}
                  statusBarColor="#268DC8"
                  leftComponent={
                    <Touchable onPress={() => this.props.navigation.goBack()} >
                    <Icon style={{ color: '#fff', fontSize: 35,marginLeft:10 }} name="left-arrow" />
                    </Touchable>}
                  
                  centerComponent={<Text style={{   fontSize: 18, color: '#fff',width:230,alignSelf:'center',fontWeight:'500' }}>

                 
                 कार्य क्षेत्र से बाहर
                      
</Text>}
rightComponent={ <Touchable onPress={() => this.props.navigation.navigate('Dashboard')} >
< MaterialCommunityIcons style={{ color: '#fff', fontSize: 30,marginRight:10 }} name="home" />
</Touchable>}
                />

                <View  style={{ marginTop:70, }}>
                {this.renderDepartmentPicker()}
                <Divider styleName="line" style={{ borderBottomWidth: 2, borderBottomColor: '#E6E6E6', width: '96%', marginLeft: '3%', marginRight: '3%' }} />

                {this.renderAttributePicker()}
               
                <Divider styleName="line" style={{ borderBottomWidth: 2, borderBottomColor: '#E6E6E6', width: '96%', marginLeft: '3%', marginRight: '3%' }} />
                {this.renderDistrictPicker()}

                
                {this.state.thanaActive ? (this.renderThanaPicker()) : null}
                <Divider styleName="line" style={{ borderBottomWidth: 2, borderBottomColor: '#E6E6E6', width: '96%', marginLeft: '3%', marginRight: '3%' }} />
                
                {this.state.blockActive ? (this.renderBlockPicker()) : null}
                <Divider styleName="line" style={{ borderBottomWidth: 2, borderBottomColor: '#E6E6E6', width: '96%', marginLeft: '3%', marginRight: '3%' }} />
                
                {this.state.wardActive ? (this.renderWardPicker()) : null}
                <Divider styleName="line" style={{ borderBottomWidth: 2, borderBottomColor: '#E6E6E6', width: '96%', marginLeft: '3%', marginRight: '3%' }} />
                {this.state.anubhagActive ? (this.renderAnubhagPicker()) : null}
                
                <Divider styleName="line" style={{ borderBottomWidth: 2, borderBottomColor: '#E6E6E6', width: '96%', marginLeft: '3%', marginRight: '3%' }} />
                
                {this.state.tehsilActive ? (this.renderTehsilPicker()) : null}
                <Divider styleName="line" style={{ borderBottomWidth: 2, borderBottomColor: '#E6E6E6', width: '96%', marginLeft: '3%', marginRight: '3%' }} />

                {this.state.vkActive ? (this.renderVitranKendraPicker()) : null}
                <Divider styleName="line" style={{ borderBottomWidth: 2, borderBottomColor: '#E6E6E6', width: '96%', marginLeft: '3%', marginRight: '3%' }} />
               
                {this.state.circleActive ? (this.renderCirclePicker()) : null}
                <Divider styleName="line" style={{ borderBottomWidth: 2, borderBottomColor: '#E6E6E6', width: '96%', marginLeft: '3%', marginRight: '3%' }} />
               


                {/* {this.renderGramPanchayatPicker()} */}

                <Divider styleName="line" style={{ borderBottomWidth: 2, borderBottomColor: '#E6E6E6', width: '96%', marginLeft: '3%', marginRight: '3%' }} />

                <TextInput
                         multiline={true} 
                         placeholderTextColor="#fff"
                         placeholder="कार्रवाई"
                         value={this.state.Remark}
                          onChangeText={(text) => this.setState({ Remark:text })} >

                            
                        </TextInput>
         
    </View>
            <Button style={{backgroundColor: '#268DC8',width:300,marginLeft:30,marginTop:30,borderRadius:10,borderColor:'#268DC8'}} styleName="secondary" onPress={()=>this.UpdateCompToOutOfDepartment()} >
  <Text>कार्य क्षेत्र से बाहर</Text>
</Button>

{this.state.loading ?
    <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)', alignItems: 'center', justifyContent: 'center', position: 'absolute', width: '100%', height: '100%' }}>
        <Spinner color="white" />
    </View> : null}

        </Screen >
    );
}
}
const styles = {



    FooterView:
    {
        height: '100%',
        backgroundColor: '#FBB040',
        flexDirection: 'row',
        height: '15%'
    },

    FileIcon: {
        height: '60%',
        width: '12%',
        marginTop: '3%',
        marginLeft: '5%'

    },

    CalendarIcon: {
        height: '60%',
        width: '12%',
        marginTop: '3%',
        marginLeft: '8%'
    },

    SyncIcon: {
        height: '60%',
        width: '12%',
        marginTop: '3%',
        marginLeft: '8%'
    },



    SnagIcon: {
        height: '60%',
        width: '12%',
        marginTop: '3%',
        marginLeft: '4%'
    },
    Divider: {
        borderBottomColor: '#B3B3B3',
        borderBottomWidth: 0.3,
        marginLeft: '3%',
        marginRight: '3%',
    },


    ButtonView: {
        flex: 1,
        backgroundColor: '#414143',
        marginTop: '5%',

    },

    Text: {
        fontSize: 12,
        color: '#fff',
        paddingLeft: '45%',
        marginTop: '5%'
    },
    card1: {
        marginLeft: '5%',
        borderRadius: 5,
        backgroundColor: '#FFDC98',
        width: '25%'
    },

    Text2: {
        fontSize: 12,
        color: '#fff',
        paddingLeft: '43%',
        marginTop: '5%'
    },

    SlelectSnagText: {
        fontWeight: 'bold',
        fontSize: 14,
        color: '#FCAE3A',
        marginLeft: '45%',
        marginTop: '5%'
    },


    modalView: {
        flex: 1,
        backgroundColor: '#00000040'
    },

    TouchableHighlight: {
        flex: 1,

    },
    TouchableOpacity: {
        // borderWidth: 1,
        alignItems: 'center',
        width: 50,
        height: 50,
        marginLeft: '80%',
        backgroundColor: '#404045',
        borderRadius: 100,
        marginTop: '-5%',
        marginLeft: '45%'


    },





    Dot: {
        fontSize: 35,
        fontWeight: 'bold',
        marginTop: '-8%',
        color: '#CDCDCD'
    },


    search: {
        marginLeft: '10%',
        marginTop: '3%',
        height: '50%',
        width: '10%'

    },

    Bell: {
        // marginLeft: '5%',
        marginRight: '10%',
        height: '50%',
        width: '10%'


    },

    filter: {   // marginLeft: '5%',
        marginTop: '3%',
        height: '50%',
        width: '10%'
    },


    sidebar: {

        color: '#fff',
        fontSize: 30
    },

    SnagText: {

        color: '#fff',
        fontSize: 12,
        marginBottom: 4,
        marginLeft: '5%',
        fontWeight: 'bold'

    },



    Active: {
        fontFamily: 'CeraPro-Bold',
        fontSize: 10,
        backgroundColor: '#FEA500',

        marginRight: '50%',
        paddingLeft: '5%',
        paddingRight: '5%',
        paddingTop: 4,
        paddingBottom: 4,
        alignSelf: 'center',
        color: '#FFFFFF',
        borderRadius: 5,
        paddingBottom: 5

    },


    navigationBar: {

        backgroundColor: '#414143',
        flexDirection: 'row',
        height: '8%'

    },







    PageName: {
        fontSize: 14,
        marginRight: '50%',
        color: '#fff',
        marginTop: '5%',
        fontWeight: 'bold'
    },




    MapView: {
        height: '100%',
        paddingBottom: 0,
        marginTop: '30%'
    },


    MapImage: {
        marginTop: '30%',
        height: 50,
        width: '20%',

    },
    teaicon: {
        width: 30,
        height: 45
    },

    View1: {
        borderColor: '#ffb52e',
        height: '100%',
        marginLeft: '2%'
    },

    teaURL: {

        color: '#580f00',
        marginTop: 10,
        fontWeight: 'bold',
        marginLeft: '3%'
    },
    teaicon: {
        width: 30,
        height: 45
    },

    View1: {
        borderColor: '#ffb52e',
        height: '100%',
        marginLeft: '2%'
    },

    teaURL: {

        color: '#580f00',
        marginTop: 10,
        fontWeight: 'bold'
    },
    HeaderNavIcon: {
        fontSize: 22,
        padding: 3,
        paddingLeft: 6
    },

    View2: {
        flexDirection: 'row',
        marginLeft: '-1%',
        width: '50%'
    },

    searchicon: {
        fontSize: 15
    },


    placeholder: {
        height: 40,
        width: '67%',
        fontWeight: 'bold'
    },

    cardscroll: {

        marginLeft: 2,
        marginTop: 5,

        borderRadius: 5


    },
    listStyle: {
        marginLeft: 3,
        paddingBottom: 4,
        marginRight: 0,
        paddingTop: 2
    },

    textstyle: {
        color: '#47e02c',
        fontSize: 30,
        fontWeight: '700',
        marginLeft: 3,
        textAlign: 'center',
    },
    textstyle1: {
        color: '#141414',
        fontSize: 24,
        fontWeight: '500',
        marginLeft: 3,
        textAlign: 'center',
    },
    textstyle2: {
        color: '#141414',
        fontSize: 12,

        fontFamily: 'Montserrat-Semibold'
    },

    View3: {
        flexDirection: 'row',
        backgroundColor: '#ffb52e',
        borderColor: '#ffb52e',
        height: '100%',
        marginLeft: '2%'
    },

    username: {
        marginLeft: '1%',
        marginTop: '5%'
    },

    profilepic: {

        marginTop: '1%',


    },

    logouticon: {
        marginTop: 6,
        fontSize: 25,
        marginLeft: 8
    },

    ImageBackground: {
        width: '100%',
        height: '100%',
        borderRadius: 5,
        marginTop: 5
    },

    View4: {
        flexDirection: 'row',
        backgroundColor: '#f2f2f2'
    },

    usertext: {
        fontWeight: 'bold',
        marginLeft: '5%'
    },
    View6: {

        backgroundColor: '#614716',
        marginLeft: '45%',


    },

    srnotext: {

        color: '#530a08'
    },

    tabletext: {
        marginLeft: '8%',
        color: '#530a08'
    },

    srnodata: {
        fontWeight: 'bold',
        fontSize: 16,
        paddingTop: '1%'
    },

    profilepicdata: {
        fontSize: 16,
        marginLeft: '20%',
        paddingTop: '3%'
    },

    namedata: {
        fontSize: 16,
        paddingTop: '4%',
        width: '150%'
    },

    mobilenodata: {
        fontSize: 16,
        paddingTop: '2%'
    },

    statusdata: {
        fontSize: 14,
        backgroundColor: '#3BB44B',
        borderRadius: 5,
        paddingTop: '5%',
        height: '45%'
    },

    Actiondata: {
        fontSize: 14,
        backgroundColor: '#570909',
        color: '#F9991E',
        borderRadius: 5,
        height: '45%',
        width: '100%'
    },


    MainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },

    TextStyle: {
        fontSize: 20,
        color: '#000'
    },
    buttonText: {
        fontSize: 11,
        color: '#580f00',
        fontWeight: '400',
        paddingLeft: 0,
        paddingRight: 0
    },

    inactiveTab: {
        backgroundColor: '#fff',
        padding: 0
    },
    ImageStyle: {
        top: 0,
        left: 0,
        bottom: 0,
        right: 0

    },

    ListItemCountStyle: {
        alignSelf: 'center',
        borderColor: '#d26635',
        borderWidth: 1,
        borderBottomWidth: 1,
        paddingTop: 2,
        paddingBottom: 2,
        borderRadius: 5

    },
    ListItemIncreStyle: {
        alignSelf: 'center',
        borderColor: '#d26635',
        borderWidth: 1,
        borderBottomWidth: 1,
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 5,
        marginLeft: 2
    },
    IncreStyle: {
        color: '#d26635',
        paddingLeft: 18,
        fontWeight: 'bold',
        fontSize: 25,
        fontWeight: '600',
        // alignSelf: 'center'
    },
    DecreStyle: {
        color: '#d26635',
        paddingLeft: 5,
        paddingRight: 18,
        fontWeight: 'bold',
        fontSize: 25,
        fontWeight: '600',
        // alignSelf: 'center'
    },
    CounterStyle: {
        fontSize: 13,
        color: '#f45a24',
        marginLeft: '30%',
        alignSelf: 'center',
        paddingLeft: 6,
        paddingRight: 6
    },
    CounterStyle1: {
        fontSize: 13,
        color: '#f45a24',
        marginLeft: '30%',
        alignSelf: 'center',
        paddingLeft: 6,
        paddingRight: 6
    },
    TextInputStyleClass: {
        height: 33,
        fontSize: 15,
        flex: 1,
        paddingTop: 6
    }


}







