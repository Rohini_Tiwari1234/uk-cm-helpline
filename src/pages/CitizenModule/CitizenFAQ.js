import React, { Component, Animated } from 'react';
import {
  StyleSheet,
    AsyncStorage,
    BackHandler,
    StatusBar
  } from 'react-native';
  import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
  import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { ScrollView,Button,Title,Touchable, Text, View, Row, TouchableOpacity,NavigationBar,Icon } from '@shoutem/ui';
export default class FAQs extends Component<{}> {
    
    constructor(props) {
        super(props);
        this.state = {
          proprtyList:0,
          status: false,
          status1: false,
          view:false,
          view1:false,
          text:false,
          text1:false,
          app:false,
          app1:false,
          app2:false
         
        };
    
      }
  
      ShowHideTextComponentView1 = () => {

        if (this.state.status1 == true) {
          this.setState({ status1: false })
        }
        else {
          this.setState({ status1: true })
        }
      }
    
    
      ShowHideTextComponentstatus1 = () => {
    
        if (this.state.view1 == true) {
          this.setState({ view1: false })
        }
        else {
          this.setState({ view1: true })
        }
      }
    
      ShowHideTextComponentView2 = () => {
    
        if (this.state. text1 == true) {
          this.setState({  text1: false })
        }
        else {
          this.setState({  text1: true })
        }
      }
    
      ShowHideTextComponentView3 = () => {

        if (this.state.app1 == true) {
          this.setState({ app1: false })
        }
        else {
          this.setState({ app1: true })
        }
      }
      ShowHideTextComponentView4 = () => {

        if (this.state.app2 == true) {
          this.setState({ app2: false })
        }
        else {
          this.setState({ app2: true })
        }
      }
   


    render () {
        return (
           
            <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
            <StatusBar barStyle='light-content'
                    backgroundColor='#117bc7' />
       <NavigationBar style={{container: { zIndex: 1, backgroundColor: '#117bc7', borderBottomWidth: 0, borderColor: 'none' }}}
leftComponent=
{<Touchable onPress={() => this.props.navigation.goBack()} >
    < Icon style={{ color: '#fff', fontSize: 30,marginLeft:10 }} name="left-arrow" />
</Touchable>
}
centerComponent={
    <Title style={styles.navigationBarTitleColor} >FAQ's  </Title>
}

rightComponent={(
    <Touchable onPress={() => this.props.navigation.goBack()} >
        < MaterialCommunityIcons style={{ color: '#fff', fontSize: 30,marginRight:10 }} name="home" />
    </Touchable>
)}
/>
     
            <ScrollView>
              <View>
                <View style={{ marginTop: '12%', backgroundColor: '#FFFFFF' }} >
                  <TouchableOpacity onPress={() => this.ShowHideTextComponentView1()}>
                    <Row>
                     <Text style={{fontSize:20,fontWeight: 'bold',marginTop:10}}>
                     प्रश्न.   <Text style={{ marginTop:5,fontWeight: 'bold'}}>
                      सी एम हेल्पलाइन पर संपर्क कैसे किया जा सकता है ?</Text></Text>
                      <FontAwesome name="plus-circle" style={{bottom:5}}size={18} color={"black"} onPress={() => this.ShowHideTextComponentView1()} />
                    </Row>
                  </TouchableOpacity>
    
    
                  {this.state.status1 ? (
                   
                   <Text style={{fontSize:20,marginLeft:15}}>
                    उत्तर.   <Text >
    सी एम हेल्पलाइन पर संपर्क करने के लिए टोल फ्री नंबर 1905 पर कॉल करें |  </Text></Text>)
                    : null}
    
    
    
    
                  <View
                    style={{
                      borderBottomColor: '#E6E6E6',
                      borderBottomWidth: 1,
                      paddingTop: '1%',
    
    
                    }}
                  />
                  <TouchableOpacity onPress={() => this.ShowHideTextComponentstatus1()}>
                    <Row >

                    <Text style={{fontSize:20,fontWeight: 'bold'}}>
                     प्रश्न.   <Text style={{ marginTop: 5,fontWeight: 'bold'}}>
                      क्या सी ऍम हेल्पलाइन पर संपर्क करने की कोई समय सीमा है ?</Text></Text>
                      <FontAwesome name="plus-circle" style={{bottom:5}}size={18} color={"black"} onPress={() => this.ShowHideTextComponentstatus1()} />
                    </Row>
                  </TouchableOpacity>
    
    
                  {this.state.view1 ? (
                   
                   <Text style={{fontSize:20,marginLeft:15}}>
                   उत्तर.   <Text >
     कॉल करने का समय सुबह 8 बजे से रात्रि 10 बजे तक का है |</Text></Text>)
                    : null}
    
    
    
    
    
                  <View
                    style={{
                      borderBottomColor: '#E6E6E6',
                      borderBottomWidth: 1,
                      paddingTop: '1%'
                    }}
                  /> 
               <TouchableOpacity  onPress={() => this.ShowHideTextComponentView2()}>
                    <Row>
                    <Text style={{fontSize:20,fontWeight: 'bold'}}>
                     प्रश्न.   <Text style={{  marginTop: 5,fontWeight: 'bold' }}>
                   किस प्रकार की जानकारी यहाँ प्राप्त हो सकती है ? </Text> </Text>
                      <FontAwesome name="plus-circle"style={{bottom:5}} size={18} color={"black"} onPress={() => this.ShowHideTextComponentView2()} />
                    </Row>
                  </TouchableOpacity>
    
    
    
                  {this.state.text1 ? (
                     <Text style={{fontSize:20,marginLeft:15}}>
                     उत्तर.   <Text >
     राज्य शासन से सम्बंधित सभी योजनाओ की जानकारी के लिए आप संपर्क कर सकते है | </Text></Text>)
                    : null}
    
    
    
    
                  <View
                    style={{
                      borderBottomColor: '#E6E6E6',
                      borderBottomWidth: 1,
                      paddingTop: '1%'
                    }}
                  />
                 
                 <TouchableOpacity  onPress={() => this.ShowHideTextComponentView3()}>
                    <Row>
                    <Text style={{fontSize:20,fontWeight: 'bold'}}>
                     प्रश्न.   <Text style={{ marginTop: 5,fontWeight: 'bold' }}>
                      क्या सी एम हेल्पलाइन पर अन्य कोई सुविधा प्राप्त हो सकती है ? </Text> </Text>
                      <FontAwesome name="plus-circle"style={{bottom:5}} size={18} color={"black"} onPress={() => this.ShowHideTextComponentView3()} />
                    </Row>
                  </TouchableOpacity>
                  {this.state.app1 ? (
                    
                    <Text style={{fontSize:20,marginLeft:15}}>
                   उत्तर.   <Text >
      आपात कालीन सेवा जैसे 108, 100, 102,जननी एक्सप्रेस वाहन से सम्बंधित जानकारी भी प्राप्त कर सकते है |  </Text></Text>)
                    : null}
    
    
    
    
                  <View
                    style={{
                      borderBottomColor: '#E6E6E6',
                      borderBottomWidth: 1,
                      paddingTop: '1%'
                    }}
                  />

<TouchableOpacity  onPress={() => this.ShowHideTextComponentView4()}>
                    <Row>
                    <Text style={{fontSize:20,fontWeight: 'bold'}}>
                     प्रश्न.   <Text style={{  marginTop: 5,fontWeight: 'bold' }}>
                      किस प्रकार की शिकायते यहाँ पर दर्ज करा सकते है ?  </Text></Text>
                      <FontAwesome name="plus-circle" style={{bottom:5}}size={18} color={"black"} onPress={() => this.ShowHideTextComponentView4()} />
                    </Row>
                  </TouchableOpacity>
                  {this.state.app2 ? (
                    <Text style={{fontSize:20,marginLeft:15}}>
                    उत्तर.   <Text >
                    राज्य शासन द्वारा दी जा रही सभी सुविधाओ से सम्बंधित शिकायते दर्ज करा सकते है | साथ ही मांग एवं सुझाव भी दर्ज करा सकते हैं |  </Text></Text>)
                    : null}
    
    
    
    
                  <View
                    style={{
                      borderBottomColor: '#E6E6E6',
                      borderBottomWidth: 1,
                      paddingTop: '1%'
                    }}
                  />
                </View>
              </View>
            </ScrollView>
           
          </View> 
        
        
        );
    }
}


const styles = StyleSheet.create({
  navigationBarTitleColor: {
    fontSize: 20,
    color: '#fff'
}
})

