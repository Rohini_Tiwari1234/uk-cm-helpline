/**
 * Deepak S Rathode
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { ImageBackground, Spinner, Touchable, Lightbox, NavigationBar, Text, View, Screen, Tile, Title, TextInput, Row, Col, Card, Button, Icon, Divider, Caption, Subtitle, Image, ListView, TouchableOpacity, } from '@shoutem/ui';
import { ScrollView, Modal, ActivityIndicator, StatusBar, AsyncStorage, Alert } from 'react-native';
import Communications from 'react-native-communications';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Apis from '../../apis/CitizenApi';

export default class CitizenDashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userObject: [],
            countObject: [],
            userCountDetail: [],
            WIP: 'WIP',
            close: 'Closed',
            all: '',
            dismiss: 'PC',
            loading: false,
            mobile_no: '',
            status: '',
            Count: 0,


        };

    }


    componentDidMount = () => {
        AsyncStorage.getItem("mobile_no").then((value) => {

            this.setState({
                mobile_no: value
            });

            Apis.getUserDetailsByPhoneNo(value).then((res) => {

                console.log('res');
                console.log(res.messages);

                this.setSpinner(false);
                AsyncStorage.setItem("userDetail", res.messages[0]);
                this.setState({
                    userDetail: res.messages[0]
                });


            }, (error) => {
                this.setSpinner(false);

            });

        })


    }


    setSpinner = (st) => {
        this.setState({ loading: st });
    }
    ConfirmationToLogout = () => {

        Alert.alert(
            'Alert',
            'क्या आप लॉगआउट करना चाहते हैं ?',
            [
                { text: 'Cancel', onPress: () => this.setSpinner(false) },
                { text: 'OK', onPress: () => this.props.navigation.navigate('Logout') },

            ],
            { cancelable: false }
        )

    }
    render() {

        return (



            <Screen style={{ flex: 1, backgroundColor: '#fff' }}>




                <NavigationBar style={{ container: { zIndex: 1, backgroundColor: '#117bc7', borderBottomWidth: 0, borderColor: 'none' } }}
                    statusBarColor="#117bc7"
                    leftComponent={
                        <Image style={{ height: 60, width: 60, borderRadius: 50, }}
                            source={require('../../assets/icon/icon-01.png')}
                        />}

                    centerComponent={<Text style={{ fontSize: 18, color: '#fff', width: 250, alignSelf: 'center', fontWeight: '600' }}>

                        उत्तराखंड सीएम हेल्पलाइन
    
</Text>}


                    rightComponent={
                        <View styleName="horizontal" >
                            <Button styleName="clear" style={{ backgroundColor: '#117bc7' }} onPress={() => this.ConfirmationToLogout()}>
                                <MaterialCommunityIcons name="logout" style={{ fontSize: 25, color: '#fff', marginRight: 10 }}
                                />
                            </Button>

                        </View>
                    }
                />




                <ScrollView style={{ flex: 1, marginTop: 70 }}>
                    <View style={{ height: '100%' }}>







                        <Card style={styles.Card0}>
                            <Row style={{ backgroundColor: '#1D75BD', height: 2, marginTop: 7 }}>
                                <Text style={{ color: '#FEF400' }}>

                                    Welcome
                            </Text>
                                {/* <Icon style={{ color: '#fff' }} name="edit" /> */}

                            </Row>
                            <Row style={{ backgroundColor: '#1D75BD', height: 2 }}>
                                <Text style={{ color: '#fff' }} >
                                    {this.state.mobile_no}
                                </Text>
                                {/* <Text style={{ marginLeft: 50, color: '#fff' }}>

                               {this.state.mobile_no}
                            </Text> */}

                            </Row>
                            <Text style={{ color: '#fff', marginLeft: 18, marginBottom: 5, fontWeight: '12' }} >
                                {}
                            </Text>
                        </Card>
                        <View style={styles.row}>
                            <View style={{ flexdirection: 'column',flex:1, }}>
                                <Touchable style={{ alignSelf: 'center', width:'100%' }} onPress={() => this.props.navigation.navigate('CreateComp')}>
                                    <Card style={styles.Card1}>


                                        <Image style={{ color: '#fff', height: 50, width: 50, alignSelf: 'center' }}
                                            source={require('../../assets/icon/icon-02.png')}
                                        />

                                        <Text style={styles.RENTED} > शिकायत पंजीकरण </Text>

                                    </Card>
                                </Touchable>
                            </View>
                            <View style={{ flexdirection: 'column',flex:1, }}>
                                <Touchable style={{ alignSelf: 'center',width:'100%' }} onPress={() => this.props.navigation.navigate('CompList', { CallerId: this.state.userObject.CallerId, Open: this.state.all })}>
                                    <Card style={styles.Card2}>
                                        <Image style={{ color: '#fff', height: 50, width: 50, alignSelf: 'center' }}
                                            source={require('../../assets/icon/icon-07.png')}
                                        />
                                        <Text style={styles.VACANT} >शिकायत की स्थिति देखें </Text>

                                    </Card>
                                </Touchable>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={{ flexdirection: 'column',flex:1 }}>
                            <Touchable  style={{ alignSelf: 'center',width:'100%' }}  onPress={() => Communications.phonecall('1905', true)} >
                               
                                    <Card style={styles.Card3}>


                                         <Image style={{ color: '#fff', height: '30%', width: '25%', alignSelf: 'center' }}
                                        source={require('../../assets/icon/icon-03.png')}
                                    />
                                    <Text style={styles.PUBLISHED} >
                                        हमसे संपर्क करें</Text>

                                    </Card>
                                </Touchable>
                            </View>
                            <View style={{ flexdirection: 'column',flex:1}}>
                            <Touchable style={{ alignSelf: 'center',width:'100%' }} onPress={() => Alert.alert('विकास जारी है !')}>

                               
                                    <Card style={styles.Card4}>
                                    <Image style={{ color: '#fff', height: '30%', width: '25%', alignSelf: 'center' }}
                                        source={require('../../assets/icon/icon-04.png')}
                                    />
                                    <Text style={styles.UNPUBLISHED} >

                                        हमारे बारे में </Text>

                                    </Card>
                                </Touchable>
                            </View>
                        </View>



                        <Touchable onPress={() => this.props.navigation.navigate('FAQs')}>
                      
                            <Card style={styles.Card7}>

                                <Image style={{ color: '#fff', height: 40, width: 40, alignSelf: 'center' }}
                                    source={require('../../assets/icon/icon-05.png')}
                                />
                                <Text style={styles.PUBLISHED} >
                                    सामान्य प्रश्न </Text>


                            </Card>

                        </Touchable>







                    </View>
                </ScrollView>



            </Screen>
        );


    }

}
const styles = {



    borderview: {
        borderBottomColor: '#268DC8',

        borderBottomWidth: 70,
        paddingRight: '1%',
    },
    borderview2: {
        borderBottomColor: '#05ADEE',
        borderBottomWidth: 50,
        paddingRight: '1%',
    },
    borderview3: {
        borderBottomColor: '#fff',
        borderBottomWidth: 65,
        paddingLeft: '1%',
        marginLeft: '10%',
        width: '80%',
        borderRadius: 5
    },



    Card: {
        marginLeft: '6%',
        width: '85%',
        borderRadius: 5,
        marginTop: '5%',
        elevation: 1,
        marginRight: '10%',

    },
    Card1: {

        width:'85%',

        borderRadius: 3,
        height: 100,
        backgroundColor: '#DC4437',
        marginTop: 5,
        marginLeft:'10%',
        marginRight:'5%'

    },


    Card2: {

        width:'85%',
        // width: '100%',
        borderRadius: 3,
        backgroundColor: '#86B83B',
        height: 100,
        marginTop: 5,
        marginLeft: '5%',
        marginRight: '10%'
    },


    Card3: {
      width:'85%',

        borderRadius: 3,
        height: 100,
        backgroundColor: '#8871F5',
        marginTop: 5,
        marginLeft:'10%',
        marginRight:'5%'
    },

    Card4: {
     
        width:'85%',
        // width: '100%',
        borderRadius: 3,
        backgroundColor: '#2CC5EE',
        height: 100,
        marginTop: 5,
        marginLeft: '5%',
        marginRight: '10%'

    },

    // Card5: {
    //     marginLeft: '5%',
    //     borderRadius: 3,
    //     elevation: 1,
    //     width: '43%',
    //     // height: '50%',
    //     backgroundColor: '#8871F5',
    //     height: 100
    // },
    // Card6: {
    //     marginLeft: '5%',
    //     borderRadius: 3,
    //     elevation: 1,
    //     width: '43%',
    //     // height: '50%',
    //     backgroundColor: '#2C9DEE',
    //     height: 100
    // },



    Card0: {
        marginLeft: '5%',
        borderRadius: 3,
        marginTop: 15,

        width: '90%',
        // height: '15%',
        backgroundColor: '#1D75BD',
        height: 80
    },

    Card7: {
        marginLeft: '5%',
        borderRadius: 3,
        marginTop: 10,
        elevation: 1,
        width: '90%',
        // height: '15%',
        backgroundColor: '#FF8500',
        height: 100
    },

    RENTED: {
        fontSize: 13,
        alignSelf: 'center',
        color: '#fff',
        marginTop: '5%'
    },
    RENTED2200: {
        alignSelf: 'center',
        color: '#fff',
        fontSize: 25
    },

    VACANT: {
        fontSize: 13,
        alignSelf: 'center',
        color: '#fff',
        marginTop: '5%'
    },
    VACANT1400: {
        alignSelf: 'center',
        color: '#fff',
        fontSize: 25
    },

    PUBLISHED: {
        fontSize: 13,
        alignSelf: 'center',
        color: '#fff',
        marginTop: '5%'
    },
    PUBLISHED3600: {
        alignSelf: 'center',
        color: '#fff',
        fontSize: 25,
        marginBottom: '15%'
    },

    UNPUBLISHED: {
        fontSize: 10,
        alignSelf: 'center',
        color: '#fff',
        marginTop: '5%'
    },
    UNPUBLISHED1400: {
        alignSelf: 'center',
        color: '#fff',
        fontSize: 25
    },


    LinearGradient: {
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: '10%',
        width: '60%'
    },
    Icon: {
        alignSelf: 'center',
        marginTop: '7%',
        fontSize: 55,
        color: '#fff'
    },


    Text: {
        alignSelf: 'center',
        paddingLeft: '5%',
        fontSize: 13,
        color: '#5D5D5D'
    },
    row:
    {
        flexDirection: 'row',
        marginTop: '3%',
        marginBottom: 10,
        width: '100%',
        paddingLeft:10,
        paddingRight:10,
        alignItems: 'center'
        
        

    }

};
