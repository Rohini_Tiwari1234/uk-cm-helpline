
import { StyleSheet, Modal, AsyncStorage, FlatList, RefreshControl, ScrollView,NetInfo,StatusBar } from 'react-native';
import React, { Component } from 'react';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { NavigationBar,Spinner, Touchable, Image, Title, Button, Divider, Subtitle, Text, Icon, View, TouchableOpacity } from '@shoutem/ui';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Apis from '../../apis/CitizenApi';
import Communications from 'react-native-communications';


const getItemLayout = (data, index) => (
    { length: data.length, offset: data.length * index, index }
);


export default class CitizenComplainList extends Component {
    

    constructor(props) {
        super(props);
        this.renderRow = this.renderRow.bind(this);
        this.state = {
            modalVisible: false,
            CompList: [],
            allDeatil: [],
            singleDetail: [],
            loading: false,
            remark: '',
            status: false,
            mobileno: '',
            netstatus:'',
            CallerId:'',
            Status:'',
            userdetail:[],
            Message:''
        };
    }

    showToast = (message) => {
        Toast.show({
            text: message,
            position: 'bottom',
        })
    }
    componentDidMount() {
      this.setSpinner(true)
        AsyncStorage.getItem("mobile_no").then((value) => {

            this.setState({
                mobileno: value
            });

            let CallerId = this.props.navigation.getParam('CallerId');
           
            let Open =  this.props.navigation.getParam('Open');
           
            Apis.getUserDetailsByPhoneNo(value).then((response) => {
           
                console.log('res');
               console.log(response);
                if(response.status=="success"){
                    if(response.messages!="Record not found"){
                    
                        this.setState({
                            userdetail: response.messages[0]
                        });
        
                        CallerId=response.messages[0].CallerId;
        
                        Apis.problemStatus(CallerId, Open).then((res) => {
                            this.setSpinner(false);
                            console.log('res');
                            console.log(res);
                          
                            if(res.status=="success"){
                            this.setState({
                                CompList: res.messages,CallerId:CallerId,Status:Open
                            });
                        }
                 }, (error) => {
                        //Actions.dashboard();
                        this.setState({
                            CompList: null
                        });
                        console.log("error");
                        console.log(error);
                        this.setSpinner(false);
                        //alert('Please check internet connection.');
                    });
                }
                
               
                else{
                    this.setSpinner(false);
                    this.setState({
                        Message:"शिकायत मौजूद नहीं है !",CompList:null
                    });
                }
            }
                   
              
                 },(error) => {
                    this.setSpinner(false);
                
            });

        });

       
      }
    

  

    // openModal(value) {
    //     console.log('value');
    //     console.log(value);
    //     this.setState({
    //         modalVisible: true,
    //         singleDetail: value.Complaint_detail,
    //         allDeatil: value.Complaint_detail.Summary

    //     });
    //     console.log('singleDetail');
    //     console.log(this.state.singleDetail);
    //     console.log('allDeatil');
    //     console.log(this.state.allDeatil);
    // }

    // closeModal() {
    //     this.setState({ modalVisible: false });
    // }


    // saveRemark = () => {
    //     this.setSpinner(true);
    //     if (this.state.remark != null && this.state.remark != "") {
    //         Apis.CitizenRemark(this.state.singleDetail.compid, this.state.remark).then((res) => {
    //             this.setSpinner(false);
    //             console.log('res');
    //             console.log(res);
    //             if (res.status == 'success') {
    //                 this.setState({ remark: "" });
    //                 this.setState({status:false});
    //                 alert('आपके द्वारा दिया गया विवरण दर्ज कर दिया गया है।');
    //                 this.showToast('आपके द्वारा दिया गया विवरण दर्ज कर दिया गया है।');
    //                 this.closeModal();
    //                // Actions.dashboard();
    //             }
    //             else {
    //                 this.setSpinner(false);
    //                 this.setState({ remark: "" });
    //                 this.setState({status:false});
    //                 alert('आपके द्वारा दिया गया विवरण दर्ज किया जा चूका है| कृपया ऑफिसर की कार्यवाही की प्रतीक्षा करें|');
    //                 this.showToast('आपके द्वारा दिया गया विवरण दर्ज किया जा चूका है| कृपया ऑफिसर की कार्यवाही की प्रतीक्षा करें|');
    //                 this.closeModal();
    //                 //Actions.dashboard();
    //                 // this.showToast('"कृपया पुन: प्रयास करें।');
    //             }
    //         }, (error) => {
    //            this.closeModal();
    //             this.setSpinner(false);
    //             alert('There is no Complaints');
    //         });
    //     }
    //     else {
    //         alert('कृपया विवरण दर्ज करें!');
    //         this.showToast('कृपया विवरण दर्ज करें!');
    //     }
    // }


    // ForSatisfyPC = () => {
    //     this.setSpinner(true)
    //     var mobileno = this.state.mobileno;
    //     var compid = this.state.singleDetail.compid;
    //     var buttontype = 1;

    //     console.log('compid,mobileno,buttontype');

    //     console.log(compid, mobileno, buttontype);


    //     if (mobileno != '' && compid != '') {

    //         Alert.alert(
    //             'Alert',
    //             'आपकी शिकायत का निराकरण संबधित अधिकारी द्वारा कर  दिया गया है, क्या आप अपनी सहमति दर्ज कराना चाहते  है?',
    //             [
    //                 { text: 'Cancel', onPress: () => {this.setSpinner(false); } },
    //                 {
    //                     text: 'OK', onPress: () =>{ Apis.ForSatisfyUnsatisfyPC(compid, mobileno, buttontype).then((res) => {
                          
    //                         console.log('ForSatisfyPC res 1');
    //                         console.log(res);
                          
    //                         this.setSpinner(false);
    //                         if (res.status == 'success') {
    //                             console.log('ForSatisfyPC res 2');
    //                             Alert.alert(
    //                                 'Alert',
    //                                 'संबंधित शिकायत आपकी पुष्टि पर बंद कर दिया गया है।',
    //                                 [
    //                                   // { text: 'Cancel', onPress: () => this.setSpinner(false) },
    //                                   { text: 'OK', onPress: () => { this.showToast('संबंधित शिकायत आपकी पुष्टि पर बंद कर दिया गया है।');this.closeModal(); Actions.dashboard() } },
    //                                   //{text: 'OK', onPress: () => console.log('OK Pressed')},
    //                                 ],
    //                                 { cancelable: false }
    //                               )
    //                             this.setSpinner(false);
    //                             console.log('ForSatisfyPC res 3');
    //                         }
    //                         else {
    //                             this.setSpinner(false);
    //                             console.log('ForSatisfyPC res 4');
    //                             this.showToast('संबंधित शिकायत आपकी जानकारी में मौजूद नहीं है कृपया पुनः पुष्टि करें।');
    //                         }

    //                     }, (error) => {
    //                         this.setSpinner(false);
    //                         console.log('ForSatisfyPC res 5');
    //                         this.showToast('माफ़ कीजिये: संबंधित शिकायत बंद करने में असमर्थ!');
    //                     })}
    //                 },
    //                 //{text: 'OK', onPress: () => console.log('OK Pressed')},
    //             ],
    //             { cancelable: false }
    //         )


    //     }

    //     else {
    //         this.setSpinner(false);
    //         alert('कृपया (*) सभी जानकारी भरें।');
    //     }


    // }


    // ForUnsatisfyPC = () => {
    //     this.setSpinner(true);
    //     var mobileno = this.state.mobileno;
    //     var compid = this.state.singleDetail.compid;
    //     var buttontype = 2;

    //     console.log('compid,mobileno,buttontype');

    //     console.log(compid, mobileno, buttontype);

    //     if (mobileno != '' && compid != '') {

    //         Alert.alert(
    //             'Alert',
    //             'आपकी शिकायत का निराकरण संबधित अधिकारी द्वारा कर  दिया गया है, क्या आप अपनी असहमति दर्ज कराना चाहते है?',
    //             [
    //                 { text: 'Cancel', onPress: () => {this.setSpinner(false); } },
    //                 {
    //                     text: 'OK', onPress: () => {Apis.ForSatisfyUnsatisfyPC(compid, mobileno, buttontype).then((res) => {
                           
    //                         console.log('ForUnsatisfyPC res 1');
    //                         console.log(res);
    //                         this.setSpinner(false);
    //                         if (res.status == 'success') {
    //                             console.log('ForUnsatisfyPC res 2');
    //                             Alert.alert(
    //                                 'Alert',
    //                                 'संबंधित शिकायत पुनः पंजीकृत कर दी गयी है।',
    //                                 [
    //                                   // { text: 'Cancel', onPress: () => this.setSpinner(false) },
    //                                   { text: 'OK', onPress: () => { this.showToast('संबंधित शिकायत पुनः पंजीकृत कर दी गयी है।'); this.closeModal(); Actions.dashboard() } },
    //                                   //{text: 'OK', onPress: () => console.log('OK Pressed')},
    //                                 ],
    //                                 { cancelable: false }
    //                               )
              
    //                             this.setSpinner(false);
    //                             console.log('ForUnsatisfyPC res 3');
    //                         }
    //                         else {
    //                             this.setSpinner(false);
    //                             this.showToast('संबंधित शिकायत आपकी जानकारी में मौजूद नहीं है कृपया पुनः पुष्टि करें।');
    //                         }

    //                     }, (error) => {
    //                         this.setSpinner(false);
    //                         this.showToast('संबंधित शिकायत आपकी जानकारी में मौजूद नहीं है कृपया पुनः पुष्टि करें!');
    //                     })
    //                 }},
    //                 //{text: 'OK', onPress: () => console.log('OK Pressed')},
    //             ],
    //             { cancelable: false }
    //         )


    //     }

    //     else {
    //         this.setSpinner(false);
    //         alert('कृपया (*) सभी जानकारी भरें।');
    //     }


    // }

    setSpinner = (st) => {
        this.setState({ loading: st });
    }

  

    refreshControl() {
        Apis.problemStatus(this.state.CallerId, this.state.Status).then((res) => {
            this.setSpinner(false);
            console.log('res');
            console.log(res);
            
            console.log('res.messages');
            console.log(res.messages);
            this.setState({
                CompList: res.messages
            });
          
        }, (error) => {
            //Actions.dashboard();
            this.setState({
                CompList: null
            });
            console.log("error");
            console.log(error);
            this.setSpinner(false);
            //alert('Please check internet connection.');
        });
    }

    _keyExtractor = (item, index) => index;

    renderRow(complainList) {
       
        var complist = complainList.item.Complaint_detail;
        console.log("comp list data", complist);

        return (

            <View style={styles.Card}>
                <View styleName="horizontal space-between" style={{ backgroundColor: '#E6E6E6' }}>
                    <Text style={styles.Subtitle2}>क्रमांक :<Text >{complist.compid}</Text></Text>
                    <Subtitle >दिनांक :<Text>{complist.compdate}</Text></Subtitle>

                </View>

                <View styleName="horizontal h-start" >


                    <Title style={styles.Subtitle}>नाम :</Title>
                    <Title style={styles.Subtitle}>{complist.callername}</Title>
                </View>

                <View styleName="horizontal h-start" >
                    {/* <Title style={styles.Subtitle}>{restaurants.address}</Title> */}

                    <Title style={styles.Subtitle}>फ़ोन नम्बर :</Title>
                    <View styleName="horizontal space-between" >
                        <Title style={styles.Subtitle}>{complist.phoneno}</Title>
                        <Touchable onPress={() => Communications.phonecall(complist.phoneno, true)} >
                        <Icon name="call" style={{ alignSelf: 'center', height: 30, width: 30, borderRadius: 100, marginLeft: 80, backgroundColor: 'green', color: '#fff' }} />
                        </Touchable>
                    </View>
                </View>
                <View styleName="horizontal h-start" >
                    <Title style={styles.Subtitle}>शिकायत का विवरण :</Title>

                </View>
                <Title style={{ color: '#B4B4B4', fontSize: 17, left: 5 }}>{complist.compremarks}</Title>


                <Button style={{ left: 15, backgroundColor: '#05B2F4', borderRadius: 5, borderWidth: 2, fontFamily: 'CeraPRO-Bold', borderColor: '#fff', marginTop: 5, width: '95%', height: 40 }} onPress={() => this.props.navigation.navigate('CompDetail', { CompDetail: complist,CompId: complist.tid,Officerid:this.state.Officerid })}>
                    <View styleName="horizontal space-between">
                        <Text style={{ color: '#fff', marginLeft: '60%' }}>विवरण देखें </Text>
                        <FontAwesome name="arrow-right" style={{ fontSize: 20, color: '#fff' }} />

                    </View>
                </Button>



                <Divider styleName="line" style={{ marginTop: 3, borderBottomWidth: 2, borderBottomColor: '#E6E6E6', width: '95%', marginLeft: '3%' }} />



            </View>





        );
    }

    loadPosts = () => {
        this.setState({ loading: false })
    }

   

    render() {
        const complainList = this.state.CompList;
        console.log("complaint:", complainList)
        return (
            <View style={{ flex: 1 }}>
<StatusBar barStyle='light-content'
                    backgroundColor='#117bc7' />
<NavigationBar style={{ container: { zIndex: 1, backgroundColor: '#117bc7', borderBottomWidth: 0, borderColor: 'none' } }}

leftComponent=
{<Touchable onPress={() => this.props.navigation.goBack()} >
    < Icon style={{ color: '#fff', fontSize: 30,marginLeft:10 }} name="left-arrow" />
</Touchable>
}
centerComponent={
    <Title style={styles.navigationBarTitleColor} >शिकायतें  </Title>
}

rightComponent={(
    <Touchable onPress={() => this.props.navigation.goBack()} >
        < MaterialCommunityIcons style={{color: '#fff', fontSize: 30,marginRight:10}} name="home" />
    </Touchable>
)}
/>

                <ScrollView style={{ marginTop: '15%' }}
                    refreshControl={
                        <RefreshControl
                            onRefresh={() => this.refreshControl()}
                            refreshing={this.state.refreshing}
                            enabled={true}

                        />
                    }
                >

                    <View  >

                        {complainList!=null && complainList!='' ?
                            <FlatList

                                data={complainList}
                                renderItem={this.renderRow}
                                extraData={this.state.masterCompList}
                                keyExtractor={this._keyExtractor}
                                maxToRenderPerBatch={5}
                                getItemLayout={getItemLayout}
                                onEndReached={this.loadPosts.bind(this)}
                                // onEndReachedThreshold={1}
                                ListFooterComponent={this.state.loading ? <Spinner /> : null}
                                progressViewOffset={15}

                            />
                            :
                            // <View style={{ flex: 1, alignSelf: 'center', justifyContent: 'center', paddingTop: '40%' }}   >
                                <Text style={{ flex: 1, alignSelf: 'center', justifyContent: 'center', paddingTop: '40%',fontSize:15 }}>{this.state.Message}</Text>
                            // </View>
                        }

                    </View>
                </ScrollView>
{this.state.loading ?
    <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)', alignItems: 'center', justifyContent: 'center', position: 'absolute', width: '100%', height: '100%' }}>
        <Spinner color="white" />
    </View> : null}
            </View>
        );
    }
}



const styles = {

    Card: {
        width: '95%',
        margin: 5,
        paddingLeft: 10,
        paddingRight: 10
    },


    navigationBarTitleColor: {
        fontSize: 20,
        color: '#fff'
    },
    View: {
        width: '95%',

    },
    Subtitle2: {
        fontSize: 18,
        // fontFamily: 'CeraPro-Bold',
        //  color: '#B3B3B3',
        fontWeight: '500',
        //    paddingLeft:3,

        marginLeft: 5


    },

    Subtitle: {
        paddingLeft: 10,
        fontSize: 15,
        // color: '#F7941F',
        paddingTop: 5
    },





}