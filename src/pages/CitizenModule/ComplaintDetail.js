/**
 * Deepak S Rathode
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { ImageBackground, Touchable, Spinner, Lightbox, NavigationBar, Text, View, Screen, Tile, Title, TextInput, Row, Col, Card, Button, Icon, Divider, Caption, Subtitle, Image, ListView, TouchableOpacity, } from '@shoutem/ui';
import { ScrollView, Modal, TouchableHighlight, ActivityIndicator, StatusBar,AsyncStorage,Alert } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Apis from '../../apis/CitizenApi';
import Communications from 'react-native-communications';
import {Toast} from 'native-base';

export default class CitizenComplaintDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            CompDetail: [],
            CompDetailComment: [],
            CompDetailCommentCount: 0,
            userDetail:[],
            Remark:'',
            loading:false,
            compRemark:[],
            Officerid:'',
            CompId:0,
            Default_Rating: 0,
            Max_Rating : 5

        };
        this.Star = 'https://reactnativecode.com/wp-content/uploads/2018/01/full_star.png';

        this.Star_With_Border = 'https://reactnativecode.com/wp-content/uploads/2018/01/border_star.png';

       
    }
    setSpinner = (st) => {
        this.setState({ loading: st });
    }

    showToast = (message) => {
        Toast.show({
            text: message,
            position: 'bottom',
        })
    }

    componentDidMount() {

        this.setSpinner(true);
        var compdetail = this.props.navigation.getParam('CompDetail');
        var CompId = this.props.navigation.getParam('CompId');
        console.log('CompDetail');
        console.log(compdetail,CompId);
        this.getCompDetail(compdetail);

    }

    getCompDetail=(compdetail)=>{
        Apis.GetCompDetailsByCompid(compdetail.compid).then((res) => {
            this.setSpinner(false);
          console.log('res comp list by attri and dist');
          console.log(res);


          if (res.status == 'success') {
              this.setState({
                  CompDetail: res.messages.ticket_detail,
                  CompDetailComment: res.messages.comments.comments
              })

          }


      }, (error) => {

          console.log("error");
          console.log(error);
          this.setSpinner(false);
          
      });

    
    }


    render() {

      

        return (



            <Screen style={{ flex: 1, backgroundColor: '#fff' }}>



<NavigationBar style={{ container: { Index: 1, backgroundColor: '#117bc7', color: '#fff', borderBottomWidth: 0, borderColor: 'none' } }}
                    statusBarColor="#117bc7"
                    leftComponent={
                        <Touchable onPress={() => this.props.navigation.goBack()} >
                        <Icon style={{ color: '#fff', fontSize: 35 }} name="left-arrow" />
                        </Touchable>}

                    centerComponent={<Text style={{ fontSize: 18, color: '#fff', width: 230, alignSelf: 'center', fontWeight: '500' }}>


                        शिकायत
पर की गयी कार्रवाई

</Text>}
                    rightComponent={<Touchable onPress={() => this.props.navigation.navigate('CitizenDashboard')} >
                        < MaterialCommunityIcons style={{ color: '#fff', fontSize: 30 }} name="home" />
                    </Touchable>}
                />



                <ScrollView style={{ flex: 1, marginTop: 70 }}>
                    <View style={{ height: '100%' }}>
                       


                        <Row style={{ height: 45, backgroundColor: '#F0F0F0' }}>

                            <Text style={{ fontWeight: 'bold', marginLeft: 5, }}>क्रमांक: <Text>{this.state.CompDetail.tid}   </Text>    </Text>


                            <Text style={{ marginLeft: 40, fontWeight: 'bold' }}>दिनांक: <Text>{this.state.CompDetail.compdate}   </Text>    </Text>


                        </Row>
                       

                        <Row style={{ marginLeft: 5, height: 45 }}>

                            <Text style={{ fontWeight: 'bold' }}>शिकायत की स्थिति:  </Text>

                            <Text> <Text style={{ color: '#3AB54A', fontSize: 18, fontWeight: 'bold', paddingRight: 35 }}> o</Text>{this.state.CompDetail.compstatus}  </Text>


                        </Row>


                        <Row style={{ marginLeft: 5, height: 45 }}>

                            <Text style={{ fontWeight: 'bold' }}>विभाग का नाम:  </Text>

                            <Text>{this.state.CompDetail.Department} </Text>


                        </Row>



                        <Row style={{ marginLeft: 5, height: 45 }}>

                            <Text style={{ fontWeight: 'bold' }}>शिकायत कर्ता का नाम:  </Text>

                            <Text>{this.state.CompDetail.callername} </Text>


                        </Row>


                        <Row style={{ marginLeft: 7, height: 45 }}>

                            <Text style={{ fontWeight: 'bold' }}>फोन न.:  </Text>
                            <Row style={{ paddingLeft: 10}}>
                            <Text>{this.state.CompDetail.phone} </Text>
                            <Touchable onPress={() => Communications.phonecall(this.state.CompDetail.phone, true)} >
                            <Icon name="call" style={{ alignSelf: 'center', height: 30, width: 30, borderRadius: 100, backgroundColor: 'green', color: '#fff' }} />
                            </Touchable>
                            </Row>
                        </Row>

                        <Row style={{ marginLeft: 7 }}>

                            <Text style={{ fontWeight: 'bold' }}>पता:  </Text>

                            <Text>{this.state.CompDetail.calleraddress}</Text>


                        </Row>



                        <Row style={{ marginLeft: 7, height: 45 }}>

                            <Text style={{ fontWeight: 'bold' }}>जिला:  </Text>

                            <Text>{this.state.CompDetail.District}</Text>


                        </Row>






                        <Row style={{ backgroundColor: '#F0F0F0', height: 45 }}>

                            <Text style={{ marginLeft: 10, fontWeight: 'bold', marginTop: 10 }}>शिकायत का प्रारूप:  </Text>
                        </Row>
                        <Text style={{ marginTop: 15, marginLeft: 20 }}>{this.state.CompDetail.Complainttype} </Text>







                        <Row style={{ backgroundColor: '#F0F0F0', height: 45, marginTop: 15 }}>
                            <Text style={{ marginLeft: 15, marginTop: 10, fontWeight: 'bold', backgroundColor: '#F0F0F0' }}>शिकायत का विवरण:  </Text>
                        </Row>


                        <Text style={{ marginTop: 20, marginLeft: 15 }}>{this.state.CompDetail.complaint}</Text>





                        <Row style={{ backgroundColor: '#F0F0F0', height: 45, marginTop: 15 }}>



                            <Text style={{ marginLeft: 10, marginTop: 10, fontWeight: 'bold' }}>शिकायत पर की गई गतिविधि:  </Text>
                        </Row>

                        {this.state.CompDetailComment.map((item, index) => {
                            return (
                                <Row key={index}>
                                    <View styleName="vertical space-between" >
                                        <Text style={{ marginTop:5, marginLeft: 15 }}>{item.notes}</Text>




                                        <Text style={{ textAlign: 'right', alignSelf: 'stretch', fontSize: 12, marginTop: 2 }}>दिनांक:  {item.logdate}</Text>
                                        <Divider styleName="line" style={{ borderBottomWidth: 2, borderBottomColor: '#E6E6E6', width: '94%', marginLeft: '3%', marginRight: '3%' }} />
                                    </View>

                                </Row>
                            )
                        })}




                    </View>
                </ScrollView>

{this.state.loading ?
    <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)', alignItems: 'center', justifyContent: 'center', position: 'absolute', width: '100%', height: '100%' }}>
        <Spinner color="white" />
    </View> : null}

            </Screen>
        );


    }

}
const styles = {



    borderview: {
        borderBottomColor: '#268DC8',

        borderBottomWidth: 70,
        paddingRight: '1%',
    },
    borderview2: {
        borderBottomColor: '#05ADEE',
        borderBottomWidth: 50,
        paddingRight: '1%',
    },
    borderview3: {
        borderBottomColor: '#fff',
        borderBottomWidth: 65,
        paddingLeft: '1%',
        marginLeft: '10%',
        width: '80%',
        borderRadius: 5
    },



    Card: {
        marginLeft: '6%',
        width: '85%',
        borderRadius: 10,
        marginTop: '5%',
        elevation: 1,
        marginRight: '10%',

    },
    Card1: {
        marginLeft: '5%',
        borderRadius: 10,
        elevation: 1,
        width: '43%',
        // height: '60%',
        height: 100,
        backgroundColor: '#DC4437',
        marginTop: 5

    },


    Card2: {
        marginLeft: '5%',
        borderRadius: 10,
        elevation: 1,
        width: '43%',
        // height: '60%',
        backgroundColor: '#86B83B',
        height: 100,
        marginTop: 5
    },


    Card3: {
        marginLeft: '5%',
        borderRadius: 10,
        elevation: 1,
        width: '43%',
        // height: '50%',
        backgroundColor: '#8871F5',
        height: 100
    },

    Card4: {
        marginLeft: '5%',
        borderRadius: 10,
        elevation: 1,
        width: '43%',
        // height: '50%',
        backgroundColor: '#2CC5EE',
        height: 100
    },

    Card5: {
        marginLeft: '5%',
        borderRadius: 10,
        elevation: 1,
        width: '43%',
        // height: '50%',
        backgroundColor: '#8871F5',
        height: 100
    },
    Card6: {
        marginLeft: '5%',
        borderRadius: 10,
        elevation: 1,
        width: '43%',
        // height: '50%',
        backgroundColor: '#2C9DEE',
        height: 100
    },



    Card0: {
        marginLeft: '5%',
        borderRadius: 10,
        marginTop: 15,
        elevation: 1,
        width: '90%',
        // height: '15%',
        backgroundColor: '#1D75BD',
        height: 100
    },

    Card7: {
        marginLeft: '5%',
        borderRadius: 10,
        marginTop: 10,
        elevation: 1,
        width: '90%',
        // height: '15%',
        backgroundColor: '#FF8500',
        height: 100
    },

    RENTED: {
        fontSize: 10,
        paddingLeft: '35%',
        color: '#fff',
        fontFamily: 'CeraPRO-Medium',
        marginTop: '5%'
    },
    RENTED2200: {
        paddingLeft: '40%',
        color: '#fff',
        fontFamily: 'CeraPRO-Medium',
        fontSize: 25
    },

    VACANT: {
        fontSize: 10,
        paddingLeft: '30%',
        color: '#fff',
        fontFamily: 'CeraPRO-Medium',
        marginTop: '5%'
    },
    VACANT1400: {
        paddingLeft: '40%',
        color: '#fff',
        fontFamily: 'CeraPRO-Medium',
        fontSize: 25
    },

    PUBLISHED: {
        fontSize: 10,
        paddingLeft: '40%',
        color: '#fff',
        fontFamily: 'CeraPRO-Medium',
        marginTop: '5%'
    },
    PUBLISHED3600: {
        paddingLeft: '40%',
        color: '#fff',
        fontFamily: 'CeraPRO-Medium',
        fontSize: 25,
        marginBottom: '15%'
    },

    UNPUBLISHED: {
        fontSize: 10,
        paddingLeft: '30%',
        color: '#fff',
        fontFamily: 'CeraPRO-Medium',
        marginTop: '5%'
    },
    UNPUBLISHED1400: {
        paddingLeft: '40%',
        color: '#fff',
        fontFamily: 'CeraPRO-Medium',
        fontSize: 25
    },






    LinearGradient: {
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: '10%',
        width: '60%'
    },
    Icon: {
        alignSelf: 'center',
        marginTop: '7%',
        fontSize: 55,
        color: '#fff'
    },


    Text: {
        alignSelf: 'center',
        paddingLeft: '5%',
        fontSize: 13,
        color: '#5D5D5D'
    },
    row: { flexDirection: 'row', marginTop: '3%', marginBottom: 10 }

};
