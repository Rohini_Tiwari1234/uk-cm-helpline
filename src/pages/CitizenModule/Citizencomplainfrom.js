import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Dimensions,
  BackHandler,
  AsyncStorage,
  Keyboard,
  Image,
  TouchableOpacity,
  Modal,
  ScrollView, Alert,
  ImageBackground
} from 'react-native';
import { Root, ActionSheet, Toast, Radio, Grid, Separator, StatusBar, TabHeading, Footer, Fab, Picker, View, Row, Col, Container, List, Switch, Header, Item, Spinner, Label, Content, Form, Input, Icon, Left, Button, Title, Body, Right, SocialIcon, Badge, Card, CardItem, Text, Thumbnail, ListItem, CheckBox, Tabs, Tab } from 'native-base';


import Apis from '../../apis/CitizenApi';


export default class ComplaintForm extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {

      Mobile: '',
      First_Name: '',
      Last_Name: '',
      AadharNo: '',
      Another_Mobile_No: '',
      Email: '',
      DistrictId1: 0,
      DistrictId2: 0,
      BlockId1: 0,
      BlockId2: 0,
      GPId1: 0,
      GPId2: 0,
      Address: '',
      Radio: "R",
      DepartmentId: 0,
      CompType: '',
      TehsilId: 0,
      ThanaId: 0,
      AnubhagId: 0,
      AttributeId: 0,
      WardId: 0,
      VitranKendraId: 0,
      CircleId: 0,
      Description: '',
      Compdetail: [],
      District: [],
      District2: [],
      Block: [],
      UserBlock: [],
      Block2: [],
      GramPanchayat: [],
      UserGramPanchayat: [],
      GramPanchayat2: [],
      Department: [],
      Attribute: [],
      Thana: [],
      VitranKendra: [],
      Anubhag: [],
      Ward: [],
      Circle: [],
      Tehsil: [],
      thanaActive: false,
      blockActive: false,
      wardActive: false,
      anubhagActive: false,
      vkActive: false,
      circleActive: false,
      tehsilActive: false,
      userAutoFillData: [],
      userDetail: [],
      loading: false,
      mobile_no: '',

    };
  }

  setSpinner = (st) => {
    this.setState({ loading: st });
  }

  handleUnhandledTouches() {
    Keyboard.dismiss();
    return false;
  }

  componentDidMount() {

    Apis.fetchDistrict().then((res) => {

      this.setState({
        District: res.messages,

      });

    }, (error) => { console.log(error); });

    AsyncStorage.getItem("mobile_no").then((value) => {

      this.setState({
        mobile_no: value
      });

   

    Apis.getUserDetailsByPhoneNo(value).then((response) => {
           
      console.log('res');
     console.log(response.messages);
        
       this.setSpinner(false);
       console.log('this.state.userDetail');
       this.setState({
        userDetail: response.messages[0],userAutoFillData: response.messages[0]
      });
      
        Apis.fetchDistrictWiseBlock(response.messages[0].callerdistrict).then((res) => {

          this.setState({
            UserBlock: res.messages
          });

        }, (error) => { console.log(error); })

        Apis.fetchDistwiseGramPanchayat(response.messages[0].callerdistrict).then((res) => {

          this.setState({
            UserGramPanchayat: res.messages
          });

        }, (error) => { console.log(error); })
     
      this.setSpinner(false);
    }, (error) => { console.log(error); })

  })
    this.handleUnhandledTouches();
    this.getDistrictName2();
    this.getDepartmentName();

   
  }

 

  setRadio = (radio) => {
    console.log('radio');
    console.log(radio);
    this.setState({
      Radio: radio
    });
  }

  getUserBlockAndGp(distid) {
    this.setState({ DistrictId1: distid });

    this.getBlockName(distid);
    //this.getGramPanchayatName(distid);
  }


  getDistrictName2 = () => {
    console.log("getdistrict 2");
    this.setSpinner(true);
    Apis.fetchDistrict().then((res) => {

      this.setState({
        District2: res.messages,

      });
      this.setSpinner(false);
      this.renderDistrictPicker2();
    }, (error) => { console.log(error); });

  }

  resetDistrict = () => {
    if (this.state.DistrictId2 != null) {

      this.setState({
        DistrictId2: null,
        thanaActive: false,
        blockActive: false,
        wardActive: false,
        anubhagActive: false,
        vkActive: false,
        circleActive: false,
        tehsilActive: false,
      });
    }
    else {
      this.getDistrictName2();
    }
  }

  getBlockName(distid) {

    Apis.fetchDistrictWiseBlock(distid).then((res) => {
      console.log("getBlockName 2");
      this.setState({
        Block: res.messages
      });

    }, (error) => { console.log(error); })

  }



  getBlockName2(distid,DepartmentId) {
    this.setSpinner(true);
    console.log('block district id',distid)
    // Apis.fetchDistrictWiseBlock(distid).then((res) => {
    Apis.GetBlocksByDistByDep(distid,DepartmentId).then((res) => {
      this.setState({
        Block2: res.messages
      })
      this.setSpinner(false);
    }, (error) => { console.log(error); });
  }

  getGramPanchayatName(distid) {

    Apis.fetchDistwiseGramPanchayat(distid).then((res) => {

      this.setState({
        GramPanchayat: res.messages
      });

    }, (error) => { console.log(error); });


  }
  getGramPanchayatName2(distid) {
    this.setSpinner(true);
    Apis.fetchDistwiseGramPanchayat(distid).then((res) => {

      this.setState({
        GramPanchayat2: res.messages
      })
      this.setSpinner(false);
    }, (error) => { console.log(error); });

  }

  getDepartmentName() {

    Apis.fetchDepartmentName().then((res) => {

      this.setState({
        Department: res.messages
      });

    }, (error) => { console.log(error); });
  }

  getAttributeName(deptid) {
    this.setState({ DepartmentId: deptid, loading: true });
    this.resetDistrict();

    // this.setSpinner(true);
    let attrid = 0;
    Apis.fetchAttributeName(deptid, attrid).then((res) => {

      this.setState({
        Attribute: res.messages
      })
      this.setSpinner(false);

    }, (error) => { console.log(error); });
  }

  getThanaName(distid) {
    this.setSpinner(true);

    Apis.fetchThanaName(distid).then((res) => {

      this.setState({
        Thana: res.messages
      })
      this.setSpinner(false);
    }, (error) => { console.log(error); });
  }

  getWard(distid) {
    this.setSpinner(true);
    if (distid != null) {
      Apis.fetchWard(distid).then((res) => {

        this.setState({
          Ward: res.messages
        })
        this.setSpinner(false);
      }, (error) => { console.log(error); });
    }
    else {
      Apis.fetchWard(this.state.DistrictId2).then((res) => {

        this.setState({
          Ward: res.messages
        })
        this.setSpinner(false);
      }, (error) => { console.log(error); });
    }

  }


  getVkName(distid) {
    this.setSpinner(true);
    Apis.fetchVitranKendraName(distid).then((res) => {

      this.setState({
        VitranKendra: res.messages
      })
      this.setSpinner(false);
    }, (error) => { console.log(error); });
  }


  getAnubhag(distid, deptid) {
    this.setSpinner(true);
    Apis.fetchAnubhag(distid, deptid).then((res) => {

      this.setState({
        Anubhag: res.messages
      })
      this.setSpinner(false);
    }, (error) => { console.log(error); });
  }

  getTehsil(distid) {
    this.setSpinner(true);
    Apis.fetchTehsilName(distid).then((res) => {

      this.setState({
        Tehsil: res.messages
      })
      this.setSpinner(false);
    }, (error) => { console.log(error); });
  }

  getCircleName(distid, deptid) {
    this.setSpinner(true);
    Apis.fetchCircleName(distid, deptid).then((res) => {

      this.setState({
        Circle: res.messages
      })
      this.setSpinner(false);
    }, (error) => { console.log(error); });

  }


  setBlockState = (value) => {
    this.setSpinner(true);
    console.log('Updating time...value', value);

    this.setState({
      BlockId1: value
    })
    this.setSpinner(false);
    console.log('Updating time...BlockId1', this.state.BlockId1);
  }

  setGPState = (value) => {
    this.setSpinner(true);
    console.log('Updating time...value', value);

    this.setState({
      GPId1: value
    })
    this.setSpinner(false);
    console.log('Updating time...BlockId1', this.state.BlockId1);
  }


  renderDistrictPicker1() {

    let report = this.state.District;
    if (report.length > 0) {

      let projectEventTypes = [
        { label: 'निवासरत जिला *', value: null }
      ];
      report.forEach((eventType) => projectEventTypes.push({ label: eventType.Name, value: eventType.Id }));
      return (
        <Picker style={{ borderBottomWidth: 0.3 }}
          selectedValue={this.state.DistrictId1}
          mode="dropdown"
          onValueChange={(value) => { this.getUserBlockAndGp(value); console.log(value) }}
        >
          {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
        </Picker>
      );
    }
    return null;
  }

  renderDistrictPicker2() {

    let report = this.state.District2;
    if (report.length > 0) {

      let projectEventTypes = [
        { label: 'जिला *', value: null }
      ];
      report.forEach((eventType) => projectEventTypes.push({ label: eventType.Name, value: eventType.Id }));
      return (
        <Picker style={{ borderBottomWidth: 0.3 }}
          selectedValue={this.state.DistrictId2}
          mode="dropdown"
          // onValueChange={(value) => { this.setState({ DistrictId2: value }); this.getGramPanchayatName2(value); this.functionForPickerSelection(value); }}
          onValueChange={(value) => { this.setState({ DistrictId2: value });  this.functionForPickerSelection(value); }}
       >
          {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
        </Picker>
      );
    }
    return null;

  }

  renderBlockPicker1() {

    if (this.state.userAutoFillData.callerblockcode != 0) {
      let report = this.state.Block;
      if (report.length > 0) {

        let projectEventTypes = [
          { label: 'निवासरत ब्लॉक *', value: null }
        ];
        report.forEach((eventType) => projectEventTypes.push({ label: eventType.BlockName, value: eventType.blockId }));
        return (
          <Picker
            selectedValue={this.state.BlockId1}
            mode="dropdown"
            onValueChange={(value) => { this.setState({ BlockId1: value }) }}
          >
            {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
          </Picker>
        );
      }
    }
    else {

      let report = this.state.UserBlock;
      if (report.length > 0) {

        let projectEventTypes = [
          { label: 'निवासरत ब्लॉक *', value: null }
        ];
        report.forEach((eventType) => projectEventTypes.push({ label: eventType.BlockName, value: eventType.blockId }));
        return (
          <Picker
            selectedValue={this.state.BlockId1}
            mode="dropdown"
            onValueChange={(value) => { this.setBlockState(value) }}
          >
            {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
          </Picker>
        );
      }
    }

    return null;
  }


  renderBlockPicker2() {

    let report = this.state.Block2;
    if (report != null) {

      let projectEventTypes = [
        { label: 'ब्लॉक *', value: null }

      ];
      report.forEach((eventType) => projectEventTypes.push({ label: eventType.Block_Name_H, value: eventType.Block_Code }));
      return (
        <Picker
          enabled={this.state.blockActive}
          selectedValue={this.state.BlockId2}
          mode="dropdown"
          onValueChange={(value) => { this.setState({ BlockId2: value }); this.getWard(this.state.DistrictId2) }}
        >
          {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
        </Picker>
      );
    }
    return null;

  }

  renderGramPanchayatPicker1() {

    if (this.state.userAutoFillData.callerGPcode != 0) {
      let report = this.state.GramPanchayat;
      if (report.length > 0) {

        let projectEventTypes = [
          { label: 'निवासरत ग्राम पंचायत *', value: null }
        ];
        report.forEach((eventType) => projectEventTypes.push({ label: eventType.GP_Name_H, value: eventType.GP_Code }));
        return (
          <Picker
            selectedValue={this.state.GPId1}
            mode="dropdown"
            onValueChange={(value) => { this.setState({ GPId1: value }) }}
          >
            {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
          </Picker>
        );
      }
    }
    else {

      let report = this.state.UserGramPanchayat;
      if (report.length > 0) {

        let projectEventTypes = [
          { label: 'निवासरत ग्राम पंचायत *', value: null }
        ];
        report.forEach((eventType) => projectEventTypes.push({ label: eventType.GP_Name_H, value: eventType.GP_Code }));
        return (
          <Picker
            selectedValue={this.state.GPId1}
            mode="dropdown"
            onValueChange={(value) => { this.setGPState(value) }}
          >
            {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
          </Picker>
        );
      }
    }
    return null;
  }

  renderGramPanchayatPicker2() {

    let report = this.state.GramPanchayat2;
    if (report.length > 0) {

      let projectEventTypes = [
        { label: 'ग्राम पंचायत *', value: null }
      ];
      report.forEach((eventType) => projectEventTypes.push({ label: eventType.GP_Name_H, value: eventType.GP_Code }));
      return (
        <Picker
          selectedValue={this.state.GPId2}
          mode="dropdown"
          onValueChange={(value) => { this.setState({ GPId2: value }) }}
        >
          {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
        </Picker>
      );
    }
    return null;
  }

  renderDepartmentPicker() {

    let report = this.state.Department;

    if (report.length > 0) {

      let projectEventTypes = [
        { label: 'विभाग *', value: null }
      ];
      report.forEach((eventType) => projectEventTypes.push({ label: eventType.Department_Name, value: eventType.subDepid }));
      return (
        <Picker
          selectedValue={this.state.DepartmentId}
          mode="dropdown"
          onValueChange={(value) => { this.getAttributeName(value); }}

        >
          {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
        </Picker>
      );
    }
    return null;
  }
  renderAttributePicker() {

    let report = this.state.Attribute;
    if (report.length > 0) {

      let projectEventTypes = [
        { label: 'शिकायत की श्रेणी *', value: null }
      ];
      report.forEach((eventType) => projectEventTypes.push({ label: eventType.attribname, value: eventType.attribid }));
      return (
        <Picker
          selectedValue={this.state.AttributeId}
          mode="dropdown"
          onValueChange={(value) => { this.setState({ AttributeId: value }); }}
        >
          {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
        </Picker>
      );
    }
    return null;
  }

  renderThanaPicker() {

    let report = this.state.Thana;
    if (report.length > 0) {
      let projectEventTypes = [
        { label: 'थाना * ', value: null }

      ];
      report.forEach((eventType) => projectEventTypes.push({ label: eventType.Thana, value: eventType.ThanaId }));
      return (
        <Picker

          selectedValue={this.state.ThanaId}
          mode="dropdown"
          onValueChange={(value) => { this.setState({ ThanaId: value }); }}
        >
          {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
        </Picker>
      );
    }
    return null;
  }

  renderWardPicker() {

    let report = this.state.Ward;

    if (report.length > 0) {

      let projectEventTypes = [
        { label: 'वार्ड *', value: null }

      ];
      report.forEach((eventType) => projectEventTypes.push({ label: eventType.wardname, value: eventType.wardid }));
      return (
        <Picker

          selectedValue={this.state.WardId}
          mode="dropdown"
          onValueChange={(value) => { this.setState({ WardId: value }); }}
        >
          {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
        </Picker>
      );
    }
    else {
      <Spinner />
      //return null;
    }

  }

  renderVitranKendraPicker() {

    let report = this.state.VitranKendra;
    if (report.length > 0) {
      let projectEventTypes = [
        { label: 'वितरण केंद्र *', value: null }

      ];
      report.forEach((eventType) => projectEventTypes.push({ label: eventType.Name, value: eventType.VKId }));
      return (
        <Picker

          selectedValue={this.state.VitranKendraId}
          mode="dropdown"
          onValueChange={(value) => { this.setState({ VitranKendraId: value }); }}
        >
          {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
        </Picker>
      );
    } return null;
  }

  renderAnubhagPicker() {

    let report = this.state.Anubhag;
    if (report.length > 0) {
      let projectEventTypes = [
        { label: 'अनुभाग *', value: null }

      ];
      report.forEach((eventType) => projectEventTypes.push({ label: eventType.SubDivName, value: eventType.SubDivId }));
      return (
        <Picker


          selectedValue={this.state.AnubhagId}
          mode="dropdown"
          onValueChange={(value) => { this.setState({ AnubhagId: value }); }}
        >
          {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
        </Picker>
      );
    }
    return null;
  }
  renderTehsilPicker() {

    let report = this.state.Tehsil;
    if (report.length > 0) {
      let projectEventTypes = [
        { label: 'तहसील *', value: null }

      ];
      report.forEach((eventType) => projectEventTypes.push({ label: eventType.Tehsil_Name_H, value: eventType.Tehsil_Code }));
      return (
        <Picker

          selectedValue={this.state.TehsilId}
          mode="dropdown"
          onValueChange={(value) => { this.setState({ TehsilId: value }); }}
        >
          {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
        </Picker>
      );
    }
    return null;
  }
  renderCirclePicker() {

    let report = this.state.Circle;
    if (report.length > 0) {
      let projectEventTypes = [
        { label: 'सर्किल *', value: null }

      ];
      report.forEach((eventType) => projectEventTypes.push({ label: eventType.CircleName, value: eventType.CircleId }));
      return (
        <Picker

          selectedValue={this.state.CircleId}
          mode="dropdown"
          onValueChange={(value) => { this.setState({ CircleId: value }); }}
        >
          {projectEventTypes.map((eventType, index) => <Picker.Item key={index} {...eventType} />)}
        </Picker>
      );
    }

    return null;

  }



  functionForPickerSelection = (distid) => {

    if (this.state.DepartmentId == 95 && distid != null) {
      if (distid != null) {
        this.setState({
          anubhagActive: true,
          vkActive: true,
          thanaActive: false,
          blockActive: false,
          wardActive: false,
          circleActive: false,
          tehsilActive: false
        })
        this.getVkName(distid);
        this.getAnubhag(distid, this.state.DepartmentId);

      }
      else {
        this.showToast('"कृपया ज़िला दर्ज करें।');
      }
    }
    else if (this.state.DepartmentId == 24 && distid != null) {
      if (distid != null) {
        this.setState({
          anubhagActive: true,
          vkActive: false,
          thanaActive: true,
          blockActive: false,
          wardActive: false,
          circleActive: false,
          tehsilActive: false
        })
        this.getThanaName(distid);
        this.getAnubhag(distid, this.state.DepartmentId);
      }
      else {
        this.showToast('"कृपया ज़िला दर्ज करें।');
      }
    }
    // else if (this.state.DepartmentId == 138 && distid != null) {
    //   if (distid != null) {
    //     this.setState({
    //       anubhagActive: false,
    //       vkActive: false,
    //       thanaActive: false,
    //       blockActive: false,
    //       wardActive: false,
    //       circleActive: true,
    //       tehsilActive: false
    //     })
    //     this.getCircleName(distid, this.state.DepartmentId);

    //   }
    //   else {
    //     this.showToast('"कृपया ज़िला दर्ज करें।');
    //   }
    // }
    else if (this.state.DepartmentId == 2 || this.state.DepartmentId == 32 || this.state.DepartmentId == 62  && distid != null) {
      if (distid != null) {
        this.setState({
          anubhagActive: false,
          vkActive: false,
          thanaActive: false,
          blockActive: false,
          wardActive: false,
          circleActive: false,
          tehsilActive: true
        })
        this.getTehsil(distid);
      }
      else {
        this.showToast('"कृपया ज़िला दर्ज करें।');
      }

    }
    // else if (this.state.DepartmentId == 1042 || this.state.DepartmentId == 1041 || this.state.DepartmentId == 1030 || this.state.DepartmentId == 182 || this.state.DepartmentId == 1016 || this.state.DepartmentId == 1043
    //   || this.state.DepartmentId == 181 || this.state.DepartmentId == 184 || this.state.DepartmentId == 180 || this.state.DepartmentId == 1040 || this.state.DepartmentId == 1014 || this.state.DepartmentId == 1075
    //   || this.state.DepartmentId == 1031 || this.state.DepartmentId == 185 || this.state.DepartmentId == 187 || this.state.DepartmentId == 183 || this.state.DepartmentId == 179 || this.state.DepartmentId == 168
    //   || this.state.DepartmentId == 178 || this.state.DepartmentId == 1015 || this.state.DepartmentId == 176 || this.state.DepartmentId == 186 || this.state.DepartmentId == 177 || this.state.DepartmentId == 169
    //   || this.state.DepartmentId == 173 || this.state.DepartmentId == 171 || this.state.DepartmentId == 174 || this.state.DepartmentId == 172 || this.state.DepartmentId == 170 || this.state.DepartmentId == 175
    //   && distid != null) {
    //   if (distid != null) {
    //     this.setState({
    //       anubhagActive: false,
    //       vkActive: false,
    //       thanaActive: false,
    //       blockActive: true,
    //       wardActive: true,
    //       circleActive: false,
    //       tehsilActive: false
    //     })
    //     this.getBlockName2(distid);
    //     this.getWard(distid);
    //   }
    //   else {

    //     this.showToast('"कृपया ज़िला दर्ज करें।');
    //   }

    // }
    else {
      if (distid != null) {
        this.setState({
          anubhagActive: false,
          vkActive: false,
          thanaActive: false,
          blockActive: true,
          wardActive: false,
          circleActive: false,
          tehsilActive: false
        })
        this.getBlockName2(distid,this.state.DepartmentId);

      }
      else {

        this.showToast('"कृपया ज़िला दर्ज करें।');
      }
    }
  }

  showToast = (message) => {
    Toast.show({
      text: message,
      position: 'bottom',
    })
  }

  // getUserDetailsByPhoneNo =(phoneno)=>{
  //   console.log(phoneno);
  //   Apis.getUserDetailsByPhoneNo(phoneno).then((res) => {
  //       console.log(res);
  //       console.log('res.messages[0]');
  //       console.log(res.messages);
  //       if(res.messages=='Record not found'){
  //         this.setState({
  //           userAutoFillData:null
  //         })
  //       }
  //       else{
  //         this.setState({
  //           userAutoFillData:res.messages[0]
  //         })
  //       }
  //      console.log('this.state.userAutoFillData');
  //      console.log(this.state.userAutoFillData);
  //   });
  // }




  RegisterNewComplaint = () => {


    this.setSpinner(true);
    if (this.state.userAutoFillData.phoneno == null || this.state.userAutoFillData.phoneno == '' || this.state.userAutoFillData.phoneno == undefined) {
      var vHnPh = this.state.mobile_no;
    } else {
      var vHnPh = this.state.userAutoFillData.phoneno;
    }
    //var vHnPh=this.state.userAutoFillData.phoneno;
    if (this.state.userAutoFillData.CallerName == null || this.state.userAutoFillData.CallerName == '' || this.state.userAutoFillData.CallerName == undefined) {
      console.log('this.state.First_Name', this.state.First_Name);
      var Name = this.state.First_Name;

    } else {
      var Name = this.state.userAutoFillData.CallerName;
    }

    if (this.state.userAutoFillData.callersurname == null || this.state.userAutoFillData.callersurname == '' || this.state.userAutoFillData.callersurname == undefined) {
      var SName = this.state.Last_Name;
    } else {
      var SName = this.state.userAutoFillData.callersurname;
    }

    var vadharno = this.state.AadharNo;
    if (this.state.userAutoFillData.callaltno == null || this.state.userAutoFillData.callaltno == '' || this.state.userAutoFillData.callaltno == undefined) {
      var ANo = this.state.Another_Mobile_No;
    } else {
      var ANo = this.state.userAutoFillData.callaltno;
    }

    var Email = this.state.Email;

    if (this.state.userAutoFillData.callerdistrict == null || this.state.userAutoFillData.callerdistrict == '' || this.state.userAutoFillData.callerdistrict == undefined) {
      var cdistcode = this.state.DistrictId1;
      console.log("vDist1", vDist);
    } else {
      var cdistcode = this.state.userAutoFillData.callerdistrict;

    }

    if (this.state.userAutoFillData.callerblockcode == null || this.state.userAutoFillData.callerblockcode == '' || this.state.userAutoFillData.callerblockcode == undefined || this.state.userAutoFillData.callerblockcode == 0) {

      console.log("this.state.BlockId1", this.state.BlockId1);
      var cblockcode = this.state.BlockId1;

    } else {
      var cblockcode = this.state.userAutoFillData.callerblockcode;

    }

    if (this.state.userAutoFillData.callerGPcode == null || this.state.userAutoFillData.callerGPcode == '' || this.state.userAutoFillData.callerGPcode == undefined || this.state.userAutoFillData.callerGPcode == 0) {
      console.log("this.state.GPId1", this.state.GPId1);
      var cgpcode = this.state.GPId1;

    } else {
      var cgpcode = this.state.userAutoFillData.callerGPcode;

    }


    if (this.state.userAutoFillData.calleraddress == null || this.state.userAutoFillData.calleraddress == '' || this.state.userAutoFillData.calleraddress == undefined) {
      var Add = this.state.Address;

    } else {
      var Add = this.state.userAutoFillData.calleraddress;

    }


    var UrbanRural = this.state.Radio;
    var Dep = this.state.DepartmentId;
    var Atri = this.state.AttributeId;
    var vDist = this.state.DistrictId2;
    var vGP = this.state.GPId2;
    var vBlk = this.state.BlockId2;
    var vRM = this.state.Description;
    var vWd = this.state.WardId;
    var vG = 0;
    var vZ = 0;
    var vT = this.state.ThanaId;
    var vR = 0;
    var vSD = this.state.AnubhagId;
    var vD = 0;
    var vC = this.state.CircleId;
    var vV = this.state.VitranKendraId;
    var vHn = 0;
    var vI = 0;
    var Comp_GPCode = 0;
    var vTeh = this.state.TehsilId;



    console.log(vHnPh, Name, SName, vadharno, ANo, Email, vDist, vBlk, vGP, Add, UrbanRural, Dep, Atri, cdistcode, cgpcode, cblockcode,
      vRM, vWd, vG, vZ, vT, vSD, vD, vC, vV, vHn, vI, Comp_GPCode, vTeh, vR);
console.log( Name,  SName,  ANo,  Email,  Add,  Atri,  Dep,  vDist,  vTeh,  vBlk,  vWd,  vGP,  vG,  vZ,  vT,  vR,  vSD,  vD,  vC,  vV,  vRM,  vHn,  vHnPh,  vI,  cdistcode,  cblockcode,  cgpcode,  vadharno,  UrbanRural,  Comp_GPCode);

    if (vWd == null && vWd == '') {
      vWd = 0;
      this.setSpinner(false);
    }
    if (Name == null || Name == '' || Name == 0) {
      this.showToast('कृपया नाम दर्ज करें।');
      this.setSpinner(false);
      return false;
    }
    if (SName == null || SName == '' || SName == 0) {
      this.showToast('कृपया उपनाम दर्ज करें।');
      this.setSpinner(false);
      return false;
    }
    if (cdistcode == null || cdistcode == '' || cdistcode == 0) {
      this.showToast('कृपया निवासरत जिला दर्ज करें।');
      this.setSpinner(false);
      return false;
    }
    if (cblockcode == null || cblockcode == '' || cblockcode == 0) {
      this.showToast('कृपया निवासरत ब्लॉक दर्ज करें।');
      this.setSpinner(false);
      return false;
    }
    // if (cgpcode == null || cgpcode == '' || cgpcode == 0) {
    //   this.showToast('कृपया निवासरत ग्रामपंचायत दर्ज करें।');
    //   this.setSpinner(false);
    //   return false;
    // }
    if (Add == null || Add == '' || Add == 0) {
      this.showToast('कृपया पता दर्ज करें।');
      this.setSpinner(false);
      return null;
    }
    if (UrbanRural == null || UrbanRural == '' || UrbanRural == 0) {
      this.showToast('कृपया शहरी / ग्रामीण का चयन करें।');
      this.setSpinner(false);
      return false;
    }
    if (Dep == null || Dep == '' || Dep == 0) {
      this.showToast('कृपया विभाग दर्ज करें।');
      this.setSpinner(false);
      return false;
    }
    if (Atri == null || Atri == '' || Atri == 0) {
      this.showToast('कृपया शिकायत की श्रेणी दर्ज करें।');
      this.setSpinner(false);
      return false;
    }
    if (vDist == null || vDist == '' || vDist == 0) {
      this.showToast('कृपया जिला दर्ज करें।');
      this.setSpinner(false);
      return false;
    }

    // if (vGP == null || vGP == '' || vGP == 0) {
    //   this.showToast('कृपया ग्रामपंचायत दर्ज करें।');
    //   this.setSpinner(false);
    //   return false;
    // }
    //var k=this.State.blockActive;
    // var k=this.state.blockActive;
    // alert(this.state.blockActive);
    // return false;
    if(this.state.blockActive==true && (vBlk=="" || vBlk==null || vBlk==0))
    {
      this.showToast('कृपया ब्लाक चयन करें');
      this.setSpinner(false);
      return false;
    }
    if ((Dep == 2 || Dep == 32 || Dep ==62 ) && (vTeh==null || vTeh == '' || vTeh == 0)) {
      this.showToast('कृपया तहसील चयन करें।');
      this.setSpinner(false);
      return false;
    }
  else  if (Dep == 24 && (vT==null || vT == '' || vT == 0)) {
      this.showToast('कृपया थाना चयन करें।');
      this.setSpinner(false);
      return false;
    }
    
    if ((Dep == 24 || Dep == 95) && (vSD==null || vSD == '' || vSD == 0 ) ) {
      this.showToast('कृपया अनुभाग चयन करें।');
      this.setSpinner(false);
      return false;
    }
   
    if (Dep == 95 && (vV==null || vV == '' || vV == 0)) {
      this.showToast('कृपया वितरण केंद्र चयन करें।');
      this.setSpinner(false);
      return false;
    }
    
    // else
    //  {
    //   this.showToast('कृपया ब्लाक चयन करें।');
    //   this.setSpinner(false);
    //   return false;
    // }
   
    if (vRM == null || vRM == '' || vRM == 0) {
      this.showToast('कृपया विवरण (न्यूनतम 50 शब्द) दर्ज करें।');
      this.setSpinner(false);
      return false;
    }

    if (vRM.length > 50) {
      console.log('vRM.length');
      console.log(vRM.length);
      if (vHnPh != null && vHnPh != '' && Name != null && Name != '' && SName != null && SName != '' && vDist != null && vDist != '' && vDist != 0 && cblockcode != null && cblockcode != '' && cblockcode != 0
        && UrbanRural != null && UrbanRural != '' && UrbanRural != 0 && Dep != null && Dep != '' && Dep != -1 && Atri != null && Atri != '' && Atri != 0 && cdistcode != null && cdistcode != '' && cdistcode != 0 && cgpcode != null
        && vRM != null && vRM != '') {
          Comp_GPCode="0";
        Alert.alert(
          'Alert',
          'क्या आप शिकायत दर्ज करना चाहते हैं!',
          [
            { text: 'Cancel', onPress: () => this.setSpinner(false) },
            {
              text: 'OK', onPress: () => Apis.RegisterComplaint(vHnPh, Name, SName, vadharno, ANo, Email, vDist, vBlk, vGP, Add, UrbanRural, Dep, Atri, cdistcode, cgpcode, cblockcode,
                vRM, vWd, vG, vZ, vT, vR, vSD, vD, vC, vV, vHn, vI, Comp_GPCode, vTeh).then((res) => {
                  this.setSpinner(false);
                  console.log(res);
                  console.log('res.messages[0].MSG');
                  console.log(res.messages[0].MSG);
                  let complaintNo = res.messages[0].MSG;
                  if (res.status == 'success') {
                    Alert.alert(
                      'Alert',
                      'आपने शिकायत  का पंजीयन सफलतापूर्वक कर लिया है । आपकी शिकायत का क्रमांक : ' + complaintNo + ' है।',
                      [
                        // { text: 'Cancel', onPress: () => this.setSpinner(false) },
                        { text: 'OK', onPress: () => { } },
                        //{text: 'OK', onPress: () => console.log('OK Pressed')},
                      ],
                      { cancelable: false }
                    )


                   
                    this.props.navigation.navigate('CitizenDashboard');
                  }
                  else {
                    this.setSpinner(false);
                    alert('"माफ़ कीजिये: शिकायत का पंजीयन करने में असमर्थ!');
                  }

                }, (error) => {
                  this.setSpinner(false);
                  console.log(error);
                  alert('"माफ़ कीजिये: शिकायत का पंजीयन करने में असमर्थ!');
                })
            },
            //{text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          { cancelable: false }
        )

        // Apis.RegisterComplaint(vHnPh,Name,SName,vadharno,ANo,Email,vDist,vBlk,vGP,Add,UrbanRural,Dep,Atri,cdistcode,cgpcode,cblockcode,
        //   vRM,vWd,vG,vZ,vT,vR,vSD,vD,vC,vV,vHn,vI,Comp_GPCode,vTeh).then((res) => {
        //     this.setSpinner(false);
        //     alert("set success 2");
        //     console.log(res.messages[0].MSG);
        //     let complaintNo=res.messages[0].MSG;
        //       if(res.status=='success'){
        //         alert("set success 3");
        //           alert('आपकी शिकायत का क्रमांक : '+complaintNo+' है।');
        //           this.showToast('आपने शिकायत  का पंजीयन सफलतापूर्वक कर लिया है । आपकी शिकायत का क्रमांक : '+complaintNo+' है।');
        //
        //         }
        //         else{
        //             this.setSpinner(false);
        //             this.showToast('"कृपया पुन: प्रयास करें।');
        //         }

        // },(error) => {
        //    this.setSpinner(false);
        //     this.showToast('माफ़ कीजिये: शिकायत का पंजीयन करने में असमर्थ!');
        // });
      }
      else {
        this.setSpinner(false);
        alert('कृपया (*) सभी जानकारी भरें।');
      }

    }
    else {
      this.setSpinner(false);
      alert('कृपया विवरण में न्यूनतम 50 शब्द लिखें!');
    }


  }


  componentWillUpdate() {
    return false;
  }

  render() {
    return (
      <Root>
        <Container style={{ backgroundColor: '#ffffffe3' }}>
        {/* <StatusBar backgroundColor="#E6E6E6" barStyle="light-content" /> */}
        <Header style={{ backgroundColor: '#117bc7' }} androidStatusBarColor="#268DC8" >
           
            <Left>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()} >
                  < Icon style={{ color: '#fff', fontSize: 30,marginLeft:10 }} name="ios-arrow-back" />
                </TouchableOpacity>
            </Left>
            <Body>
              <Title style={{ color: '#ffff' }}>शिकायत का पंजीकरण</Title>
            </Body>

          </Header>

          <Content>
           
            <Card style={{ width: '100%', height: '100%', marginTop: 20, alignSelf: 'center' }}>
              <Text style={{ backgroundColor: '#e8e8e8', height: 40, padding: 5, margin: 8, fontSize: 18 }}>शिकायतकर्ता की जानकारी</Text>
              <Form style={{ marginLeft: 10, marginRight: 10 }}>
                <Item>
                  {this.state.userAutoFillData.phoneno != null ? (<Input disabled placeholder="मोबाइल नंबर *" ref='mobile' keyboardType='default' style={{ color: '#808080' }} placeholderTextColor='#808080' underlineColorAndroid='rgba(0,0,0,0)' value={this.state.userAutoFillData.phoneno} />) :

                    <Input disabled placeholder="मोबाइल नंबर *" style={{ color: '#808080' }} ref='mobile' keyboardType='numeric' underlineColorAndroid='rgba(0,0,0,0)' onChangeText={(text) => { this.setState({ mobile_no: text }) }} value={this.state.mobile_no} />}

                </Item>
                <Item>
                  {this.state.userAutoFillData.CallerName != null ? (<Input disabled placeholder="नाम *" ref='mobile' keyboardType='default' style={{ color: '#808080' }} placeholderTextColor='#808080' underlineColorAndroid='rgba(0,0,0,0)' value={this.state.userAutoFillData.CallerName} />) :

                    <Input placeholder="नाम *" ref='name' keyboardType='default' placeholderTextColor='#808080' underlineColorAndroid='rgba(0,0,0,0)' onChangeText={(text) => { this.setState({ First_Name: text }) }} value={this.state.First_Name} />}
                </Item>
                <Item>
                  {this.state.userAutoFillData.callersurname != null ? (<Input disabled placeholder="उपनाम *" ref='mobile' keyboardType='default' style={{ color: '#808080' }} placeholderTextColor='#808080' underlineColorAndroid='rgba(0,0,0,0)' value={this.state.userAutoFillData.callersurname} />) :
                    <Input placeholder="उपनाम *" ref='name' keyboardType='default' placeholderTextColor='#808080' underlineColorAndroid='rgba(0,0,0,0)' onChangeText={(text) => { this.setState({ Last_Name: text }) }} value={this.state.Last_Name} />}
                </Item>
                <Item>
                  <Input maxLength={12} placeholder="आधार नंबर" ref='mobile' keyboardType='numeric' placeholderTextColor='#808080' underlineColorAndroid='rgba(0,0,0,0)' onChangeText={(text) => { this.setState({ AadharNo: text }) }} value={this.state.AadharNo} />
                </Item>
                <Item>
                  <Input maxLength={10} placeholder="अन्य मोबाइल नंबर" ref='mobile' keyboardType='numeric' placeholderTextColor='#808080' underlineColorAndroid='rgba(0,0,0,0)' onChangeText={(text) => { this.setState({ Another_Mobile_No: text }) }} value={this.state.Another_Mobile_No} />
                </Item>
                <Item>
                  <Input placeholder="ई-मेल" ref='email' keyboardType='default' placeholderTextColor='#808080' underlineColorAndroid='rgba(0,0,0,0)' onChangeText={(text) => { this.setState({ Email: text }) }} value={this.state.Email} />
                </Item>



                {this.state.userAutoFillData.callerdistrict != null && this.state.userAutoFillData.callerdistrict != '' && this.state.userAutoFillData.callerdistrict != 0 ? (<Input disabled placeholder="निवासरत जिला *" ref='mobile' keyboardType='default' style={{ color: '#808080' }} placeholderTextColor='#808080' underlineColorAndroid='rgba(0,0,0,0)' onChangeText={(text) => { this.setState({ DistrictId1: this.state.userAutoFillData.callerdistrict }) }} value={this.state.userAutoFillData.District_Name_H} />) :
                  this.renderDistrictPicker1()
                }

                <Item />
                {this.state.userAutoFillData.callerblockcode != 0 ? (
                  this.state.userAutoFillData.callerblockcode != null && this.state.userAutoFillData.callerblockcode != '' ? (<Input disabled placeholder="निवासरत ब्लॉक *" ref='mobile' keyboardType='default' style={{ color: '#808080' }} placeholderTextColor='#808080' underlineColorAndroid='rgba(0,0,0,0)' onChangeText={(text) => { this.setState({ BlockId1: this.state.userAutoFillData.callerblockcode }) }} value={this.state.userAutoFillData.Block_Name_H} />) :
                    this.renderBlockPicker1())
                  : this.renderBlockPicker1()}

                <Item />
                {/* {this.state.userAutoFillData.callerGPcode != 0 ? (
                  this.state.userAutoFillData.callerGPcode != null && this.state.userAutoFillData.callerGPcode != '' && this.state.userAutoFillData.callerGPcode != 0 ? (<Input disabled placeholder="निवासरत ग्राम पंचायत *" ref='mobile' keyboardType='default' style={{ color: '#808080' }} placeholderTextColor='#808080' underlineColorAndroid='rgba(0,0,0,0)' onChangeText={(text) => { this.setState({ GPId1: this.state.userAutoFillData.callerGPcode }) }} value={this.state.userAutoFillData.GP_Name_H} />) :
                    this.renderGramPanchayatPicker1())
                  : this.renderGramPanchayatPicker1()} */}

                <Item />

                <Item>
                  {this.state.userAutoFillData.calleraddress != null && this.state.userAutoFillData.calleraddress != '' ? (<Input disabled placeholder="पता *" ref='mobile' keyboardType='default' style={{ color: '#808080' }} placeholderTextColor='#808080' underlineColorAndroid='rgba(0,0,0,0)' value={this.state.userAutoFillData.calleraddress} />) :
                    <Input placeholder="पता *" placeholderTextColor='#808080' ref='mobile' keyboardType='default' underlineColorAndroid='rgba(0,0,0,0)' onChangeText={(text) => { this.setState({ Address: text }) }} value={this.state.Address} />}
                </Item>
              </Form>

              <Card style={{ width: '100%', height: '100%', marginTop: 20, alignSelf: 'center' }}>
              <Text style={{ backgroundColor: '#e8e8e8', height: 40, padding: 5, margin: 8, fontSize: 18 }}>शिकायतकर्ता का पंजीयन</Text>
              <Form style={{ marginLeft: 10, marginRight: 10, marginTop: 10 }}>

                <Text style={{ marginLeft: 45 }}>शिकायत के आधार पर शहरी अथवा ग्रामीण </Text>
                <Text style={{ marginLeft: 120 }}> का चयन करें:</Text>



                <Item style={{ marginLeft: 75, borderBottomWidth: 0 }}>

                  <Item style={{ borderBottomWidth: 0 }}>

                    <Text style={{ padding: 10 }}>शहरी</Text>
                    <Radio selected={this.state.Radio == "U" ? true : false} ref='Radio' onPress={() => { this.setRadio("U") }} />
                  </Item>

                  <Item style={{ borderBottomWidth: 0 }}>
                    <Text style={{ padding: 10 }}>ग्रामीण</Text>
                    <Radio selected={this.state.Radio == "R" ? true : false} ref='Radio' onPress={() => { this.setRadio("R") }} />

                  </Item>
                </Item>

                {this.renderDepartmentPicker()}
                <Item />

                {this.renderAttributePicker()}
                <Item />

                {this.renderDistrictPicker2()}

                <Item />
                {this.state.thanaActive ? (this.renderThanaPicker()) : null}

                <Item />

                {this.state.blockActive ? (this.renderBlockPicker2()) : null}

                <Item />
                {this.state.wardActive ? (this.renderWardPicker()) : null}

                <Item />

                {this.state.anubhagActive ? (this.renderAnubhagPicker()) : null}
                <Item />

                {this.state.tehsilActive ? (this.renderTehsilPicker()) : null}
                <Item />

                {this.state.vkActive ? (this.renderVitranKendraPicker()) : null}
                <Item />
                {this.state.circleActive ? (this.renderCirclePicker()) : null}
                <Item />

                <Item />

                {/* {this.renderGramPanchayatPicker2()} */}

                <Input multiline={true} placeholder="विवरण *" placeholderTextColor='#808080' ref='mobile' keyboardType='default' underlineColorAndroid='rgba(0,0,0,0)' onChangeText={(text) => { this.setState({ Description: text }) }} value={this.state.Description} />
                <Item />
                {this.state.Description != null && this.state.Description != '' && this.state.DepartmentId != '' && this.state.DepartmentId != null && this.state.AttributeId != '' && this.state.AttributeId != null && this.state.AttributeId != 0
                  && this.state.DistrictId2 != 0 && this.state.DistrictId2 != '' && this.state.DistrictId2 != null  ? (
                    <Button block info style={{ backgroundColor:'#268DC8',width: '80%', marginTop: 15, marginBottom: 10, alignSelf: 'center' }} onPress={() => this.RegisterNewComplaint()} >
                      <Text>जन शिकायत को दर्ज करे</Text>
                    </Button>
                  )
                  : <Button block disabled style={{backgroundColor:'#268DC8', width: '80%', marginTop: 15, marginBottom: 10, alignSelf: 'center' }} onPress={() => this.RegisterNewComplaint()} >
                    <Text>जन शिकायत को दर्ज करे</Text>
                  </Button>}
              </Form>

            </Card>
            </Card>
            
          </Content>

          {this.state.loading ?
            <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)', alignItems: 'center', justifyContent: 'center', position: 'absolute', width: '100%', height: '100%' }}>
              <Spinner color="green" />
            </View> : null}
        </Container>
      </Root>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});