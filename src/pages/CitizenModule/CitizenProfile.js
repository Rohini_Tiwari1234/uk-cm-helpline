import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Dimensions,
  BackHandler,
  AsyncStorage,
  Keyboard,
  Image,
  TouchableOpacity,
  Modal,
  ScrollView, Alert,
  ImageBackground
} from 'react-native';
import { Toast, Radio, Grid, FooterTab, Separator, StatusBar, TabHeading, Footer, Fab, ActionSheet, Picker, View, Row, Col, Container, List, Switch, Header, Item, Spinner, Label, Content, Form, Input, Icon, Left, Button, Title, Body, Right, SocialIcon, Badge, Card, CardItem, Text, Thumbnail, ListItem, CheckBox, Tabs, Tab } from 'native-base';

import Apis from '../../apis/CitizenApi';


export default class Profile extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      userDetail: [],
      userObject: [],
      all: '',
      loading: false,
      mobile_no: '',
    };
  }

  setSpinner = (st) => {
    this.setState({ loading: st });
  }

  componentDidMount() {
    this.setSpinner(true);

    AsyncStorage.getItem("mobile_no").then((value) => {

      this.setState({
        mobile_no: value
      });
    });

    AsyncStorage.getItem("userDetail").then((value) => {
      this.setSpinner(false);

      Apis.getUserDetailsByPhoneNo(this.state.mobile_no).then((res) => {
        console.log(res);
        console.log('res.messages[0]');
        console.log(res.messages[0]);
        if (res.messages == 'Record not found') {
          this.setState({
            userDetail: null
          })
        }
        else {
          this.setState({
            userDetail: res.messages[0]
          })
        }
        console.log('this.state.userDetail');
        console.log(this.state.userDetail);
      });

      // for (let userObject of this.state.userDetail) {
      // this.setState({
      //     userObject: userObject
      // });
      // }
      // this.setState({
      //    userDetail: JSON.parse(value)
      // });
      //   for (let userObject of this.state.userDetail.messages) {
      //       this.setState({
      //         userObject: userObject
      //     });
      //       console.log('userObject.messages.CallerName');
      //       console.log(this.state.userObject.CallerName);

      //     }

    });
    
  }

  ConfirmationToLogout = () => {

    Alert.alert(
      'Alert',
      'क्या आप लॉगआउट करना चाहते हैं ?',
      [
        { text: 'Cancel', onPress: () => this.setSpinner(false) },
        { text: 'OK', onPress: () => this.props.navigation.navigate('Logout') },

      ],
      { cancelable: false }
    )

  }



  render() {
    return (
      <Container style={{ backgroundColor: '#ffffffe3' }}>
        <Header>
          <Left>
            <Button transparent >
              <Icon name='md-arrow-round-back' style={{ color: '#fff' }} />

            </Button>
          </Left>
          <Body>
            <Title style={{ color: '#fff' }}> प्रोफाइल </Title>
          </Body>
          <Right>
            <Button transparent onPress={() => this.ConfirmationToLogout()} vertical >
              <Icon name='md-log-out' style={{ color: '#fff' }} />
              <Text style={{ color: '#fff' }}>लॉगआउट</Text>
            </Button>
          </Right>
        </Header>

        <Card style={{ backgroundColor: 'rgba(255, 255, 255, .4)', height: 200 }} >

          {/* <ImageBackground source={require('../../assets/Image/background.jpg')} style={{ height: 200, width: null, flex: 1 }}> */}

            <View style={{ top: 50, alignSelf: 'center', bottom: 40 }}>
              <Image source={require('../../assets/Image/default.png')} style={{ height: 100, width: 100, borderRadius: 20 }} />
            </View>
            {this.state.userDetail!=null ?(
              <Body style={{ marginTop: '15%' }}>

                <Text>{this.state.userDetail.CallerName + '  ' + this.state.userDetail.callersurname}</Text>
                <Text>{this.state.mobile_no}</Text>

              </Body>)
              : <Body style={{ marginTop: '15%' }}>

                <Text></Text>
                <Text></Text>

              </Body>}
          {/* </ImageBackground> */}


        </Card>


        <Content>
          <Card>
            
            <ListItem itemDivider style={{ paddingBottom: 5 }}>

              {this.state.userDetail != null ? (
                <Row>
                  <Col style={{ marginLeft: '-12%' }}><Text>आधार नंबर</Text></Col>
                  <Col style={{ width: '1%' }}><Text>:</Text></Col>
                  <Col><Text>{this.state.userDetail.AadharNo}</Text></Col>
                </Row>
              ) : <Text>आधार नंबर :</Text>}
            </ListItem>
            <ListItem style={{ paddingBottom: 5 }}>
              {this.state.userDetail != null ? (
                <Row>
                  <Col style={{ marginLeft: '-9%' }}><Text>अन्य मोबइल नंबर</Text></Col>
                  <Col style={{ width: '1%' }}><Text>:</Text></Col>
                  <Col><Text>{this.state.userDetail.callaltno}</Text></Col>
                </Row>) : <Text>अन्य मोबइल नंबर :</Text>}

            </ListItem>
            <ListItem itemDivider style={{ paddingBottom: 5 }}>
              {this.state.userDetail != null ? (
                <Row>
                  <Col style={{ marginLeft: '-12%' }}><Text>ब्लॉक</Text></Col>
                  <Col style={{ width: '1%' }}><Text>:</Text></Col>
                  <Col><Text>{this.state.userDetail.Block_Name_H}</Text></Col>
                </Row>)
                : <Text>ब्लॉक :</Text>}
            </ListItem>
            <ListItem style={{ paddingBottom: 5 }}>
              {this.state.userDetail != null ? (
                <Row>
                  <Col style={{ marginLeft: '-11%' }}><Text>जिला</Text></Col>
                  <Col style={{ width: '1%' }}><Text>:</Text></Col>
                  <Col><Text>{this.state.userDetail.District_Name_H}</Text></Col>
                </Row>) : <Text>जिला :</Text>}
            </ListItem >
            {/* <ListItem itemDivider style={{ paddingBottom: 5 }}>
              {this.state.userDetail != null ? (
                <Row>
                  <Col style={{ marginLeft: '-12%' }}><Text>ग्राम पंचायत</Text></Col>
                  <Col style={{ width: '1%' }}><Text>:</Text></Col>
                  <Col><Text>{this.state.userDetail.GP_Name_H}</Text></Col>
                </Row>) : <Text>ग्राम पंचायत :</Text>}
            </ListItem> */}
            <ListItem style={{ paddingBottom: 5 }}>
              {this.state.userDetail != null ? (
                <Row>
                  <Col style={{ marginLeft: '-12%' }}><Text>पता</Text></Col>
                  <Col style={{ width: '1%' }}><Text>:</Text></Col>
                  <Col><Text>{this.state.userDetail.calleraddress}</Text></Col>
                </Row>) : <Text>पता :</Text>}
            </ListItem>
          </Card>

        </Content>
        {this.state.loading ?
              <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)', alignItems: 'center', justifyContent: 'center', position: 'absolute', width: '100%', height: '100%' }}>
                <Spinner color="white" />
              </View> : null}
      

      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});