
import React, { Component } from 'react';
import Apis from '../../apis/Api';
import Form from 'react-native-form';
import { NavigationBar, Title, Screen, Divider, Button, Text, Icon, Image, centerComponent, View, Row, TouchableOpacity, Card, ImageBackground } from '@shoutem/ui';
import Spinner from 'react-native-loading-spinner-overlay';
import DeviceInfo from 'react-native-device-info';

import { AppRegistry, StyleSheet, Platform, NetInfo, Alert, AsyncStorage, StatusBar, TextInput,KeyboardAvoidingView, ScrollView } from 'react-native';

const brandColor = '#ade3ff';
const styles = StyleSheet.create({
  countryPicker: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  container: {
    flex: 1
  },
  header: {
    textAlign: 'center',
    marginTop: 60,
    fontSize: 22,
    margin: 20,
    color: '#4A4A4A',
  },
  form: {
    margin: 20
  },
  textInput: {
    padding: 0,
    margin: 0,
    flex: 1,
    fontSize: 20,
    color: brandColor
  },
  button: {
    marginTop: 20,
    height: 50,
    backgroundColor: brandColor,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  buttonText: {
    color: '#fff',
    fontFamily: 'Helvetica',
    fontSize: 16,
    fontWeight: 'bold'
  },
  wrongNumberText: {
    margin: 10,
    fontSize: 14,
    textAlign: 'center'
  },
  disclaimerText: {
    marginTop: 30,
    fontSize: 12,
    color: 'grey'
  },
  callingCodeView: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  callingCodeText: {
    fontSize: 20,
    color: brandColor,
    fontFamily: 'Helvetica',
    fontWeight: 'bold',
    paddingRight: 10
  }
});
export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPassword: true,
      spinner: false,
      userid: '',
      Password: '',
      country: {
        cca2: 'US',
        callingCode: '1'
      }
    };
    this.toggleSwitch = this.toggleSwitch.bind(this);
    this._verifySubmitAction = this._verifySubmitAction.bind(this);
  }

  componentDidMount() {
    this.setState({ spinner: false });

    var uniqueId = DeviceInfo.getUniqueID();
    console.log(uniqueId);
    Apis.AddRegister_CitizenAppmobileuser(uniqueId).then((res) => {
      // this.setState({
      //   spinner: true
      // });

      console.log(res);
      console.log(res.status);

      if (res.status == 'success') {
        setTimeout(async () => {

          try {

            console.log("res.err", res);


            this.setState({
              spinner: false,

            });


          } catch (err) {

            this.setState({ spinner: false });
            setTimeout(() => {
              console.log('Oops');

              Alert.alert('Oops!', err);
            }, 5000);
          }

        }, 1000);
      }
      else {
        this.setSpinner(false);
        this.showToast('कृपया पुन: प्रयास करें।');
      }
    }, (error) => {
      Alert.alert('Oops!', err);
      this.setSpinner(false);

    });
  }


  _verifySubmitAction = () => {

    if (this.state.userid == "" || this.state.userid == null) {
      alert("कृपया यूजर आईडी दर्ज करें");
    }
    else if (this.state.Password == "" || this.state.Password == null) {
      alert("कृपया पासवर्ड डालें");
    }
    else {
      this.setState({ spinner: true });

      setTimeout(async () => {
        var uniqueId = DeviceInfo.getUniqueID();
        Apis.getOfficerLogin(uniqueId, this.state.userid, this.state.Password).then((res) => {
          this.setState({
            spinner: true
          });
          try {

            console.log(res);


            if (res.err) throw res.err;

            // this.refs.textInput.blur();
            if (res.status == 'success') {


              setTimeout(() => {
                Alert.alert('Success!', 'आपने अपने विवरणों को सफलतापूर्वक सत्यापित कर लिया है');
                this.setState({ spinner: false });
                console.log('data');
                console.log(res);
                AsyncStorage.setItem("userDetail", JSON.stringify(res.user));
                AsyncStorage.getItem("userDetail").then((value) => {
                  console.log('value');
                  console.log(JSON.parse(value));
                });

                AsyncStorage.setItem("loggedIn", "1");
                this.props.navigation.navigate('Dashboard');
              }, 1000);
            }
            else {
              this.setState({ spinner: false });
              // Alert.alert("Invalid Login Credentials!..");
              Alert.alert("पुनः प्रयास करें  !..");
              // Alert.alert(res.body.messages);
              this.props.navigation.navigate('Auth');
            }
          } catch (err) {
            this.setState({ spinner: false });
            setTimeout(() => {
              Alert.alert('Oops!', err.message);
            }, 1000);
          }


        }, (error) => {
          Alert.alert('Oops!', err.message);
          this.setSpinner(false);

        });
      }, 5000);
    }
  }

  toggleSwitch = () => {

    this.setState({ showPassword: !this.state.showPassword });

  }
  render() {

    let buttonText = 'Login';
    let textStyle = {};
    const keyboardVerticalOffset = Platform.OS === 'ios' ? 40 : 0
    return (
      <View style={{ flex: 1, backgroundColor: '#117bc7' }}>
        <StatusBar
          barStyle="dark-content"
          backgroundColor="#117bc7"
        />

        <ScrollView>
          <KeyboardAvoidingView style={{ flex: 1 }}
            keyboardVerticalOffset={keyboardVerticalOffset} behavior={"position"}>

            <Image style={{ alignSelf: 'center', marginTop: 50, height: 180 }}
              styleName="medium-avatar"
              source={require('../../assets/icon/icon-01.png')}
            />

            {/* <Text style={{ textDecorationLine: 'underline', marginTop: '-20%', marginLeft: '85%' }}>Skip</Text> */}


            <View style={{ width: '90%', alignSelf: 'center', marginTop: '10%' }} >

              <View style={{ borderRadius: 5, width: '95%', backgroundColor: '#399DD8' }}>


                <TextInput style={{ alignItems: 'center', color: 'white', borderRadius: 5, width: '90%', backgroundColor: '#399DD8' }}
                  placeholder={'नाम '}
                  ref={'textInput'}
                  name={'mobileno'}
                  type={'TextInput'}
                  placeholderTextColor="#fff"
                  ref={input => { this.textInput = input }}
                  value={this.state.userid}
                  onChangeText={(text) => this.setState({ userid: text })}
                />
              </View>

              <View styleName="horizontal"
                style={{ borderRadius: 5, width: '95%', backgroundColor: '#399DD8', height: 45, marginTop: 10 }}>
                <View style={{ borderRadius: 5, width: '80%', backgroundColor: '#399DD8' }}>

                  <TextInput style={{ alignItems: 'center', color: '#fff', borderRadius: 5, width: '95%', backgroundColor: '#399DD8' }}
                    placeholder={'पासवर्ड  '}
                    placeholderTextColor="#fff"
                    ref={'textInput'}
                    name={'mobileno'}
                    type={'TextInput'}
                    secureTextEntry={true}
                    secureTextEntry={this.state.showPassword}
                    ref={input => { this.textInput = input }}
                    value={this.state.Password}
                    onChangeText={(text) => this.setState({ Password: text })}
                  />
                </View>
                <View styleName="space-between" style={{ width: '10%', borderRadius: 5 }}>
                  <TouchableOpacity onPress={this.toggleSwitch}
                    value={!this.state.showPassword}>


                    <Image source={require('../../assets/Image/icon_view_password.png')}
                      style={{ marginBottom: 10, right: 6 }} />
                  </TouchableOpacity>
                </View>

              </View>

              <Button style={{ backgroundColor: '#05B2F4', borderRadius: 5, borderWidth: 2, fontFamily: 'CeraPRO-Bold', borderColor: '#05B2F4', marginTop: '5%', width: '95%', height: 45 }} onPress={this._verifySubmitAction} >
                <Text style={{ color: '#fff', fontSize: 17, bottom: 5 }}>लॉगिन </Text>
              </Button>
              {/* <Text style={{ marginTop:'3%',marginLeft:'5%', color:'#2297d3', fontWeight:'bold'}} >
                          Forgot your login details ? get help singing in
                      </Text> */}
            </View>

          


            {/* <View style={{ borderBottomColor: '#E6E6E6', borderBottomWidth: 1, width: '100%',marginTop:'30%'}} />
                   
                    <View style={{backgroundColor:'#306EB9',height:'100%',width:'100%'}}>
                        <View styleName="horizontal"style={{alignSelf:'center'}}>
                          <Text style={{color:'#fff',alignSelf:'center',fontSize:16,marginTop:20}}>
                              Don't have account ? 
                            </Text>
                          <Text style={{color:'#fff',fontSize:18,paddingLeft:3}} >
                          Sign up
                          </Text>
                        </View>
                    </View> */}

              </KeyboardAvoidingView>
            </ScrollView>

            <Spinner
              visible={this.state.spinner}
              textContent={'कृपया प्रतिक्षा करें...'}
              textStyle={{ color: '#fff' }} />
          </View>


          );
    }
}