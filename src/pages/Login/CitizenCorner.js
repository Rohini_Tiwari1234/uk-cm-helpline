/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import { StyleSheet, Footer, Dimensions, StatusBar } from 'react-native';
import React, { Component, Container, } from 'react';
import { AsyncStorage } from 'react-native';
import { NavigationBar, Title, Screen, Divider, Button, Text, Icon, Image, centerComponent, TextInput, View, Row, TouchableOpacity, Card, ImageBackground } from '@shoutem/ui';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import { ScrollView } from 'react-native-gesture-handler';
const ScreenHeight = Dimensions.get("window").height;
const sliderWidth = Dimensions.get("window").width;

export default class CitizenCorner extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      loading: false
    };
  }
  DepartmentUserLogin=()=>{
      this.props.navigation.navigate('Login');
  }

  CitizenUserLogin=()=>{
    this.props.navigation.navigate('CitizenLogin');
}

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#117bc7' }}>
  <StatusBar
          barStyle="dark-content"
          backgroundColor="#117bc7"
        />
        
        
        
     
      
      
        <Image style={{ alignSelf: 'center',marginTop:50}}
  styleName="medium-avatar"
  source={require('../../assets/icon/icon-01.png')}
/>

         
         
          
<Text style={{fontSize:16,alignSelf:'center' ,color:'#fff', fontWeight:'bold',marginTop:'10%'}} >
सिटीजन कॉर्नर 
            </Text>
          
           <View style={{marginTop:20}}>
            {/* <Button style={{backgroundColor:'#306EB9',marginLeft:5, borderRadius: 5, borderWidth: 2, fontFamily: 'CeraPRO-Bold', borderColor: '#306EB9', marginTop: '5%', width: '48%',  height:50 }} onPress={() => this.Login()} >
              <Text style={{color:'#fff',bottom:5}}>Register</Text>
            </Button> */}
            
            <Button style={{backgroundColor:'#fff', borderRadius: 5, borderWidth: 2, fontFamily: 'CeraPRO-Bold', borderColor: '#fff', marginTop: '5%', width: '48%',  height:50,alignSelf:'center' }} onPress={() => this.CitizenUserLogin()} >
              <Text style={{color:'#05B2F4',bottom:5,fontSize:16}}>लॉगिन</Text>
            </Button>
            </View>
         
         
          
         
           <View style={{ borderBottomColor: '#E6E6E6',  borderBottomWidth: 1,width: '95%',marginLeft:'3%',marginRight:'3%',marginTop:'10%' }} />
           <Text style={{color:'#fff',fontSize:16,alignSelf:'center',marginTop:20,fontWeight:'bold'}}>
           फॉर डिपार्टमेंट यूजर 
            </Text>
          
            <Button style={{backgroundColor:'#fff',marginLeft:6,marginRight:8, borderRadius: 5, borderWidth: 2, fontFamily: 'CeraPRO-Bold', borderColor: '#fff', marginTop: '5%', width: '48%',  height:50,alignSelf:'center' }} onPress={() => this.DepartmentUserLogin()} >
              <Text style={{color:'#05B2F4',paddingTop:5,fontSize:16}}>लॉगिन</Text>
            </Button>
        
       
       

      </View>


    );

  }
}
const styles = StyleSheet.create({
placeholder: {
  borderRadius: 5,
  backgroundColor: '#ce9440',
  height: 55,
  width: '90%',
  fontWeight: 'bold',
  marginLeft: 12,
 
  borderColor: '#ce9440',
  borderWidth: 2,
  marginTop: '5%',
  alignSelf: 'center',
  marginRight: '3%',
  fontFamily: 'CeraPro-Bold'

},


})