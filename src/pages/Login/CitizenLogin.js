/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,StatusBar,
  Platform, Image, NetInfo,
  Alert, ImageBackground, AsyncStorage
} from 'react-native';

import Frisbee from 'frisbee';
import Spinner from 'react-native-loading-spinner-overlay';
import Form from 'react-native-form';
import DeviceInfo from 'react-native-device-info';
import { Grid, Separator, Row, Col, Container, List, Switch, Header, Item, Label, Content, Input, Icon, Left, Button, Title, Body, Right, SocialIcon, Card, CardItem, Thumbnail, ListItem, CheckBox, Tabs, Tab } from 'native-base';
import Apis from '../../apis/CitizenApi';

// var DeviceInfo = require('react-native-device-info');

const api = new Frisbee({
  //baseURI: 'http://10.75.159.78',
  baseURI: 'http://cmhelpline.mp.gov.in:90/api',
  // headers: {
  //   'Accept': 'application/json',
  //   'Content-Type': 'application/json'
  // }
 
});
console.log(api);
// const apis = new Frisbee({
//   baseURI: 'http://localhost:3000',
  // headers: {
  //   'Accept': 'application/json',
  //   'Content-Type': 'application/json'
  // }
// });

const MAX_LENGTH_CODE = 6;
const MAX_LENGTH_NUMBER = 10;

// if you want to customize the country picker


// your brand's theme primary color
const brandColor = '#fff';

const styles = StyleSheet.create({
 
  container: {
    flex: 1
  },
  header: {
    textAlign: 'center',
    marginTop: 60,
    fontSize: 22,
    margin: 20,
    color: '#4A4A4A',
  },
  form: {
    margin: 20
  },
  textInput: {
    padding: 0,
    margin: 0,
    flex: 1,
    fontSize: 20,
    color:'#fff',
    marginLeft:10
  },
  button: {
    marginTop: 20,
    height: 50,
    backgroundColor: '#05B2F4',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    borderColor: '#fff'
  },
  buttonText: {
    color: '#fff',
    fontFamily: 'Helvetica',
    fontSize: 16,
    fontWeight: 'bold'
  },
  wrongNumberText: {
    margin: 10,
    fontSize: 14,
    textAlign: 'center'
  },
  disclaimerText: {
    marginTop:10,
    fontSize: 12,
    color: '#E6E6E6'
  },


});

export default class CitizenLogin extends Component {

  constructor(props) {
    super(props);
    this.state = {
      enterCode: false,
      spinner: false,
      mobileno: '',
      mob: '',
      otp: '',
      country: {
        cca2: 'US',
        callingCode: '1'
      }
    };
  }

  _getCode = () => {

    var uniqueId = DeviceInfo.getUniqueID();
    console.log('uniqueId');
    console.log(uniqueId);

    if (this.state.mobileno != null && this.state.mobileno != '' && this.state.mobileno.length == 10) {
      this.setState({
        spinner: true
      });

      Apis.AddRegister_CitizenAppmobileuser(uniqueId,this.state.mobileno).then((res) => {
        this.setState({
          spinner: true
        });
        console.log('AddRegister_CitizenAppmobileuser res');
        console.log(res);
        console.log(res.status);
       
        if (res.status == 'success') {
          setTimeout(async () => {
            console.log('this.state.mob kajal 12');
            try {
              this.setState({ mob: this.state.mobileno });
              console.log('this.state.mob kajal');
              console.log(this.state.mob);
              AsyncStorage.setItem("mobile_no", this.state.mob);
              console.log("this.state.mob kajal", this.state.mobileno);
              
            

               
              // const res = api.get('/api-login.asmx/send_otp?mobileno=' + this.state.mobileno, {
                
                // body: {
                //   ...this.refs.form.getValues(),
                //   //this.state.country
                // }
              // });

          const res=   Apis.generateOTP(this.state.mobileno).then((response) => {

                console.log('res response', response)
                body: {
                  this.refs.form.getValues()
                  //this.state.country
                }

              });

              

              // if (res.err) throw res.err;
              console.log('const res response',res.body,res)

              this.setState({
                spinner: false,
                enterCode: true,
                verification: res.body
              });
              this.refs.form.refs.textInput.setNativeProps({ text: '' });

              setTimeout(() => {
                // Alert.alert('Sent!', "We've sent you a verification code", [{
                  Alert.alert('भेज दिया!', "हमने आपको एक सत्यापन कोड भेजा है", [{
                  text: 'OK',
                  onPress: () => this.refs.form.refs.textInput.focus()
                }]);
              }, 5000);
           

            } catch (err) {
              // <https://github.com/niftylettuce/react-native-loading-spinner-overlay/issues/30#issuecomment-276845098>
              this.setState({ spinner: false });
              setTimeout(() => {
                console.log('Oops',err.message);
                // console.log(this.state.mobileno);
                Alert.alert('Oops!', err.message);
              }, 5000);
            }

          }, 1000);
        }
        else {
          this.setState({ spinner: false });
          this.showToast('"कृपया पुन: प्रयास करें।');
        }
      }, (error) => {
        Alert.alert('Oops!', error);
        this.setState({ spinner: false });

      });


    } else {
      this.setState({
        spinner: false
      });

      alert('कृपया 10 अंकों का वैध मोबाइल नंबर दर्ज करें !')
    }

  }

  _verifyCode = () => {

    this.setState({ spinner: true });

    setTimeout(async () => {

      try {

        Apis.getData(this.state.mob,this.state.mobileno).then((response) => {

          console.log('res response',response)
         
        // const res = api.get('/api-login.asmx/Verify_Otp_and_Getdetails', {
        //   body: {
        //     ...this.refs.form.getValues(),
        //     //this.state.country
        //   }
        // });
       
        // if (res.err) throw res.err;

        this.refs.form.refs.textInput.blur();
        // <https://github.com/niftylettuce/react-native-loading-spinner-overlay/issues/30#issuecomment-276845098>
        this.setState({ spinner: false });
        if (response.status == 'success') {
          // console.log('after Sucess',this.state.mobileno);
          // AsyncStorage.setItem("mobile_no",this.state.mobileno);
          setTimeout(() => {
            // Alert.alert('Success!', 'You have successfully verified your phone number');
            Alert.alert('Success!', 'आपने अपना फ़ोन नंबर सफलतापूर्वक सत्यापित कर लिया है');
            // console.log('res');
            // console.log(res);
            AsyncStorage.setItem("userDetail", JSON.stringify(response.messages));
            AsyncStorage.getItem("userDetail").then((value) => {
              console.log('value');
              console.log(JSON.parse(value));
            });
            //    AsyncStorage.getItem("mobile_no").then((value) => {
            //     console.log('mobile_no'); 
            //   console.log((value) );       
            //  });
            AsyncStorage.setItem("loggedIn", "2");
            this.props.navigation.navigate('CitizenDashboard');
         
          }, 1000);
        } else {
          Alert.alert(response.messages);
       
          this.props.navigation.navigate('Auth');
        }
      });
      } 
      
      catch (err) {
        // <https://github.com/niftylettuce/react-native-loading-spinner-overlay/issues/30#issuecomment-276845098>
        this.setState({ spinner: false });
        console.log('hello', err.message);
        setTimeout(() => {
          Alert.alert('Oops!', err.message);

        }, 1000);
      }

    }, 5000);

  }

  _onChangeText = (val) => {
    //console.log('val');
    // console.log(val);
    this.setState({ mobileno: val });
    // console.log('this.state.mobileno');
    //console.log(this.state.mobileno);
    if (!this.state.enterCode) return;
    if (val.length === MAX_LENGTH_CODE)
      this._verifyCode();
  }

  _tryAgain = () => {
    this.refs.form.refs.textInput.setNativeProps({ text: '' })
    this.refs.form.refs.textInput.focus();
    this.setState({ enterCode: true });
    console.log('this.state.enterCode');
    console.log(this.state.enterCode);
  }

  _getSubmitAction = () => {
    this.state.enterCode ? this._verifyCode() : this._getCode();
  }

  _changeCountry = (country) => {
    this.setState({ country });
    this.refs.form.refs.textInput.focus();
  }

  // componentDidMount(){
  //   NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);

  //   NetInfo.isConnected.fetch().done(
  //     (isConnected) => { this.setState({ status: isConnected }); }
  //   );
  // }

  // componentWillUnmount() {
  //   NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
  // }

  // handleConnectionChange = (isConnected) => {
  //         this.setState({ status: isConnected });
  //         console.log(`is connected: ${this.state.status}`);
  //         alert(`Please check internet connection.`);
  // }

  _renderFooter = () => {

    if (this.state.enterCode)
      return (
        <View>
          <Text style={styles.wrongNumberText} onPress={this._tryAgain}>
            {/* Enter the wrong number or need a new code? */}
          </Text>
        </View>
      );

    return (
      <View>
        {/* <Text style={styles.disclaimerText}>By tapping "Send confirmation code" above, we will send you an SMS to confirm your phone number. </Text> */}
     
        <Text style={styles.disclaimerText}>बटन क्लिक करके " ओ टी पी कोड भेजें" आपके फ़ोन नंबर की पुष्टि करने के लिए हम आपको एक एसएमएस भेजेंगे. </Text>

      </View>
    );

  }

  render() {

    // let headerText = `What's your ${this.state.enterCode ? 'verification code' : 'phone number'}?`
    // let buttonText = this.state.enterCode ? 'Verify confirmation code' : 'Send confirmation code';
    let headerText = `What's your ${this.state.enterCode ? ' ओ टी पी  संख्या' : 'फ़ोन  नम्बर '}?`
    let buttonText = this.state.enterCode ? 'ओ टी पी  कोड सत्यापित करें ' : 'ओ टी पी  कोड भेजें ';
    let textStyle = this.state.enterCode ? {
      height: 50,
      textAlign: 'center',
      fontSize: 40,
      fontWeight: 'bold',
      fontFamily: 'Courier'
    } : {};

    return (

      <View style={{ flex: 1, backgroundColor: '#117bc7' }}>
      <StatusBar
                  barStyle="dark-content"
                  backgroundColor="#117bc7"
                />
            
           
          
            <Image style={{ alignSelf: 'center',marginTop:50,width:180,height:180}}
      styleName="medium-avatar"
      source={require('../../assets/icon/icon-01.png')}
    />
      <Row >
                    <Col style={{ alignItems: 'center', marginTop: 20, }}>

                      <Form ref={'form'} style={styles.form}>

                        <View style={{ flexDirection: 'row', borderRadius: 5, width: '100%', backgroundColor: '#399DD8',height:50 }}>
 <TextInput style={{color:'#fff', borderRadius: 5, width: '95%', backgroundColor: '#399DD8' }}
                            ref={'textInput'}
                            name={this.state.enterCode ? 'otp' : 'mobileno'}
                            type={'TextInput'}
                            underlineColorAndroid={'transparent'}
                            autoCapitalize={'none'}
                            autoCorrect={false}
                            onChangeText={this._onChangeText}
                            placeholder={this.state.enterCode ? '_ _ _ _ _ _' : 'फ़ोन  नम्बर'}
                            keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'numeric'}
                            style={[styles.textInput, textStyle]}
                            returnKeyType='go'
                            autoFocus
                            placeholderTextColor={brandColor}
                            selectionColor={brandColor}
                            maxLength={this.state.enterCode ? 6 : 10}
                            onSubmitEditing={this._getSubmitAction} />

                        </View>

                        <TouchableOpacity style={styles.button} onPress={this._getSubmitAction}>
                          <Text style={styles.buttonText}>{buttonText}</Text>
                        </TouchableOpacity>

                        {this._renderFooter()}

                      </Form>
                    </Col>
                  </Row>
                  <Spinner
            visible={this.state.spinner}
            textContent={'कृपया प्रतिक्षा करें...'}
            textStyle={{ color: '#fff' }}/>
          </View>
    );
  }
}

