import React, { Component } from 'react';
import {Platform, StyleSheet, Image, Dimensions, AsyncStorage} from 'react-native';
import { View, Grid, Separator, Row, Col, Container, List, Switch, Header, Item, Spinner, Label, Content, Form, Input, Icon, Left, Button, Title, Body, Right, SocialIcon, Card, CardItem, Text, Thumbnail, ListItem, CheckBox, Tabs, Tab } from 'native-base';



export default class Logout extends Component<{}> {
  
    componentWillMount(){
        AsyncStorage.setItem("loggedIn",'0');
        AsyncStorage.setItem("mobile",'');
        AsyncStorage.setItem('userDeatail','');
        AsyncStorage.getItem("loggedIn").then((value) => {
            if(value=='0'){
                this.props.navigation.navigate('Auth');
            }
        });
    }
  
    render(){
        return (
            <Container style={{backgroundColor:'#fff'}}>
               <Spinner />
            </Container>
        );
    }
}