import React, { Component } from 'react';
import { AsyncStorage, BackHandler } from 'react-native';
import {createStackNavigator,createAppContainer } from 'react-navigation';

import Dashboard from '../pages/OfficerModule/Dashboard';

import AttributeWiseList from '../pages/OfficerModule/AttributeWiseList';
import DistrictWiseList from '../pages/OfficerModule/DistrictWiseList';
import CompList from '../pages/OfficerModule/ComplainList';
import CompDetail from '../pages/OfficerModule/ComplaintDetail';

import SearchComp from '../pages/OfficerModule/ComplainListWithSearch';
import Profile from '../pages/OfficerModule/Profile';
import Logout from '../pages/Login/Logout';
import OutofDept from '../pages/OfficerModule/OutofDepartment';


console.disableYellowBox = true;

 const AppNavigator = createStackNavigator (
    {
        Dashboard: {
            screen: Dashboard,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        },
       
        AttributeWiseList: {
            screen: AttributeWiseList,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        },
        DistrictWiseList: {
            screen: DistrictWiseList,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        },
        CompList: {
            screen: CompList,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        },
        OutofDept: {
            screen: OutofDept,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        },
        CompDetail: {
            screen: CompDetail,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        },
      
        SearchComp: {
            screen: SearchComp,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        },
        Profile: {
            screen: Profile,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        },
        Logout: {
            screen: Logout,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        }
        
      
      },
    {
         initialRouteName: 'Dashboard',

    }
  );


  export default createAppContainer(AppNavigator);

