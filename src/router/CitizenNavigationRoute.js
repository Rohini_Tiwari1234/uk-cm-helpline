import React, { Component } from 'react';
import { AsyncStorage, BackHandler } from 'react-native';
import {createStackNavigator,createAppContainer } from 'react-navigation';

import CitizenDashboard from '../pages/CitizenModule/CitizenDashboard';
import SearchComp from '../pages/CitizenModule/ComplainListWithSearch';
import CreateComp from '../pages/CitizenModule/Citizencomplainfrom';
import CompList from '../pages/CitizenModule/ComplainList';
import CProfile from '../pages/CitizenModule/CitizenProfile';
import Logout from '../pages/Login/Logout';
import FAQs from '../pages/CitizenModule/CitizenFAQ';
import CompDetail from '../pages/CitizenModule/ComplaintDetail';


console.disableYellowBox = true;

 const AppNavigator = createStackNavigator (
    {
        CitizenDashboard: {
            screen: CitizenDashboard,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        },
       
        SearchComp: {
            screen: SearchComp,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        },

        CreateComp: {
            screen: CreateComp,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        },

        CompDetail: {
            screen: CompDetail,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        },
      
        CompList: {
            screen: CompList,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        },

        Logout: {
            screen: Logout,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        },
        FAQs: {
            screen: FAQs,
            navigationOptions: ({ navigation }) => ({
                header: null,
            })
        }
        
      
      },
    {
         initialRouteName: 'CitizenDashboard',

    }
  );


  export default createAppContainer(AppNavigator);

